# GazeEEGSynchro software


## Introduction

The GazeEegSynchro software runs under DOS.  It allows to synchronize data coming from Eyelink ASC data files and Brainamp EEG+VHDR+VMRK data files. The synchronization process is based on piecewise linear alignments of shared triggers in both acquisition devices.
The detailed documentation is available in the present package. A Zenodo repository with datasets is available at [https://doi.org/10.5281/zenodo.5718043](https://doi.org/10.5281/zenodo.5718043)

**Related article:** Ionescu†, G., Frey, A., Guyader, N., Kristensen, E., Andreev, A., & Guérin-Dugué, A. (2021). Synchronization of acquisition devices in neuroimaging: An application using co-registration of eye movements and electroencephalography. Accepted, _Behavior Research Methods_

## Contents

- License file (Licence CeCILL) 
- Tutorial file : GazeEEGTutorial.pdf 
- Codes
      * Sources (c++) for GazeEegSynchro 
      "./src/"    
      "./src/GazeEegLib"    
      "./src/GazeEegLib/AcqPhase.cpp"  
      "./src/GazeEegLib/AcqPhase.h"  
      "./src/GazeEegLib/AcqPhases.cpp"  
      "./src/GazeEegLib/AcqPhases.h"  
      "./src/GazeEegLib/AddEventStim.cpp"  
      "./src/GazeEegLib/AscHeader.cpp"  
      "./src/GazeEegLib/AscHeader.h"  
      "./src/GazeEegLib/AscRawData.cpp"  
      "./src/GazeEegLib/AscRawData.h"  
      "./src/GazeEegLib/Blink.cpp"  
      "./src/GazeEegLib/Blink.h"  
      "./src/GazeEegLib/Blinks.cpp"  
      "./src/GazeEegLib/Blinks.h"  
      "./src/GazeEegLib/Brainamp.cpp"  
      "./src/GazeEegLib/Brainamp.h"  
      "./src/GazeEegLib/BrainampEEG.h"  
      "./src/GazeEegLib/BrainampVhdr.cpp"  
      "./src/GazeEegLib/BrainampVhdr.h"  
      "./src/GazeEegLib/BrainampVmrk.cpp"  
      "./src/GazeEegLib/BrainampVmrk.h"  
      "./src/GazeEegLib/CAddEventStim.cpp"  
      "./src/GazeEegLib/CAddEventStim.h"  
      "./src/GazeEegLib/CSequence.cpp"  
      "./src/GazeEegLib/CSequence.h"  
      "./src/GazeEegLib/CTransition.cpp"  
      "./src/GazeEegLib/CTransition.h"  
      "./src/GazeEegLib/Displayable.cpp"  
      "./src/GazeEegLib/Displayable.h"  
      "./src/GazeEegLib/Displayables.cpp"  
      "./src/GazeEegLib/Displayables.h"  
      "./src/GazeEegLib/Event.cpp"  
      "./src/GazeEegLib/Event.h"  
      "./src/GazeEegLib/Events.cpp"  
      "./src/GazeEegLib/Events.h"  
      "./src/GazeEegLib/EventStimMap.cpp"  
      "./src/GazeEegLib/EventStimMap.h"  
      "./src/GazeEegLib/File.cpp"  
      "./src/GazeEegLib/File.h"  
      "./src/GazeEegLib/FixationSaccade.cpp"  
      "./src/GazeEegLib/FixationSaccade.h"  
      "./src/GazeEegLib/FixationSaccades.cpp"  
      "./src/GazeEegLib/FixationSaccades.h"  
      "./src/GazeEegLib/Frame.cpp"  
      "./src/GazeEegLib/Frame.h"  
      "./src/GazeEegLib/Frames.cpp"  
      "./src/GazeEegLib/Frames.h"  
      "./src/GazeEegLib/GazeEegEnv.cpp"  
      "./src/GazeEegLib/GazeEegEnv.h"  
      "./src/GazeEegLib/GazeEegSynchro.cpp"  
      "./src/GazeEegLib/Geometry.hpp"  
      "./src/GazeEegLib/Interpolator.cpp"  
      "./src/GazeEegLib/Interpolator.h"  
      "./src/GazeEegLib/Lcs.h"  
      "./src/GazeEegLib/Marker.cpp"  
      "./src/GazeEegLib/Marker.h"  
      "./src/GazeEegLib/MatBuilder.cpp"  
      "./src/GazeEegLib/MatComponents.cpp"  
      "./src/GazeEegLib/MatComponents.h"  
      "./src/GazeEegLib/MemoryFile.cpp"  
      "./src/GazeEegLib/MemoryFile.h"  
      "./src/GazeEegLib/Parser.cpp"  
      "./src/GazeEegLib/Parser.h"  
      "./src/GazeEegLib/Point2d.cpp"  
      "./src/GazeEegLib/Point2d.h"  
      "./src/GazeEegLib/Random.h"  
      "./src/GazeEegLib/Record.cpp"  
      "./src/GazeEegLib/Record.h"  
      "./src/GazeEegLib/Records.cpp"  
      "./src/GazeEegLib/Records.h"  
      "./src/GazeEegLib/Sequences.cpp"  
      "./src/GazeEegLib/Sequences.h"  
      "./src/GazeEegLib/stdafx.cpp"  
      "./src/GazeEegLib/stdafx.h"  
      "./src/GazeEegLib/StimulationMap.cpp"  
      "./src/GazeEegLib/StimulationMap.h"  
      "./src/GazeEegLib/targetver.h"  
      "./src/GazeEegLib/Transitions.cpp"  
      "./src/GazeEegLib/Transitions.h"  
      "./src/GazeEegLib/Trigger.cpp"  
      "./src/GazeEegLib/Trigger.h"  
      "./src/GazeEegLib/TriggerDef.cpp"  
      "./src/GazeEegLib/TriggerDef.h"  
      "./src/GazeEegLib/TriggerMatching.cpp"  
      "./src/GazeEegLib/TriggerMatching.h"  
      "./src/GazeEegLib/Triggers.cpp"  
      "./src/GazeEegLib/Triggers.h"  
      "./src/GazeEegLib/Ttl.cpp"  
      "./src/GazeEegLib/Ttl.h"  
      "./src/GazeEegLib/Types.h"  
      "./src/GazeEegLib/Version.hpp"  
      "./src/GazeEegSynchro"  
      "./src/GazeEegSynchro/GazeEegSynchro.cpp"  
      "./src/GazeEegSynchro/stdafx.cpp"  
      "./src/GazeEegSynchro/stdafx.h"  
      "./src/GazeEegSynchro/targetver.h"  
      "./src/Geometry"  
      "./src/Geometry/Box.cpp"  
      "./src/Geometry/Box.inl.hpp"  
      "./src/Geometry/Circle.cpp"  
      "./src/Geometry/Circle.inl.hpp"  
      "./src/Geometry/Covector2d.cpp"  
      "./src/Geometry/Covector2d.inl.hpp"  
      "./src/Geometry/Covector3d.cpp"  
      "./src/Geometry/Covector3d.inl.hpp"  
      "./src/Geometry/Ellipse.cpp"  
      "./src/Geometry/Equation.cpp"  
      "./src/Geometry/Equation.hpp"  
      "./src/Geometry/Geometry.hpp"  
      "./src/Geometry/Geometry.inl.hpp"  
      "./src/Geometry/Geometry2d.hpp"  
      "./src/Geometry/Geometry3d.hpp"  
      "./src/Geometry/GeometryInt2d.cpp"  
      "./src/Geometry/GeometryInt2d.hpp"  
      "./src/Geometry/GeometryInt2d.inl.hpp"  
      "./src/Geometry/GeometryInt3d.cpp"  
      "./src/Geometry/GeometryInt3d.hpp"  
      "./src/Geometry/GeometryInt3d.inl.hpp"  
      "./src/Geometry/Line2d.cpp"  
      "./src/Geometry/Line2d.inl.hpp"  
      "./src/Geometry/Line3d.cpp"  
      "./src/Geometry/Line3d.inl.hpp"  
      "./src/Geometry/Macros.hpp"  
      "./src/Geometry/Matrix2d.cpp"  
      "./src/Geometry/Matrix2d.inl.hpp"  
      "./src/Geometry/Matrix3d.cpp"  
      "./src/Geometry/Matrix3d.inl.hpp"  
      "./src/Geometry/Plane.cpp"  
      "./src/Geometry/Plane.inl.hpp"  
      "./src/Geometry/Point2d.cpp"  
      "./src/Geometry/Point2d.inl.hpp"  
      "./src/Geometry/Point3d.cpp"  
      "./src/Geometry/Point3d.inl.hpp"  
      "./src/Geometry/Polygon.cpp"  
      "./src/Geometry/Quadric2d.cpp"  
      "./src/Geometry/Quadric2d.inl.hpp"  
      "./src/Geometry/Quadric3d.cpp"  
      "./src/Geometry/Quadric3d.inl.hpp"  
      "./src/Geometry/Quaternion.cpp"  
      "./src/Geometry/Quaternion.inl.hpp"  
      "./src/Geometry/Transform2d.cpp"  
      "./src/Geometry/Transform2d.inl.hpp"  
      "./src/Geometry/Transform3d.cpp"  
      "./src/Geometry/Transform3d.inl.hpp"  
      "./src/Geometry/Vector2d.cpp"  
      "./src/Geometry/Vector2d.inl.hpp"  
      "./src/Geometry/Vector3d.cpp"  
      "./src/Geometry/Vector3d.inl.hpp"  
	* matio library
      "./src/matio"
	* zlib library
      "./src/zlib"
	* Code to generate histograms 
	"./ViewHistograms/"
      "./ViewHistograms/fViewHistograms.m"  
      "./ViewHistograms/ViewHistograms.m"
      "./ViewHistograms/ViewHistograms.py"
      "./ViewHistograms/histoerror.py"
      "./ViewHistograms/GlobalError.txt"
	This file is described in GazeEEGTutorial.pdf




## Author
**Gelu Ionescu** (2018, Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP)

## Contributors
- Researchers:
* **Anne Guérin-Dugué** (2021, Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP)
* **Aline Frey** (2021, Laboratoire de Neurosciences Cognitives, UMR 7291, CNRS - INSPE d'Aix-Marseille)
* **Nathalie Guyader** (2021, Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP)

- Engineers:
* **Emmanuelle Kristensen** (2021, Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP)
* **Anton Andreev** (2021, Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP)

## Contact

You can contact Emmanuelle Kristensen through email at emmanuelle.kristensen[at]gipsa-lab.grenoble-inp.fr

## License
Copyright (c) 2018  Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev, Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
All rights reserved. 

This software is governed by the CeCILL-B license under French law and abiding by the rules of distribution of free software.  You can use, modify and/ or redistribute the software under the terms of the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license, users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited liability. 

In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that it is reserved for developers  and  experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and,  more generally, to use and operate it in the same conditions as regards security. 

The fact that you are presently reading this means that you have had knowledge of the CeCILL-B license and that you accept its terms.

## Dependency

This software uses several dependencies.

* [zlib]( http://zlib.net/)

Copyright (C) 1995-2017 Jean-loup Gailly and Mark Adler

 This software is provided 'as-is', without any express or implied warranty.  In no event will the authors be held liable for any damages arising from the use of this software.
 Permission is granted to anyone to use this software for any purpose, including commercial applications, and to alter it and redistribute it freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Jean-loup Gailly        Mark Adler
  jloup@gzip.org          madler@alumni.caltech.edu


* [Matio](https://github.com/tbeu/matio)

Copyright (c) 2011-2020, Christopher C. Hulbert
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

* [io.cpp](http://groups.inf.ed.ac.uk/vision/MAJECKA/Detector/Resources/libraries/matio-1.3.3/src/io.c)

Copyright (C) 2005-2006   Christopher C. Hulbert

This library is free software; you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation; either version 2.1 of the License, or (at your option) any later version.
This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.
You should have received a copy of the GNU Lesser General Public License along with this library; if not, write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

* stdind.h and inttypes.h

Copyright (c) 2006 Alexander Chemeris

 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
3. The name of the author may be used to endorse or promote products
derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED
WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


