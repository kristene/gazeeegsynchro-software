/*!	\file	GazeEegLib.h
 	\author	Gelu Ionescu.
 
 	\brief	C++ library support for converting the "matio" library toward the "GazeEeg" matlab conventions
*/
#pragma once


#include <string>
#include <vector>

#include "GazeEegLib/GazeEegLib/Parser.h"
#include "GazeEegLib/GazeEegLib/Random.h"
#include "GazeEegLib/GazeEegLib/TriggerMatching.h"

extern int	MatBuilder(int argc, char* argv[]);
extern int	GaseEegSynchro(int argc, char* argv[]);
extern int	AddEventStim(int argc, char* argv[]);
