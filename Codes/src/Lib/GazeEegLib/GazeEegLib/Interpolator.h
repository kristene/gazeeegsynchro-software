/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

#pragma once

#define _MATH_DEFINES_DEFINED
#include "TriggerMatching.h"

template <class srcT, class destT> class Interpolator
{
public:
	Interpolator(destT*& pDest, const srcT* pSrcCrt, const srcT* pSrcLim, const size_t nbChannels, const srcT nan)
		: pDestCrt(pDest)
		, pSrcCrt(pSrcCrt)
		, pSrcLim(pSrcLim)
		, nbChannels(nbChannels)
		, nan(nan)
		, interpolatorFactor(1)
	{}

	bool Process(const size_t iSample, const bool noInterpolateLast = false)
	{	
		double	posD	= iSample*interpolatorFactor;
		size_t	posI	= size_t(posD);
		double	d		= posD	- posI;
		double	d1		= 1.0	- d;

		const srcT* pSrc	= pSrcCrt	+ posI*nbChannels;
		const srcT* pSrc1	= pSrc		+ nbChannels;
		
		if(pSrc1 >= pSrcLim)
			return false;

		size_t nbChans = noInterpolateLast ? (nbChannels - 1) : nbChannels; 

		for(size_t ii=0; ii < nbChans; ii++)
		{	srcT v	= *pSrc++;
			srcT v1	= *pSrc1++;
			
			*pDestCrt++	= ((v == nan) || (v1 == nan)) ?
						  destT(nan) : 
						  destT(d1*v + d*v1);
		}

		if(noInterpolateLast)
			*pDestCrt++	= destT((d < 0.5) ? *pSrc : *pSrc1);

		return true;
	}

public:
	destT*&			pDestCrt;
	const srcT*		pSrcCrt;
	const srcT*		pSrcLim;
	size_t			nbChannels;
	const srcT		nan;
	double			interpolatorFactor;
};

template <class gazeT, class eegT> class Merger : public std::vector<eegT>
{
public:
	Merger(const std::vector<gazeT>& gazeData, const size_t gazeOffSamples, const size_t gazeNbSamples, const size_t gazeNbChannels, const double gazeSamplingRate, const gazeT gazeNan, const std::vector<eegT>& eegData, const size_t eegOffSamples, const size_t eegNbSamples, const size_t eegNbChannels, const double eegSamplingRate, const eegT eegNan)
		: eegIsReference(false)
		, nbOfSamples(0)
		, nbOfChannels(0)
		, gazeNbSamples(gazeNbSamples)
		, gazeSamplingRate(::round(gazeSamplingRate))
		, eegNbSamples(eegNbSamples)
		, eegSamplingRate(::round(eegSamplingRate))
		, pDestCrt(0)
		, gazeInterpolator(pDestCrt, &gazeData[0]	+ gazeOffSamples*gazeNbChannels,	&gazeData[0] + gazeData.size() - gazeNbChannels, gazeNbChannels, gazeNan)
		, eegInterpolator(pDestCrt, &eegData[0]		+ eegOffSamples*eegNbChannels,		&eegData[0]	 + eegData.size()  - eegNbChannels,  eegNbChannels, eegNan)
	{}

	size_t						NbOfSamples()				{	return nbOfSamples;											}
	size_t						NbOfChannels()				{	return nbOfChannels;										}
	const	std::vector<eegT>&	Data() const				{	return *this;												}
	double						GazeInterpolatorFactor()	{	return gazeInterpolator.interpolatorFactor;					}
	double						SamplingRate()				{	return eegIsReference ? eegSamplingRate : gazeSamplingRate;	}
	double						EegSamplingRate()			{	return eegSamplingRate;										}
	double						GazeSamplingRate()			{	return gazeSamplingRate;									}
	int							EegReference()				{	return eegIsReference ? 1 : 0;								}
	int							GazeReference()				{	return eegIsReference ? 0 : 1;								}

	bool Process(const std::string& fileName, const bool eegIsReference, const bool doDrift)
	{
		this->eegIsReference	= eegIsReference;
		nbOfChannels			= gazeInterpolator.nbChannels + eegInterpolator.nbChannels;

		if(eegIsReference)
		{	nbOfSamples								= eegNbSamples;
			gazeInterpolator.interpolatorFactor		= double(doDrift ? double(gazeNbSamples)/double(eegNbSamples) : gazeSamplingRate/eegSamplingRate);
		}
		else
		{	nbOfSamples								= gazeNbSamples;
			eegInterpolator.interpolatorFactor		= double(doDrift ? double(eegNbSamples)/double(gazeNbSamples) : eegSamplingRate/gazeSamplingRate);
		}
				
		resize(nbOfSamples*nbOfChannels);
		pDestCrt	= &(*this)[0];	
	
		size_t	indSample = 0;
	
		
		while (indSample < nbOfSamples)

		{

			if(!eegInterpolator.Process(indSample))
				break;
			
			if(!gazeInterpolator.Process(indSample, true))
				break;

			indSample++;
		}

		if(indSample < nbOfSamples)
			erase(begin() + indSample*nbOfChannels);
		
		nbOfSamples	= indSample;

		#ifdef _DEBUG
		//added by Anton
		//===========================
		/*double sum = 0;
		for (int i = 0; i < Data().size(); i++)
			sum = sum + Data()[i];
		if (sum == 0)
			std::cout << "ERROR: data is 0!" << std::endl;*/

		//===========================
		
		#endif

		#if 0 //debug 
		if (Data().size() != nbOfChannels * nbOfSamples)
			std::cout << "WARNING: EEG data not multiple of data points and number of samples! Current value is: " << Data().size() << " vs expected: " << (nbOfChannels * nbOfSamples) << " (expected). nbOfChannels = " << nbOfChannels << ", nbOfSamples = " << nbOfSamples << std::endl;
		#endif
		
		
		return GazeEeg::File::Write(fileName, Data());
	}

private:
	bool							eegIsReference;
	size_t							nbOfSamples;
	size_t							nbOfChannels;
	size_t							gazeNbSamples;
	double							gazeSamplingRate;
	size_t							eegNbSamples;
	double							eegSamplingRate;
	eegT*							pDestCrt;
	Interpolator<gazeT, eegT>		gazeInterpolator;
	Interpolator<eegT, eegT>		eegInterpolator;
};

template <class gazeT, class eegT> class PrepareSynchro
{
public:
	bool Process(MarkerList_t& trueMarkers, size_t& trueGazeOff, size_t& trueGazeSamples, size_t& trueEegOff, size_t& trueEegSamples, std::vector<gazeT>& gaze, std::vector<eegT>& eeg, const MarkerList_t& gazeTriggers, const MarkerList_t& gazeEvents, const size_t gazeNbChannels, const MarkerList_t& eegTriggers, const size_t eegNbChannels, const std::vector<Lcs::Indexes>& indexes, const TriggerMatching& triggerMatching)
	{
		std::vector<MarkerList_t::iterator>::const_iterator	firstIt = triggerMatching.mySegments->begin() + triggerMatching.myFirstSegment;
		std::vector<MarkerList_t::iterator>::const_iterator	lastIt	= triggerMatching.mySegments->begin() + triggerMatching.myLastSegment - 1;
		std::vector<Lcs::Indexes>::const_iterator			startIt = indexes.begin() + (*firstIt)->start;
		std::vector<Lcs::Indexes>::const_iterator			stopIt	= indexes.begin() + (*lastIt)->stop;
		
		MarkerList_t::const_iterator firstGaze	= gazeTriggers.begin()	+ startIt->index1;
		MarkerList_t::const_iterator firstEeg	= eegTriggers.begin()	+ startIt->index2;
		MarkerList_t::const_iterator lastGaze	= gazeTriggers.begin()	+ stopIt->index1;
		MarkerList_t::const_iterator lastEeg	= eegTriggers.begin()	+ stopIt->index2;

		trueGazeOff		= firstGaze->offset;
		trueEegOff		= firstEeg->offset;
		trueGazeSamples = lastGaze->offset	- trueGazeOff + 1;
		trueEegSamples	= lastEeg->offset	- trueEegOff + 1;

		// combine triggers & events
		trueMarkers.clear();
		trueMarkers.reserve(gazeTriggers.size() + gazeEvents.size());

		MarkerList_t::const_iterator gazeEventsIt = gazeEvents.begin();
		while(gazeEventsIt->offset < firstGaze->offset) // tant que l'event est devant l'offset du 1er trigger
			gazeEventsIt++;

		trueMarkers.push_back(CMarker("", firstGaze->value, firstGaze->offset, firstGaze->duration, 0));
		trueMarkers.rbegin()->fseek = firstGaze->fseek;

#if 1
		for(MarkerList_t::const_iterator gazeTriggerIt=firstGaze+1; gazeTriggerIt <= lastGaze; gazeTriggerIt++)
		{	while((gazeEventsIt != gazeEvents.end()) && (gazeEventsIt->offset < gazeTriggerIt->offset))
			{	if(gazeEventsIt->value >= eyetrackerEvents) // si l'event > 1000 = event oculo
				{	trueMarkers.push_back(CMarker("", gazeEventsIt->value, gazeEventsIt->offset, gazeEventsIt->duration, 0));
					trueMarkers.rbegin()->srcEvent = gazeEventsIt->srcEvent;
				}

				gazeEventsIt++;
			}

			trueMarkers.push_back(CMarker("", gazeTriggerIt->value, gazeTriggerIt->offset, gazeTriggerIt->duration, 0));
			trueMarkers.rbegin()->fseek = gazeTriggerIt->fseek;
		}
#else
		for(std::vector<Lcs::Indexes>::const_iterator it=startIt+1; it <= stopIt; it++)
		{	MarkerList_t::const_iterator gazeTriggerIt	= gazeTriggers.begin()	+ it->index1;

			while((gazeEventsIt != gazeEvents.end()) && (gazeEventsIt->offset < gazeTriggerIt->offset))
			{	if(gazeEventsIt->value >= eyetrackerEvents)
				{	trueMarkers.push_back(CMarker("", gazeEventsIt->value, gazeEventsIt->offset, gazeEventsIt->duration, 0));
					trueMarkers.rbegin()->srcEvent = gazeEventsIt->srcEvent;
				}

				gazeEventsIt++;
			}

			trueMarkers.push_back(CMarker("", gazeTriggerIt->value, gazeTriggerIt->offset, gazeTriggerIt->duration, 0));
			trueMarkers.rbegin()->fseek = gazeTriggerIt->fseek;
		}
#endif

		return true;
	}
};