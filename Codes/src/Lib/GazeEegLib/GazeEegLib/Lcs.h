/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

#pragma once

#include <vector>

class Lcs
{
public:
	class Indexes
	{
	public:
		Indexes(const size_t i1 = 0, const size_t i2 = 0)
			: index1(i1)
			, index2(i2)
		{}

		size_t	index1;
		size_t	index2;
	};

	Lcs()
		: myNbLines(0)
		, myNbCols(0)
		, myScoreCore(0)
		, myScores(0)
		, myScoreCoreCh(0)
		, myScoresCh(0)
	{
	}
	~Lcs()
	{
		/*if (myScoreCore)
			::free(myScoreCore);
		if (myScores)
			::free(myScores);
		if (myScoreCoreCh)
			::free(myScoreCoreCh);
		if (myScoresCh)
			::free(myScoresCh);*/
	}

	const std::vector<Indexes>& GetIndexes() const { return myIndexes; }

	template <class type> bool Process1(const std::vector<type>& list1, const std::vector<type>& list2) // LCS Cumin
	{
		std::cout << "Running Process LCS" << std::endl;

		if (!Build(list1, list2))
			return false;



	
		for (size_t l = 1; l <= myNbLines; l++)
		{
			int vList1 = list1[l - 1];

			for (size_t c = 1; c <= myNbCols; c++)
			{
				int vList2 = list2[c - 1];

				if (vList1 == vList2)
				{
					myScores[l][c] = myScores[l - 1][c - 1] + 1;
					myScoresCh[l][c] = '\\';
				}
				else
				{
					myScores[l][c] = myScores[l - 1][c - 1];
				}
				if (myScores[l - 1][c] >= myScores[l][c])
				{
					myScores[l][c] = myScores[l - 1][c];
					myScoresCh[l][c] = '|';
				}
				if (myScores[l][c - 1] >= myScores[l][c])
				{
					myScores[l][c] = myScores[l][c - 1];
					myScoresCh[l][c] = '-';
				}
			}
		}

		Match(myNbLines, myNbCols);

	/*	FILE *f = fopen("scores.csv", "w");
		if (f != NULL) {
			for (int i = 0; i <= myNbLines; i++) {
				for (int j = 0; j <= myNbCols; j++) {
					if (j != 0)
						fprintf(f, ";");
					fprintf(f, "%d", myScores[i][j]);
				}
				fprintf(f, "\n");
			}
			fclose(f);
		}
		f = fopen("scoresch1.csv", "w");
		if (f != NULL) {
			for (int i = 0; i <= myNbLines; i++) {
				for (int j = 0; j <= myNbCols; j++) {
					if (j != 0)
						fprintf(f, ";");
					fprintf(f, "%c", myScoresCh[i][j]);
				}
				fprintf(f, "\n");
			}
			fclose(f);
		}*/

		return true;
	}
	
	template <class type> bool Process2(const std::vector<type>& list1, const std::vector<type>& list2) // classical LCS
	{
		std::cout << "Running Process classical LCS" << std::endl;

		if (!Build(list1, list2))
			return false;

		for (size_t l = 1; l <= myNbLines; l++)
		{
			int vList1 = list1[l - 1];

			for (size_t c = 1; c <= myNbCols; c++)
			{
				int vList2 = list2[c - 1];

				if (vList1 == vList2)
				{
					myScores[l][c] = myScores[l - 1][c - 1] + 1;
					myScoresCh[l][c] = '\\';
				}
				else
				{
						if(myScores[l-1][c] >= myScores[l][c-1])
						{
							myScores[l][c]	= myScores[l-1][c];
							myScoresCh[l][c]	= '|';

						}
						else
						{

							myScores[l][c]	= myScores[l][c-1];
							myScoresCh[l][c]	= '-';



					}
				}
				
			}
		}

		Match(myNbLines, myNbCols);

		return true;
	}

private:
	void Match(const size_t col, const size_t line)
	{
		if((col == 0) || (line == 0))
			return;

		if (myScoresCh[col][line] == '\\')
		{
			Match(col - 1, line - 1);
			myIndexes.push_back(Indexes(col - 1, line - 1));
		}
		else if (myScoresCh[col][line] == '|')
			Match(col - 1, line);
		else if (myScoresCh[col][line] == '-')
			Match(col, line - 1);
	
	}
	template <class type> bool Build(const std::vector<type>& list1, const std::vector<type>& list2) 
	{
		myNbLines	= list1.size();
		myNbCols	= list2.size();
	


		if(!myNbLines || !myNbCols)
			return false;

		myIndexes.reserve(__max(myNbLines, myNbCols));

		myScoreCore		= (int*)  ::calloc((myNbLines+1)*(myNbCols+1), sizeof(int));
		if(!myScoreCore)
			return false;

		myScores		= (int**) ::calloc((myNbLines+1), sizeof(int*));
		if(!myScores)
			return false;

		myScoreCoreCh	= (char*)  ::calloc((myNbLines+1)*(myNbCols+1), sizeof(char));
		if(!myScoreCoreCh)
			return false;

		myScoresCh		= (char**) ::calloc((myNbLines+1), sizeof(char*));
		if(!myScoresCh)
			return false;

		for(size_t ii=0; ii < myNbLines+1; ii++)
		{	myScores[ii]	= myScoreCore	+ ii*(myNbCols+1);
			myScoresCh[ii]	= myScoreCoreCh	+ ii*(myNbCols+1);
		}
		return true;
	}

private:
	std::vector<Indexes>		myIndexes;
	size_t						myNbLines;
	size_t						myNbCols;
    int*						myScoreCore;
    int**						myScores;
    char*						myScoreCoreCh;
    char**						myScoresCh;
};

