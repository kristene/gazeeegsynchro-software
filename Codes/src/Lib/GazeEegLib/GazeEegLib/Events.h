/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

/*!	\file	Events.h
 	\author	Gelu Ionescu.
 
 	\brief	Generic Events support
*/

#pragma once

#include "MatComponents.h"

/*!	\class	Events
 
	\brief Basic structure for an acquisition events vector
	
	\remark The \a Events class is simply a vector of  \a Event classes

	\see Event class defined in \a Event.h file
 */
 
class Events
{
protected:
	//! protected constructor
	/*! \param size size of the \a Event vector
	 */
	Events(const size_t size = 0);

	//! protected constructor
	/*! \param conserve
			- false	: compress the data
			- true	: do not compress the data

		/return matComponents a reference to the Matlab structure
	 */
	void GetComponents(MatComponents& matComponents, const bool conserve);

public:
	~Events();
	
	bool IsValid()	{	return value.size() != 0;	}
	void Add(const uint16 value, const uint32 time, const uint32 duration, const uint32 fseek);

	std::vector<uint16>		value;
	std::vector<uint32>		time;	//!< timestamps vector
	std::vector<uint32>		fseek;	//!< seek vector
};
