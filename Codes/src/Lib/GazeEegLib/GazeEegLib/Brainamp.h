/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

/*!	\file	Brainamp.h
 	\author	Gelu Ionescu.
 
 	\brief	Support for \a Brainamp data acquisition
*/
#pragma once

#define _USE_MATH_DEFINES
#include <math.h>

#include <iomanip>

#include "BrainampVhdr.h"
#include "BrainampVmrk.h"
#include "BrainampEEG.h"
#include "AscRawData.h"
#include "Random.h"
#include "TriggerMatching.h"
#include "Ttl.h"

class	Brainamp;

/*!	\class	Brainamp
 
 	\brief	\a Brainamp data acquisition structure
 */
 
class Brainamp
{
public:
	//! public constructor & destructor
	Brainamp();
	~Brainamp();

	//! static function that tests the validity of the \a Brainamp files
    /*! \param srcFileName source acquisition file (one of the \a EEG, \a VHDR or \a VMRK files
		
		\param resultFileName starting from \a srcFileName file, returns the name of the \a EEG file
		\param matFileName starting from \a srcFileName file, returns the name of the \a MAT file
		
		\return boolean
	 */
	bool	IsValid(GazeEegEnv& gazeEegEnv, const std::string& srcFileName); 
	
	//! function that returns the sample format (int16, float or double)
    /*!	\param	fileName source file name containing the information (actualy \a VHDR file)
		
		\return	the sample format
		
		\see	binaryFormat_t for sample formats
	 */
	Parser::binaryFormat_t		GetBinaryFormat(const std::string& fileName);

	double	AcqSamplingRate() const				{	return 1000000.0 / myVhdr.UsecPerSample();							}
	
	//! function that creates the \a MAT file recognized by \a Matlab
    /*! \param matFileName \a MAT destination file name	
		\param compress boolean that controls the data compression (avoid it)
		\param forceRaw for big anount of data, put the raw data in a separate file instead of inserting it in the resulting \a MAT file

		\return  boolean 
	 */
	bool						SaveMatFile(const GazeEegEnv& gazeEegEnv);
	bool						SaveMatFile(AscRawData& eyelink, const std::string& ascFileName);

	template <class type> bool	Process(const GazeEegEnv& gazeEegEnv, const type valNaN, const bool doDataCorrection = true)
	{
		delete	myEEG;
		myEEG			= new BrainampEEG<type>();
		((BrainampEEG<type>*) myEEG)->valNaN	= valNaN;

		// EK : replacement .eeg by .vhdr and .vhdr reading
		if(!myVhdr.Process(Parser::ReplaceExtention(gazeEegEnv.eegFileName, "vhdr")))
			return MSendError("Brainamp::Process::myVhdr.Process");

		// EK : replacement .eeg by .vmrk and .vmrk reading
		if(!myVmrk.Process(gazeEegEnv.DoAes(), Parser::ReplaceExtention(gazeEegEnv.eegFileName, "vmrk")))
			return MSendError("Brainamp::Process::myVmrk.Process");

		// EK : .eeg reading
		if(!myEEG->Process(gazeEegEnv.eegFileName))
			return MSendError("Brainamp::Process::myEEG.Process");

		#if 0 //debug code
		//Added by Anton. Why it was not used before ???
		if (!myEEG->IsDataValid(myVhdr.GetNbSamples(), myVhdr.GetNbChannels()))
		{
			std::cout << "NbSamples = " << myVhdr.GetNbSamples() << std::endl; //usually 0 because DataPoints= is missing in the input file
			std::cout << "NbChannels = " << myVhdr.GetNbChannels() << std::endl;
			std::cout << "Data.size = " << myEEG->GetDataSize() << std::endl;
			std::cout << "NbSamples * NbChannels = " << myVhdr.GetNbSamples() * myVhdr.GetNbChannels() << std::endl;
			//return MSendError("Brainamp::Process::myEEG.IsDataValid");
		}
		//std::cout << "NbSamples calculated = " << (double)myEEG->GetDataSize() / (double)myVhdr.GetNbChannels() << std::endl;
		#endif
		
		if(!gazeEegEnv.DoAes() && !ProcessDataSegments(doDataCorrection))
			return MSendError("Brainamp::Process::ProcessDataSegments");

		return true;
	}
	
	bool SaveEegLab(const GazeEegEnv& gazeEegEnv, const std::string& resultFile);

	//! template function that realises the synchronization with the \Eyelink data
    /*! \param srcEeg	source file name	
		\param destEeg	destination file name for the synchronized data
		\param eyelink	\a Eyelink data structure
		\param synchroType	synchronization mode
		\param valNaN	value for invalid sample
		\param eegReference	do the synchronization
				- true : toward the EEG device
				- false: toward the Eyetracker device
				
		\return  boolean 
	 */
	//this is the 5 parameters version which calls the 7 parameters version
	template <class type> bool SynchronizeGazeEeg(GazeEegEnv& gazeEegEnv, const std::string& destEeg, AscRawData& eyelink, const Ttl& ref, type eegNaN)
	{
		clock_t			begin		= clock();
		MarkerList_t&	eegTriggers	= GetTriggerMarkers();

		MarkerList_t	refTriggers;
		ref.GetTriggerMarkers(refTriggers);

		MarkerList_t	gazeTriggers, gazeEvents;
		eyelink.GetTriggerMarkers(gazeTriggers);
		eyelink.GetEventMarkers(gazeEvents);

		BrainampEEG<type>&	eegObj	= *((BrainampEEG<type>*) myEEG);

		if(ref.IsValid())
		{
			// test eeg section
			{	
				TriggerMatching	triggerMatching(refTriggers, eyelink.AcqSamplingRate(), eegTriggers, AcqSamplingRate());
				if (!triggerMatching.LongestCommonSubstring(gazeEegEnv, "Match TTLreference with EEGtriggers")) return false;
			} 
			// test Eyelink section
			{	
				TriggerMatching	triggerMatching(refTriggers, eyelink.AcqSamplingRate(), gazeTriggers, eyelink.AcqSamplingRate());
				//anton: possible error - missing "!"
				if (triggerMatching.LongestCommonSubstring(gazeEegEnv, "Match TTLreference with ETtriggers")) return false;
			}
		}

		TriggerMatching	triggerMatching(gazeTriggers, eyelink.AcqSamplingRate(), eegTriggers, AcqSamplingRate(), &myVmrk.Segments());

		if (!triggerMatching.LongestCommonSubstring(gazeEegEnv, "Match ETtriggers with EEGtriggers")) return false;

		triggerMatching.CorrectData(eegObj.data, myVhdr.GetNbChannels(), eegNaN);

		size_t			gazeOffSamples, gazeNbSamples, eegOffSamples, eegNbSamples;
		PrepareSynchro<Parser::double_t, type>	prepareSynchro;
		prepareSynchro.Process(eyelink.mySynchronization.currentMarkersS, gazeOffSamples, gazeNbSamples, eegOffSamples, eegNbSamples, eyelink.Data(), eegObj.data, gazeTriggers, gazeEvents, eyelink.GetNbChannels(), eegTriggers, myVhdr.GetNbChannels(), triggerMatching.GetIndexes(), triggerMatching);
		
		eyelink.mySynchronization.currentMarkersN	= eyelink.mySynchronization.currentMarkersS;
		
		gazeEegEnv.quality.tmpSync = (double)(clock() - begin) / CLOCKS_PER_SEC;

		//in all cases output a file with drift correction
		if(!SynchronizeGazeEeg(gazeEegEnv, triggerMatching, gazeOffSamples, gazeNbSamples, eegOffSamples, eegNbSamples, destEeg, eyelink, eegNaN, true))
			return false;

		//exit if the user has not requested a file with no drift correction
		if(!gazeEegEnv.doNoDrift)
			return true;

		//if requested also output a file with no drift correction
		std::string destEegNo(destEeg);
		std::string::size_type pos = destEegNo.find("synchro_");
		destEegNo.replace(pos, 8, "synchrono_");

		//with no drift correction
		return SynchronizeGazeEeg(gazeEegEnv, triggerMatching, gazeOffSamples, gazeNbSamples, eegOffSamples, eegNbSamples, destEegNo, eyelink, eegNaN, false);
	}

	//This is the 7 parameters version which is called from the 5 parameters version
	template <class type> bool	SynchronizeGazeEeg(GazeEegEnv& gazeEegEnv, const TriggerMatching& triggerMatching, const size_t gazeOffSamples, const size_t gazeNbSamples, const size_t eegOffSamples, const size_t eegNbSamples, const std::string& destEeg, AscRawData& eyelink, const type eegNaN, const bool doDrift)
	{
		clock_t				begin = clock();

		MarkerList_t&		currentMarkers = doDrift ? eyelink.mySynchronization.currentMarkersS : eyelink.mySynchronization.currentMarkersN;
		eyelink.mySynchronization.withDrift = doDrift;

		BrainampEEG<type>&	eegObj = *((BrainampEEG<type>*) myEEG);

		Merger<Parser::double_t, type> merger(eyelink.Data(), gazeOffSamples, gazeNbSamples, eyelink.GetNbChannels(), eyelink.AcqSamplingRate(), Parser::gazeNaN, eegObj.data, eegOffSamples, eegNbSamples, myVhdr.GetNbChannels(), AcqSamplingRate(), eegNaN);

		//1) Saves the new .eeg file (save eeg)
		merger.Process(destEeg + ".eeg", gazeEegEnv.eegReference, doDrift); //writes the file to disk

		eyelink.mySynchronization.nbOfSamples = merger.NbOfSamples();
		eyelink.mySynchronization.nbOfChannels = merger.NbOfChannels();
		eyelink.mySynchronization.samplingRate = merger.SamplingRate();

		std::ostringstream ossTrigger, ossOffet;
		size_t	offset = currentMarkers.begin()->offset;
		double	fact = 1.0 / merger.GazeInterpolatorFactor();

		//Correction for the EEG bufferoverflow. We want to remove eytracker events for which we do not have EEG signal
		if (triggerMatching.myEEGBufferOveflowStartOffset >=0 && triggerMatching.myEEGBufferOveflowEndOffset > 0)
		{
			//buffer overflow debug only
			//for (MarkerList_t::iterator it = currentMarkers.begin(); it != currentMarkers.end(); it++)
			//{
			//	if (it->offset >= triggerMatching.myEEGBufferOveflowStartOffset && it->offset <= triggerMatching.myEEGBufferOveflowEndOffset)
			//	{
			//		if (it->IsStimulation() && it->value >= 1000)
			//		{
			//			std::cout << "EEG bufferoverflow event removed: value " << it->value << " offset " << it->offset << std::endl;
			//		}
			//	}
			//} //end debug

			int oldCount = currentMarkers.size();

			currentMarkers.erase(std::remove_if(
				currentMarkers.begin(), currentMarkers.end(),
				[triggerMatching](const CMarker& x) {
				return x.offset >= triggerMatching.myEEGBufferOveflowStartOffset && x.offset <= triggerMatching.myEEGBufferOveflowEndOffset;
			}), currentMarkers.end());

			int newCount = currentMarkers.size();
			std::cout << "Total EEG bufferoverflow events removed: " << oldCount - newCount << std::endl;
		}

		for (MarkerList_t::iterator it = currentMarkers.begin(); it != currentMarkers.end(); it++)
		{
			it->offset -= offset; //make sure the new offset starts from 0

			if (gazeEegEnv.eegReference) //update each value multiplied by the interpolation factor
			{
				it->offset = uint32(::round(it->offset*fact));
				it->duration = uint32(::round(it->duration*fact));
			}

			if (it->offset >= eyelink.mySynchronization.nbOfSamples) //remove markers with invalid offset
			{
				currentMarkers.erase(it);
				break;
			}

			if (it->value < 256) //record all triggers for debug
			{
				ossTrigger << it->value << ' ';
				ossOffet << it->offset << ' ';
			}
		}

		// EK : Adding ET channels in .vhdr
		//2) Saves the new Header file
		if(!myVhdr.SaveGazeEeg(gazeEegEnv.eegFileName, destEeg, eyelink.ChannelNames(), merger.SamplingRate(), merger.NbOfSamples()))
			return MSendError("Brainamp::SynchronizeGazeEeg::myVhdr.SaveGazeEeg");

		//3) Saves the new .vnrk marker file
		if(!myVmrk.SaveGazeEeg(gazeEegEnv.eegFileName, destEeg, currentMarkers, gazeEegEnv.forceDuration1))
			return MSendError("Brainamp::SynchronizeGazeEeg::myVmrk.SaveGazeEeg");

		std::string eventFile = Parser::ReplaceExtention(destEeg, std::string("asc.eeg") + MEventFile);
		eyelink.mySynchronization.eventFile = eventFile;
		eyelink.SaveStimulations(eyelink.mySynchronization.eventFile, currentMarkers);

		eyelink.mySynchronization.matParams.gazeReferenceAsc		= gazeOffSamples;
		eyelink.mySynchronization.matParams.gazeReference           = merger.GazeReference();
		eyelink.mySynchronization.matParams.trueSamplingRate        = uint32(merger.SamplingRate());
		eyelink.mySynchronization.matParams.eegSamplingRate         = uint32(merger.EegSamplingRate());
		eyelink.mySynchronization.matParams.gazeSamplingRate        = uint32(merger.GazeSamplingRate());
		eyelink.mySynchronization.matParams.eegOnGazeSamplingRatio  = triggerMatching.RealSamplingRatio();
		eyelink.mySynchronization.matParams.nbOfChannels            = eyelink.mySynchronization.nbOfChannels;
		eyelink.mySynchronization.matParams.eegNbChannels           = myVhdr.GetNbChannels();
		eyelink.mySynchronization.matParams.gazeNbChannels          = eyelink.GetNbChannels();
		eyelink.mySynchronization.matParams.nbOfSamples             = eyelink.mySynchronization.nbOfSamples;
		eyelink.mySynchronization.matParams.eegNbSamples            = eegNbSamples;
		eyelink.mySynchronization.matParams.gazeNbSamples           = gazeNbSamples;
		eyelink.mySynchronization.matParams.nbOfSegments            = triggerMatching.NbOfSegments();
		eyelink.mySynchronization.matParams.nbOfTriggers            = triggerMatching.NbOfTriggers();
		eyelink.mySynchronization.matParams.nbOfExtraTriggers       = myVmrk.NbExtraStimulations();
		eyelink.mySynchronization.matParams.firstTrigger            = triggerMatching.FirstTrigger();
		eyelink.mySynchronization.matParams.lastTrigger             = triggerMatching.LastTrigger();
		triggerMatching.GetTriggerSynchro(eyelink.mySynchronization.matParams.triggerSynchro, eyelink.GetDebugMode());
		
		//4) Saves a .mat file 
		if(gazeEegEnv.doMatbuilder && !SaveMatFile(eyelink, destEeg))
			return MSendError("Brainamp::SynchronizeGazeEeg::SaveMatFile");
		
		gazeEegEnv.quality.tmpEEG = (double)(clock() - begin) / CLOCKS_PER_SEC;
		
		return true;
	}

	BrainampVmrk&			Vmrk()						{	return myVmrk;					}
	BrainampVhdr&			Vhdr()						{	return myVhdr;					}
private:
	MarkerList_t&			GetTriggerMarkers()			{	return myVmrk.Markers();		}
	const BrainampEegVirt*	EegVirt() const				{	return myEEG;					}
	matvar_t*				GetDataComponents(AscRawData* eyelink = 0);
	matvar_t*				GetEventComponents(const std::string& eventFileName, AscRawData* eyelink = 0);
	bool					ProcessDataSegments(const bool doDataCorrection);

private:
	BrainampVhdr			myVhdr;							//!< mirror of the \a VHDR file
	BrainampVmrk			myVmrk;							//!< mirror of the \a VMRK file
	BrainampEegVirt*		myEEG;							//!< mirror of the \a EEG file
	double					myEegToGazeSamplingRatioReal;	//!< real ration between the sampling rates
	size_t					mySamplesEeg;					//!< nb of EEG samples
	size_t					mySamplesGaze;					//!< nb of gaze samples
};
