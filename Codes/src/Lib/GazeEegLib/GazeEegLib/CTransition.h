/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

/*!	\file	CTransition.h
 	\author	Gelu Ionescu.
 
 	\brief	Transition Event support
*/

#pragma once

#include "Event.h"
#include "Displayable.h"

/*!	\class	CTransition
 
 	\brief	Support for Transition events
	\remark this work for both eyes and for different types of transitions
 */
 
class CTransitionNum : public Event
{
public:
	//! public constructor
	/*! \param type event type.
	 *  \param startTime start timestamp.
	 *  \param endTime end timestamp.
	 *  \param nbEvents number of events of the same type necessary to generate a transition
	 *
	 *	\see eventType_t for event types
	 */
	CTransitionNum(const uint16 value, const uint32 startTime, const uint16 nbEvents);
	
	//! public constructor
	/*! \param type event type.
	 *  \param startTime start timestamp.
	 *  \param endTime end timestamp.
	 *  \param name transition name
	 *  \param timeout associated \a timeout
	 *  \param nbEvents number of events of the same type necessary to generate a transition
	 *
	 *	\see eventType_t for event types
	 */
	CTransitionNum(const uint16 value, const uint32 startTime, const uint32 timeout, const uint16 nbEvents);
	
	//! public constructor
	/*! \param displayable associated displayable with a transition
	 *
	 *	\see eventType_t for event types
	 */
	CTransitionNum(const Displayable& displayable);
	virtual ~CTransitionNum();
	
	uint16			typeAsked;					//!< asked transition type 
	uint32			timeout;					//!< associated \a timeout 
	uint16			nbEvents;					//!< number of events of the same type necessary to generate a transition (generally is 1)
	uint16			stopAttribute;				//!< stop attibute mouse ('L' 'R') etc;
	uint16			posX;						//!< mouse position in X
	uint16			posY;						//!< mouse position in Y
};


class CTransition : public CTransitionNum
{
public:
	//! public constructor
	/*! \param type event type.
	 *  \param startTime start timestamp.
	 *  \param endTime end timestamp.
	 *  \param nbEvents number of events of the same type necessary to generate a transition
	 *
	 *	\see eventType_t for event types
	 */
	CTransition(const uint16 value, const uint32 startTime, const std::string& name, const uint16 nbEvents);
	
	//! public constructor
	/*! \param type event type.
	 *  \param startTime start timestamp.
	 *  \param endTime end timestamp.
	 *  \param name transition name
	 *  \param timeout associated \a timeout
	 *  \param nbEvents number of events of the same type necessary to generate a transition
	 *
	 *	\see eventType_t for event types
	 */
	CTransition(const uint16 value, const uint32 startTime, const std::string& name, const uint32 timeout, const uint16 nbEvents);
	
	//! public constructor
	/*! \param displayable associated displayable with a transition
	 *
	 *	\see eventType_t for event types
	 */
	CTransition(const Displayable& displayable);
	~CTransition();
	
	std::string		name;						//!< transition name (works as un ID) 
};

