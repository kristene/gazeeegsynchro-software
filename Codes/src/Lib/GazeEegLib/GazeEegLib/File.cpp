/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

#include "stdafx.h"

#include "File.h"

#include <sstream>

// File::File
// ------------------
/*!	Constructors of the File class
PARAMETERS
	file	- the file name
	oflag	- the normal opening flags (see open on UNIX)
	pmode	- the normal opening pmode (see open on UNIX)
OUTPUT
	NA
RESULT
	NA
COMMENTS
	Use '!' operator to test if the file is correctly opened
*/
using GazeEeg::File;
File::File() : myHandler(-1)
{
}

File::File(const char* file, const int oflag, const int pmode /* = 0 */) : myHandler(-1)
{
	Open(file, oflag, pmode);
}

File::File(const std::string& file, const int oflag, const int pmode /* = 0 */) : myHandler(-1)
{
	Open(file, oflag, pmode);
}

// File::~File
// -------------------
/*!	Destructor of the File class
*/
File::~File()
{
	Close();
}

/* exists >>>>>>>>>>------------------------------------------ 
PURPOSE
	Test if a named file exists
PARAMETERS
	file	- the file name
OUTPUT
	NA
RESULT
	true	- exists
	false	- otherwise
COMMENTS
	NA
*/
bool File::Exists(const char* file)
{
	struct	_stat statfile;
	int		ret;
	
	return (ret = ::_stat(file, &statfile)) != -1;
}

bool File::IsFile(const char* file)
{
	struct	_stat statfile;
	
	if(_stat(file, &statfile) == -1)
		return MSendError1("File::IsFile::_stat", file);

	return (statfile.st_mode & _S_IFREG) != 0;
}

bool File::IsDirectory(const char* file)
{
	struct	_stat statfile;
	
	if(_stat(file, &statfile) == -1)
		return MSendError1("File::IsDirectory::_stat", file);

	return (statfile.st_mode & _S_IFDIR) != 0;
}
// END exists ---------------------------------------------<<<<<<<<<<

/* size >>>>>>>>>>------------------------------------------ 
PURPOSE
	Get the size of a named or opened file
PARAMETERS
	file	- the file name
OUTPUT
	NA
RESULT
	unsigned long	- file size
	0				- error or file size = 0
COMMENTS
	NA
*/
unsigned long File::Size(const char* file)
{
	struct	_stat statfile;
	int		ret;
	
	return ((ret = ::_stat(file, &statfile)) == -1) ? 0 : statfile.st_size;
}

unsigned long File::Size()
{
	if(myHandler == -1)
		return 0;
		
	struct _stat statfile;
	int		ret;
	
	return ((ret = ::_fstat(myHandler, &statfile)) == -1) ? 0 : statfile.st_size;
}
// END size ---------------------------------------------<<<<<<<<<<

std::string File::GetTime(const char* file, const File::time_type timeType)
{
	struct _stat statfile;
	int		ret;

	if((ret = ::_stat(file, &statfile)) == -1)
		return "";
	
	char buf[1000];
	if(timeType == TIME_CREATED)
		::ctime_s(buf, sizeof(buf), &statfile.st_ctime);
	else if(timeType == TIME_MODIFIED)
		::ctime_s(buf, sizeof(buf), &statfile.st_mtime);
	else if(timeType == TIME_LAST_ACCESS)
		::ctime_s(buf, sizeof(buf), &statfile.st_atime);

	std::string			sbuf(buf);

	int idx;
	while((idx = sbuf.find_first_of(':')) != std::string::npos)
		sbuf.replace(idx, 1, " " );

	std::istringstream	iss(sbuf);
	std::string			weekDay, month;
	int					year, mo, day, hour, min, sec;

	iss	>> weekDay >> month >> day >> hour >> min >> sec >> year;

	if(month == "Jan")
		mo = 1;
	else if(month == "Feb")
		mo = 2;
	else if(month == "Mar")
		mo = 3;
	else if(month == "Apr")
		mo = 4;
	else if(month == "May")
		mo = 5;
	else if(month == "Jun")
		mo = 6;
	else if(month == "Jul")
		mo = 7;
	else if(month == "Aug")
		mo = 8;
	else if(month == "Sep")
		mo = 9;
	else if(month == "Oct")
		mo = 10;
	else if(month == "Nov")
		mo = 11;
	else if(month == "Dec")
		mo = 12;

	sprintf_s(buf, "%04d%02d%02d%02d%02d%02d", year, mo, day, hour, min, sec);

	return buf;
}

bool File::IsEqual(const char* file1, const char* file2)
{
	std::vector<char>	v1;
	std::vector<char>	v2;

	if(!File::Read(file1, v1))
		return MSendError1("File::IsEqual::File::Read", file1);

	if(!File::Read(file2, v2))
		return MSendError1("File::IsEqual::File::Read", file2);

	return (v1 == v2);
}

bool File::Read(const std::string& file, void* data)
{
	if(!File::Exists(file))
		return MSendError1("File::Read::File::Exists", file);

	unsigned long sz = File::Size(file);

	File	pf(file, OPEN_READ);
	if(!pf)
		return MSendError1("File::Read::File::pf", file);

	unsigned long sz1 = pf.Read(data, (unsigned long) sz);

	return sz == sz1;
}

bool File::ReadInString(std::string& data, const std::string& file)
{
	if(!Exists(file))
		return MSendError1("File::ReadInString::Exists", file);
		
	data.resize(Size(file));
		
	return Read(file, (void*) &data[0]);
}

bool File::WriteFromString(std::string& data, const std::string& file)
{
	return Write(file, (void*) &data[0], data.length());
}

bool File::Write(const std::string& file, const void* data, const unsigned long sz)
{
	File	pf(file, OPEN_WRITE, _S_IWRITE );
	if(!pf)
		return MSendError1("File::Write::pf", file);
		
	if(sz == 0)
		return true;

	if(pf.Write(data, sz) != sz)
		return false;

	return true;
}

/* mkdir >>>>>>>>>>------------------------------------------ 
PURPOSE
	Create a directory
PARAMETERS
	file	- the directory name
	pmode	- the normal opening pmode (see mkdir on UNIX)
OUTPUT
	NA
RESULT
	true	- OK
	false	- otherwise
COMMENTS
	NA
*/
bool File::Mkdir(const char* file)
{
	if(File::Exists(file))
		return true;

	if(::_mkdir(file) == -1)
    {	int myErr = errno;
		if (errno != EEXIST)
			return MSendError1("File::Mkdir::EEXIST", file);
	}
	
	return true;
}
// END mkdir ---------------------------------------------<<<<<<<<<<

/* copy >>>>>>>>>>------------------------------------------ 
PURPOSE
	copy a file from a source address to a destination address
PARAMETERS
	dest	- destination address
	src		- source address
OUTPUT
	NA
RESULT
	true	- OK
	false	- otherwise
COMMENTS
	NA
*/
bool File::Copy(const char* src, const char* dest)
{
	std::vector<char>	v;

	if(!File::Read(src, v))
		return MSendError1("File::Copy::File::Read", src);

	if(!File::Write(dest, v))
		return MSendError1("File::Copy::File::Read", dest);

	return true;
}
// END copy ---------------------------------------------<<<<<<<<<<

/* move >>>>>>>>>>------------------------------------------ 
PURPOSE
	move a file from a source address to a destination address
PARAMETERS
	dest	- destination address
	src		- source address
OUTPUT
	NA
RESULT
	true	- OK
	false	- otherwise
COMMENTS
	NA
*/
bool File::Move(const char* src, const char* dest)
{
	return true;//::MoveFile(src, dest) == TRUE;
}

bool File::Rename(const char* oldName, const char* newName)
{
	return ::rename(oldName, newName) == 0;
}
// END move ---------------------------------------------<<<<<<<<<<

/* remove >>>>>>>>>>------------------------------------------ 
PURPOSE
	remove a named file
PARAMETERS
	file	- the name of the file
OUTPUT
	NA
RESULT
	true	- OK
	false	- otherwise
COMMENTS
	NA
*/
bool File::Remove(const char* file)
{
	if(File::Exists(file))
	{	if(::remove(file) == -1)
			return MSendError1("File::Remove::remove", file);
	}
		
	return true;
}
// END remove ---------------------------------------------<<<<<<<<<<

/* open >>>>>>>>>>------------------------------------------ 
PURPOSE
	open a file
PARAMETERS
	file	- the file name
	oflag	- the normal opening flags (see open on UNIX)
	pmode	- the normal opening pmode (see open on UNIX)
OUTPUT
	NA
RESULT
	true	- OK
	false	- otherwise
COMMENTS
	See open/close functions on UNIX
*/
bool File::Open(const char* file, const int oflag, const int pmode /* = 0 */)
{
	if(myHandler == -1)
		::_sopen_s(&myHandler, file, oflag, _SH_DENYNO, pmode);
	else
		Seek(0);	// position the file pointer at the beginning of the file
	
	return myHandler != -1;
}

bool File::Open(const std::string& file, const int oflag, const int pmode /* = 0 */)
{
	return Open(file.c_str(), oflag, pmode);
}
// END open ---------------------------------------------<<<<<<<<<<

/* close >>>>>>>>>>------------------------------------------ 
PURPOSE
	Mount/umount a mountable device
PARAMETERS
	file	- the file name
	oflag	- the normal opening flags (see open on UNIX)
	pmode	- the normal opening pmode (see open on UNIX)
OUTPUT
	NA
RESULT
	true	- OK
	false	- otherwise
COMMENTS
	See mount/umount functions on UNIX
*/
bool File::Close()
{
	// close the file if it is opened
	if(myHandler != -1)
	{	int val = ::_close(myHandler);
		myHandler = -1;
		
		return val == 0;
	}
		
	return false;
}
// END close ---------------------------------------------<<<<<<<<<<

/* read >>>>>>>>>>------------------------------------------ 
PURPOSE
	read a number of bytes to a memory buffer
PARAMETERS
	ptr		- the memory buffer address
	dim		- the memory buffer dimension
OUTPUT
	NA
RESULT
	true	- OK
	false	- otherwise
COMMENTS
	See read functions on UNIX
*/
unsigned long File::Read(void* ptr, const unsigned long dim)
{
	unsigned long ret = 0;
	
	if(myHandler != -1)
		ret = ::_read(myHandler, ptr, dim);
		
	return ret;
}
// END read ---------------------------------------------<<<<<<<<<<

/* write >>>>>>>>>>------------------------------------------ 
PURPOSE
	write a number of bytes from a memory buffer
PARAMETERS
	ptr		- the memory buffer address
	dim		- the memory buffer dimension
OUTPUT
	NA
RESULT
	true	- OK
	false	- otherwise
COMMENTS
	See write functions on UNIX
*/
unsigned long File::Write(const void* ptr, const unsigned long dim)
{
	if(myHandler != -1)
	{	unsigned long val = ::_write(myHandler, ptr, dim);
		return val;
	}
		
	return 0;
}
// END write ---------------------------------------------<<<<<<<<<<

/* write >>>>>>>>>>------------------------------------------ 
PURPOSE
	Change the access position for an open file
PARAMETERS
	pos		- offset 
	whence	- how o interpret 'pos' (seek_SET, seek_END)
OUTPUT
	NA
RESULT
	true	- OK
	false	- otherwise
COMMENTS
	See seek functions on UNIX
*/
bool File::Seek(const long pos, int whence /* = seek_SET */)
{
	if(myHandler != -1)
	{	int val = ::_lseek(myHandler, pos, whence);
		return val != -1;
	}
		
	return MSendError("File::Seek");
}
// END seek ---------------------------------------------<<<<<<<<<<

/* tell >>>>>>>>>>------------------------------------------ 
PURPOSE
	Return a file offset for a file descriptor
PARAMETERS
	NA
OUTPUT
	NA
RESULT
	unsigned long	- the file offset for a file descriptor
COMMENTS
	See seek functions on UNIX
*/
unsigned long File::Tell()
{
	if(myHandler != -1)
	{	unsigned long val = ::_tell(myHandler);
		return val == (unsigned long) -1 ? 0 : val;
	}
		
	return 0;
}
// END tell ---------------------------------------------<<<<<<<<<<

std::string File::Getline()
{
	std::string 	result;
	char			val=0;

	while(true)
	{	if((Read(&val, 1) == 0) || (val == '\n'))
			break;
		
		result += val;
	}
	
	return result;
}

bool Error(const std::string& text, const std::string& text1 /*= ""*/, const std::string& file /*= __FILE__*/, const int line /*= __LINE__*/)
{
	std::cout << std::endl << "ERROR in : " << file << " (" << line << ") " << text << " " << text1 << std::endl;

	return false;
}
void Warning(const std::string& text, const std::string& text1 /*= ""*/, const std::string& file /*= __FILE__*/, const int line /*= __LINE__*/)
{
	std::cout << std::endl << "WARNING in : " << file << " (" << line << ") " << text << " " << text1 << std::endl;
}
