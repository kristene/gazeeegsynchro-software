/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

// stdafx.h�: fichier Include pour les fichiers Include syst�me standard,
// ou les fichiers Include sp�cifiques aux projets qui sont utilis�s fr�quemment,
// et sont rarement modifi�s
//

#pragma once

#include <math.h>

#include <iostream>
#include <vector>
#include <map>
#include <sstream>
#include <fstream>
#include <limits>

#include <matio.h>
#include <float.h>
#include <algorithm>
#include <functional>
#include <algorithm>
#include <iomanip>

#include "Version.hpp"

#include "Parser.h"


typedef signed		char	int8;
typedef unsigned	char	uint8;
typedef signed		short	int16;
typedef unsigned	short	uint16;
typedef signed		int		int32;
typedef unsigned	int		uint32;
typedef signed		__int64	int64;
typedef unsigned	__int64	uint64;


/*
	ENUM_MAP helpers
*/

template<typename T> struct EnumMapInitHelper
{
    T& data;
    
	EnumMapInitHelper(T& d) : data(d) {}
    EnumMapInitHelper& operator() (typename T::key_type const& key, typename T::mapped_type const& value)
    {
        data[key] = value;
        
		return *this;
    }
};

template<typename T> EnumMapInitHelper<T> EnumMapInit(T& item)
{
    return EnumMapInitHelper<T>(item);
}

#define	ENUM_ID_TO_STRING_PAIR(id)	id, #id	
#define	ENUM_STRING_TO_ID_PAIR(id)	#id, id	
#define	ENUM_TO_STRING(id)			#id

inline double round(double x)
{
    int temp; temp = (x >= 0. ? (int)(x + 0.5) : (int)(x - 0.5));
    
	return (double) temp;
}

extern std::string	MyGetPrivateProfileString(const std::string& section, const std::string& key, const std::string& defVal, const std::string& fileName);
extern int			MyGetPrivateProfileInt(const std::string& section, const std::string& key, const int defVal, const std::string& fileName);
extern double		MyGetPrivateProfileDouble(const std::string& section, const std::string& key, const double defVal, const std::string& fileName);


