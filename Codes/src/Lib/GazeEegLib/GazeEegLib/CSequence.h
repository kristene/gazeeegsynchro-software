#pragma once

#include "Event.h"

class CSequence : public Event
{
public:
	CSequence(const uint16 value, const uint32 startTime, const uint16 index, const std::string& name);
	CSequence(const uint32 startTime, std::vector<char*>::const_iterator& it);
	~CSequence(void);

	matvar_t*	GetComponents();

	uint16		index;
	std::string	name;
};

