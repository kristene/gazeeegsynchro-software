/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

#include "StdAfx.h"

#include "MatComponents.h"
#include "StimulationMap.h"

void StimulationMap::Add(const uint16 first, const std::string& name)
{
	iterator it = find(first);
	if(it == end())
		(*this)[first]	= triggerPair_t(name, 1);
	else
		it->second.second++;
}

void StimulationMap::AddComponents(MatComponents& typeComponents)
{
	for(iterator it=begin(); it != end(); it++)
		typeComponents.AddVariable(it->second.first,	MAT_C_UINT16,	uint16(it->first));
}

bool StimulationMap::Write(const std::string& fileName)
{
	GazeEeg::File::Remove(fileName);
	
	std::ofstream ofs(fileName.c_str());
	if(ofs.bad())
		return MSendError1("Opening log file : ", fileName);

	for(iterator it=begin(); it != end(); it++)
	{	ofs.width(10);
		ofs << std::left << it->first;
		ofs.width(30);
		ofs << std::left << it->second.first;
		ofs << ' ' << it->second.second << std::endl;
	}

	return true;
}

bool StimulationMap::Read(const std::string& fileName)
{
	if(!GazeEeg::File::Exists(fileName))
		return MSendError1("File doesn't exist : ",  fileName);

	std::ifstream ifs(fileName.c_str());
	if(ifs.bad())
		return MSendError1("Opening log file : ", fileName);

	while(true)
	{	uint16		frst;
		std::string name;
		uint16		sec;

		ifs >> frst >> name >> sec;
		if(ifs.bad() || ifs.eof())
			break;
		
		(*this)[frst]	= triggerPair_t(name, sec);
	}

	return true;
}
