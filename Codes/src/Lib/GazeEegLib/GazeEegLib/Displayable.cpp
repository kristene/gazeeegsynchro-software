/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

#include "StdAfx.h"

#include "Displayable.h"

DisplayableNum::DisplayableNum(const uint16 value, const uint32 startTime)
	: Event(value, startTime)
	, nbFrames(1)
	, playedFrames(1)
	, firstFrame(0)
	, frameDuration(0)
	, leftRect(0)
	, topRect(0)
	, rightRect(0)
	, bottomRect(0)
	, tTimeout(0)
	, tNbEvents(1)
	, tType(transitionTime)
	, tTypeEnd(transitionTime)
	, tStopAttribute(0)
	, tPosX(0)
	, tPosY(0)
{
}

DisplayableNum::DisplayableNum(const uint16 value, const uint32 startTime, const uint16 nbFrames, const uint16 frameDuration, const uint16 leftRect, const uint16 topRect, const uint16 rightRect, const uint16 bottomRect)
	: Event(value, startTime)
	, nbFrames(nbFrames)
	, playedFrames(1)
	, firstFrame(0)
	, frameDuration(frameDuration)
	, leftRect(leftRect)
	, topRect(topRect)
	, rightRect(rightRect)
	, bottomRect(bottomRect)
	, tTimeout(0)
	, tNbEvents(1)
	, tType(transitionTime)
	, tTypeEnd(transitionTime)
	, tStopAttribute(0)
	, tPosX(0)
	, tPosY(0)
{
}

DisplayableNum::DisplayableNum(const uint32 startTime)
	: Event(displayableImage, startTime)
	, playedFrames(1)
	, firstFrame(0)
	, tType(transitionTime)
	, tTypeEnd(transitionTime)
	, tStopAttribute(0)
	, tPosX(0)
	, tPosY(0)
{
}

DisplayableNum::~DisplayableNum()
{
}



Displayable::Displayable(const uint16 value, const uint32 startTime, const std::string& name, const std::string& relativePath)
	: DisplayableNum(value, startTime)
	, name(name)
	, relativePath(relativePath)
{
}

Displayable::Displayable(const uint16 value, const uint32 startTime, const std::string& name, const std::string& relativePath, const uint16 nbFrames, const uint16 frameDuration, const uint16 leftRect, const uint16 topRect, const uint16 rightRect, const uint16 bottomRect)
	: DisplayableNum(value, startTime, nbFrames, frameDuration, leftRect, topRect, rightRect, bottomRect)
	, name(name)
	, relativePath(relativePath)
{
}

Displayable::Displayable(const uint32 startTime, std::vector<char*>::const_iterator& it)
	: DisplayableNum(startTime)
{
	if(strstr(*it, "time_transition"))
		tType		= transitionTime;
	else if(strstr(*it, "mouse_transition"))
		tType		= transitionMouse;
	else if(strstr(*it, "keyboard_transition"))
		tType		= transitionKeyboard;
	else if(strstr(*it, "gaze_transition"))
		tType		= transitionGaze;
	else if(strstr(*it, "ssacc_transition"))
		tType		= transitionSSacc;
	else if(strstr(*it, "esacc_transition"))
		tType		= transitionESacc;
	else if(strstr(*it, "sfix_transition"))
		tType		= transitionSFix;
	else if(strstr(*it, "efix_transition"))
		tType		= transitionEFix;
	else if(strstr(*it, "special_transition"))
		tType		= transitionSpecial;

	tTypeEnd		= tType;

	it++;
	tName			= *it++;
	tTimeout		= Parser::GetValueUint32(*it++);
	tNbEvents		= Parser::GetValueInt16(*it++);

	it++;
	if(strstr(*it, "VIDEOWITHSOUND"))
		value		= displayableVideoSound;
	else if(strstr(*it, "VIDEOWITHOUTSOUND"))
		value		= displayableVideoNoSound;
	else if(strstr(*it, "TEXT"))
		value		= displayableText;
	
	it++;
	name			= *it++;
	
	relativePath	= *it++;
	if(Parser::strcmp(relativePath.c_str(), "SPECIAL"))
		value		= displayableSpecial;

	nbFrames		= Parser::GetValueInt16(*it++);
	frameDuration	= uint16(Parser::GetValueUint32(*it++)/1000);
	leftRect		= uint16(Parser::GetValueInt16(*it++));
	topRect			= uint16(Parser::GetValueInt16(*it++));
	rightRect		= uint16(Parser::GetValueInt16(*it++));
	bottomRect		= uint16(Parser::GetValueInt16(*it++));
}

void Displayable::End(const uint32 endTime, std::vector<char*>::const_iterator& it)
{
	this->endTime		= endTime;

	if(Parser::strcmp(*it, "TIMEOUT"))
		tTypeEnd		= transitionTime;
	else if(Parser::strcmp(*it, "KEYBOARD"))
	{	it++;
		tStopAttribute	= (::strlen(*it) == 1)? ' ' : (*it)[1];
		tTypeEnd		= transitionKeyboard;
	}
	else if(Parser::strcmp(*it, "MOUSE"))
	{	it++;
		tStopAttribute	= **it++;
		tPosX			= uint16(Parser::GetValueInt16(*it++));
		tPosY			= uint16(Parser::GetValueInt16(*it++));
		tTypeEnd		= transitionMouse;
	}
	else if(Parser::strcmp(*it, "GAZE"))
	{	it++;
		tStopAttribute	= **it++;
		tTypeEnd		= transitionGaze;
	}
}
// Nathalie 20170529
Displayable::Displayable(const uint32 startTime, const std::string& imageName)
	: DisplayableNum(startTime)
{
	tType			= transitionKeyboard;
	tTypeEnd		= tType;
	tName			= "";
	tTimeout		= 1000;
	tNbEvents		= 1;
	
	name			= imageName;	
	relativePath	= "";
	nbFrames		= 1;
	frameDuration	= 0;
	leftRect		= 0;
	topRect			= 0;
	rightRect		= 1023;
	bottomRect		= 767;
}
void Displayable::End(const uint32 endTime, const uint16 stopAttribute)
{
	this->endTime	= endTime;

	tStopAttribute	= stopAttribute;
	tTypeEnd		= transitionKeyboard;
}

void Displayable::SetTransition(const uint16 value, const std::string& name, const uint32 timeout, const uint16 nbEvents)
{
	tType		= value;
	tTypeEnd	= value;
	tName		= name;
	tTimeout	= timeout;
	tNbEvents	= nbEvents;
}

Displayable::~Displayable()
{
}
