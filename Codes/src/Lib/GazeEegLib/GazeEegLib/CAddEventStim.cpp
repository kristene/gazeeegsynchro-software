/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

#include "StdAfx.h"

#include "CAddEventStim.h"
#include "MemoryFile.h"

CAddEventStim::CAddEventStim(std::ifstream& isf, const std::string& firstLine)
{
	char			line[10000];
	std::string		str;
	std::string		before;
	std::string		after;
	iterator		it;

	Parser::GetPair(before, after, firstLine, '=');
	(*this)[before]	= after;

	while(true)
	{	isf.getline(line, sizeof(line));
		Parser::GetPair(before, after, line, '=');
		(*this)[before]	= after;

		it				= find("referenceTrigger");
		if(it != end())
			break;
	}

	referenceTriggerNum		= atoi(it->second.c_str());
	isReferenceTriggerNum	= referenceTriggerNum != 0;
}

bool CAddEventStim::IsValid()
{
	if(!GazeEeg::File::Exists(Get("eegRejectFileFullPath")))
		return ::Error("File does not exist : ", Get("eegRejectFileFullPath"));
	
	if(Get("outputFullPath").empty())
	{	std::string path, name, ext;
		Parser::ParsePath(path, name, ext, Get("eegRejectFileFullPath"));

		(*this)["outputFullPath"] = path;
	}
	else if(!GazeEeg::File::Exists(Get("outputFullPath")))
	{	if(!Parser::CreatePath(Get("outputFullPath")))
			return ::Error("File does not exist : ", Get("outputFullPath"));
	}
	
	if(!GazeEeg::File::Exists(Get("synchroFileFullPath")))
		return ::Error("File does not exist : ", Get("synchroFileFullPath"));
	
	return true;
}

bool CAddEventStim::GetCleanedEpochs(std::vector<size_t>& cleanedEpocs)
{
	cleanedEpocs.clear();
	
	std::string fileName = Get("cleanFileFullPath");
	if(fileName.empty() || !GazeEeg::File::Exists(fileName))
		return true;

	MemoryFile	memFile;
			
	if(!memFile.Read(fileName))
		return false;

	memFile.Trim();

	std::istringstream iss(memFile);
	while(!iss.eof())
	{	size_t val;
		iss >> val;
		
		cleanedEpocs.push_back(val - 1);
	}

	return true;
}

bool CAddEventStim::ReadEvents(const std::string& eventFileName)
{
	if(!eventDictAll.Read(eventFileName))
		return false;

	StimulationMap::iterator it = eventDictAll.find(referenceTriggerNum);
	
	if(it == eventDictAll.end())
		return false;

	return true;
}

AddEventStimList::AddEventStimList(const std::string& srcFileName)
{
	std::ifstream isf(srcFileName.c_str());

	while(true)
	{	char			line[10000];
		std::string		str;

		do
		{	isf.getline(line, sizeof(line));
			str	= line;
		} while(str.empty() && !isf.eof());

		if(isf.eof())
			break;

		push_back(CAddEventStim(isf, str));
	}
}
