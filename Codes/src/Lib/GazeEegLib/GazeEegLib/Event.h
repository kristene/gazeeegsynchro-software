/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

/*!	\file	Event.h
 	\author	Gelu Ionescu.
 
 	\brief	Generic Event support
*/

#pragma once

#include "MatComponents.h"

#define	 MEventFile		".event.txt"

/*!	\def	DEF_BIT
	\brief	prepare an acquisition flag by shifting left "1" several times
	\param	bit flag position; accepted range [0,7] 
 */
/*!	\def	DEF_BITS
	\brief	prepare an acquisition mask by shifting left "1" several times combining severat successive bits
	\param	startBit starting flag position; accepted range [0,7] 
	\param	nbBits number of successive flags

	\remark startBit + nbBits sould fall in the [0,7] intervall
 */
#define	DEF_BIT(bit)				(1 << bit)
#define	DEF_BITS(startBit, nbBits)	(((1 << nbBits) - 1) << startBit)

/*!	\enum	acqFlags_t
	\brief	acquisition flags
 */
typedef enum
{	fRecording			= DEF_BIT(0),	//!< recording mode (not in calibration & drift correction mode)
	fEyeL				= DEF_BIT(1),	//!< valid left eye
	fEyeR				= DEF_BIT(2),	//!< valid right eye
	fEvent				= DEF_BIT(3),	//!< generic event occurs
	fFrame				= DEF_BIT(4),	//!< frame in video acquisition
	fOutOfScreen		= DEF_BIT(5),	//!< error : out of screen eye position
	fOutOfDisplayable	= DEF_BIT(6),	//!< warning : out of displayable eye position
	fCalibDrift			= DEF_BIT(7),	//!< calibration mode
	fEyeLR				= (fEyeL | fEyeR),	//!< both eyes mask,
} acqFlags_t;

/*!	\enum	attribute_t
	\brief	transition attributes
 */
typedef enum
{	mouseLeft			= 'L',			//!< mouse left button
	mouseRight			= 'R',			//!< mouse right button
	gazeInside			= 'I',			//!< gaze inside zone
	gazeOutside			= 'O',			//!< gaze outside zone
} attribute_t;

/*!	\enum	eventType_t
	\brief	definition of the logical events
 */
typedef enum
{	eyetrackerEvents	= 1000,				//!< eyetracker events zone
	displayableSpecial	= eyetrackerEvents,	//!< special image : cross, circle, filled circle, background, foreground
	displayableImage,					//!< image : bmp, jpg, etc.
	displayableText,					//!< text : configurable fonts, positions, colors
	displayableVideoSound,				//!< video : configurable positions, with sound
	displayableVideoNoSound,			//!< video : configurable positions, without sound
	displayableFrame,					//!< video frame
	transitionTime,						//!< transition timeout  1006
	transitionMouse,					//!< transition mouse
	transitionKeyboard,					//!< transition keyboard
	transitionGaze,						//!< transition gaze
	transitionSSacc,					//!< transition start of saccade
	transitionESacc,					//!< transition end of saccade
	transitionSFix,						//!< transition start of fixation
	transitionEFix,						//!< transition end of fixation
	transitionSpecial,					//!< transition special
	flowSequence,						//!< flow secquence      1015
	flowFixationLeft,					//!< flow left fixation  1016 
	flowFixationRight,					//!< flow right fixation 1017
	flowSaccadeLeft,					//!< flow left saccade   1018
	flowSaccadeRight,					//!< flow right saccade  1019
	flowBlinkLeft,						//!< flow left blink     1020
	flowBlinkRight,						//!< flow right blink    1021
	phaseLearning,						//!< phase learning
	phaseTesting,						//!< phase testing
	recordStep,							//!< recording step
	eventStim = 2000,					//!< event stim zone
} eventType_t;


/*!	\enum	misc_t
	\brief	miscellaneous types
 */
typedef enum
{	ChannelNameSize		= 16,			//!< max size for channel names
	NameSize			= 50,			//!< max size for displayable names
	PathSize			= 256,			//!< max size for displayable paths
} misc_t;
		
/*!	\class	Event
 
	\brief Basic structure for an acquisition event
 */
 
class Event
{
public:
	//! protected constructors
    /*! \param type event type
	 *  \param startTime start timestamp
	 *  \param endTime end timestamp
	 *
	 *	\overload Event constructors
	 *
	 *	\see eventType_t for event types
	 *	\remark if punctual event (ex. trigger) the \a endTime parameter is not necessary
	 *			and the first constructor will be used
	 */
	Event(const uint16 value, const uint32 startTime);
	Event(const uint16 value, const uint32 startTime, const uint32 endTime);

public:
	virtual ~Event(void);

	uint16	value;			//!< event value; \see {eventType_t} for event types 
	uint32	startTime;		//!< start timestamp.
	uint32	endTime;		//!< end timestamp
	uint32	fseek;
};
