/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

#include "StdAfx.h"

#include "File.h"
#include "Ttl.h"

Ttl::Ttl()
{
}

Ttl::~Ttl()
{
}

bool Ttl::Debug(const std::vector<Trigger*>& eyelinkTrigs, const std::string& ascFileName)
{
	if(eyelinkTrigs.empty())
		return true;
	
	std::string		path, name, ext;
	Parser::ParsePath(path, name, ext, ascFileName);
	
	std::string ttlFileName = Parser::FullPath(path, name, "ttl");

	MSendDebug1("Processing TTL  file: ", ttlFileName);

	std::ofstream	ofs(ttlFileName.c_str(), std::ios::out | std::ios::trunc);
	if(ofs.good())
	{	::srand((unsigned int) ::time(NULL));
		
		unsigned int	first = (*eyelinkTrigs.begin())->startTime;
		ofs << "0 " << (*eyelinkTrigs.begin())->value << std::endl;
		for(std::vector<Trigger*>::const_iterator it=eyelinkTrigs.begin()+1; it != eyelinkTrigs.end(); it++)
		{	unsigned int	last	= (*it)->startTime - first;
			int				rnd		= int(2000.0*(double(::rand())/RAND_MAX));
			__int64			val		= 1000*__int64(last) - rnd;
			ofs << val << " " << (*it)->value << std::endl;
	}	}

	return true;
}

bool Ttl::Read(const std::string& ascFileName)
{
	std::string		path, name, ext;
	Parser::ParsePath(path, name, ext, ascFileName);
	
	std::string ttlFileName = Parser::FullPath(path, name, "ttl");

	std::ifstream ifs(ttlFileName.c_str());
	while(true)
	{	if(!ifs.good())
			break;

		uint64	tmp;
		ifs	>> tmp;

		if(!ifs.good())
			break;

		uint16	val;
		ifs	>> val;

		if(!ifs.good())
			break;

		double	tmpD	= double(tmp)/1000.0;
		uint32	tmp32	= uint32(::round(tmpD));

		push_back(Trigger(tmp32, val));
	}
		
	return true;
}

void Ttl::GetTriggerMarkers(MarkerList_t& markerList) const
{
	markerList.clear();
	markerList.reserve(size());

	int	index = 0;
	for(const_iterator it=begin(); it != end(); it++)
		markerList.push_back(CMarker("", it->value, it->startTime, 0, 0));
}
