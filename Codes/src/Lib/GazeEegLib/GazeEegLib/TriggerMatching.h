/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

#pragma once

#include "Lcs.h"
#include "Marker.h"
#include "Point2d.h"

#include <algorithm>
#include <iomanip>

class GazeEegEnv;

class TriggerMatching
{
public:
	class TriggerSynchro
	{
	public:
		std::vector<uint16>	value;
		std::vector<uint32>	gazeOffset;
		std::vector<uint32>	eegOffset;
		std::vector<int16>	diffs;
		uint32				gazeFirst;
		uint32				eegFirst;
		double				R2;
	};

public:
	TriggerMatching(MarkerList_t& stim1, const double acqF1, MarkerList_t& stim2, const double acqF2, const std::vector<MarkerList_t::iterator>* segments = 0); 
	virtual ~TriggerMatching();

	size_t	FirstTrigger() const	{	return myStimuli1[GetIndexes().begin()->index1].value;	}
	size_t	LastTrigger() const		{	return myStimuli1[GetIndexes().rbegin()->index1].value;	}
	size_t	NbOfTriggers() const	{	return GetIndexes().size();								}
	size_t	NbOfSegments() const	{	return mySegments ? mySegments->size() : 0;				}

	template <class type> void CorrectData(std::vector<type>& data, const uint32 nbChannels, const type valNaN)
	{
		if(mySegments->size() <= 1)
			return;

		for(std::vector<MarkerList_t::iterator>::const_reverse_iterator it=mySegments->rbegin(); it != mySegments->rend()-1; it++)
		{	uint32 pos		= (*it)->offset*nbChannels;
			uint32 nbS		= (*it)->samplesInsertedBefore;
			uint32 nb		= nbS*nbChannels;

			data.insert(data.begin() + pos, nb, valNaN);
		}

		for(std::vector<MarkerList_t::iterator>::const_iterator it=mySegments->begin(); it != mySegments->end(); it++)
		{	std::cout	<< "Segment "							<< std::setw(2) << (it - mySegments->begin() + 1)
						<< " Offset = "							<< std::setw(7) << (*it)->offset
						<< " SamplesInserted = "				<< std::setw(7) << (*it)->samplesInsertedBefore
						<< " Correction = "						<< std::setw(4) << (*it)->correctionInsertedBefore
						<< std::endl;				
		}
		std::cout	<< std::endl;
	}
	
	bool LongestCommonSubstring(GazeEegEnv& gazeEegEnv, const std::string& prompt);
	void GetTriggerSynchro(TriggerSynchro& triggerSynchro, const bool doDebug) const;

	void Dump()
	{
		std::cout << "LCS   Triggers = " << myLcs.GetIndexes().size() << std::endl;
	}
	const std::vector<Lcs::Indexes>&	GetIndexes() const			{	return myLcs.GetIndexes();		}
	const double						RealSamplingRatio()	const	{	return myIdealSamplingRatio;	}

private:
	bool	MatchTriggers(GazeEegEnv& gazeEegEnv);
	void	DoCorrection(GazeEegEnv& gazeEegEnv);
	void	ProcessSegmentIndexes();
	double	GetSlope(std::vector<MarkerList_t::iterator>::const_iterator& first, std::vector<MarkerList_t::iterator>::const_iterator& last);
	double	GetEnergy(MarkerList_t::iterator& segmentIt, const uint32 first1, const uint32 first2, const int shiftLoop);
	void	GetEnergy(int& shift, MarkerList_t::iterator& segmentIt, const uint32 first1, const uint32 first2, const double slope, const int incrShift);
	void	SetQualityFactor(TriggerSynchro& triggerSynchro) const;
	void	ProcessArticleHisto(GazeEegEnv& gazeEegEnv, const std::vector<Lcs::Indexes>& indexes);
	int     BufferOveflowSectionConstruct(std::vector<short>	source, std::vector<short> target, int startpos,bool DirectionIncreasing, int maxValueSourceStream);
	void	ApplyBufferOverflowCorrection();
	void    WriteErrorHistograms(std::vector<int> &triggers, const std::vector<double> &d1);

public:
	Lcs											myLcs;
	const std::vector<MarkerList_t::iterator>*	mySegments;
	MarkerList_t&								myStimuli1;
	MarkerList_t&								myStimuli2;
	double										myIdealSamplingRatio;
	double										myRealSamplingRatio;
	size_t										myFirstSegment;
	size_t										myLastSegment;

    signed long								myEEGBufferOveflowStartOffset;
	signed long								myEEGBufferOveflowEndOffset;
};
