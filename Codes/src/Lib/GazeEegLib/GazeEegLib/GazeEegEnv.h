/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

#pragma once

#include "Parser.h"
#include "CAddEventStim.h"

class GazeEegEnv
{
public:
	
	class Aes
	{
	public:
		Aes() : doIt(false) {}

	public:
		bool						doIt;
		std::string					srcFile;
		std::string					dictFile;
		AddEventStimList::iterator	it;
	};

	class Quality
	{
	public:
		Quality() : doIt(false), tmpET(0.0), tmpEEG(0.0), tmpSync(0.0), r2(0.0), histogram(11, int(0)) {}

		bool	Process();
	public:
		bool						doIt;
		std::string					srcFile;
		double						tmpET;
		double						tmpEEG;
		double						tmpSync;
		double						r2;
		std::vector<int>			histogram;
	};

	class Article
	{
	public:
		typedef enum
		{	HALF_WIDTH	= 5,
			FULL_WIDTH	= (2*HALF_WIDTH + 1),
		} width_t;
	public:
		Article() : doIt(false), indHist(0), histTtlEegEt(3*FULL_WIDTH, int(0)) {}

		bool	Process();
	public:
		bool						doIt;
		std::string					srcFile;
		size_t						indHist;
		std::vector<int>			histTtlEegEt;
	};

public:
	GazeEegEnv()
		: falseBlinkDuration(0)
		, isAscFile(false)
		, onPupil(false)
		, forceDuration1(true)
		, eegReference(false)
		, onVideoframe(false)
		, doNoDrift(false)
		, doPause(false)
		, doDebug(false)
		, doMatbuilder(false)
		, doTtl(false)
		, doClassicLCS(false)
		, doVelocityData(false)
		, doRounding(false)
	{}

	void ProcessOutputMatFiles();
	bool			DoAes()	const {	return aes.doIt;	}

	
	size_t			falseBlinkDuration;
	bool			isAscFile;
	bool			onPupil;

	static std::string LogFolder;

	std::string		eventStimFileName;
	std::string		triggerFileName;
	std::string		ttlFileName;
	
	std::string		outputPath;

	std::string		gazeFileName;
	std::string		gazeMatFileName;
	
	std::string		eegFileName;
	std::string		eegMatFileName;

	std::string		eventFileName;
	std::string		error;

	bool			forceDuration1;
	bool			eegReference;
	bool			onVideoframe;

	// optional
	bool			doNoDrift;
	bool			doPause;
	bool			doDebug;
	bool			doMatbuilder;
	bool			doTtl;
	bool			doClassicLCS;
	bool			doVelocityData;
	bool            doRounding; //used to avoid truncating the decimal part of the value. Instead the value is rounded. Used when reading and saving to a int16 .eeg file.

	// AddEventStim
	Aes				aes;
	Quality			quality;
	Article			article;
};
