/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

/*!	\file	AscRawData.h
 	\author	Gelu Ionescu.
 
 	\brief	Support for \a Eyelink data acquisition
*/
#pragma once

#include "GazeEegEnv.h"
#include "AscHeader.h"
#include "MatComponents.h"
#include "Blink.h"
#include "Sequences.h"
#include "Displayables.h"
#include "Frame.h"
#include "Frames.h"
#include "Transitions.h"
#include "FixationSaccades.h"
#include "Blinks.h"
#include "AcqPhases.h"
#include "Records.h"
#include "Triggers.h"
#include "TriggerDef.h"
#include "Parser.h"
#include "Interpolator.h"
#include "StimulationMap.h"
#include "TriggerMatching.h"

/*!	\class	AscRawData
 
 	\brief	\a Eyelink data acquisition structure
 */
 
class AscRawData
{
public:
	class EventTypeMap : public markerMap_t
	{
	public:
		EventTypeMap();
	};
	
public:
	class Synchronization
	{
	public:
		class MatParams
		{
		public:
			uint32							gazeReferenceAsc;
			uint32							gazeReference;
			uint32							trueSamplingRate;
			uint32							eegSamplingRate;
			uint32							gazeSamplingRate;
			double							eegOnGazeSamplingRatio;
			uint32							nbOfChannels;
			uint32							eegNbChannels;
			uint32							gazeNbChannels;
			uint32							nbOfSamples;
			uint32							eegNbSamples;
			uint32							gazeNbSamples;
			uint32							nbOfSegments;
			uint32							nbOfTriggers;
			uint32							nbOfExtraTriggers;
			uint32							firstTrigger;
			uint32							lastTrigger;
			TriggerMatching::TriggerSynchro	triggerSynchro;
		};

	public:
		Synchronization(){}

	public:
		uint32			nbOfSamples;
		uint32			nbOfChannels;
		double			samplingRate;
		std::string		eventFile;
		bool			withDrift;
		MarkerList_t	currentMarkersS;
		MarkerList_t	currentMarkersN;

	// synchro params to put in MAT file
	public:
		MatParams		matParams;
	};
	/*!	\enum	version_t
		\brief	process version of the \a ASC file
		\remark	This software had different versions up to the last stable version 
				It contains the date of the version
	 */
	typedef enum
	{	CORRECT_DIM			= 2,
		CORRECT_DIM_PLUS	= (CORRECT_DIM + 1),
	} correct_t;

public:
	//! public constructor & destructor
	AscRawData();
	~AscRawData();

	//! static function that tests the validity of the \a EDF Eyelink file
    /*! \param ascFileName source acquisition file
		
		\param matFileName starting from \a srcFileName file, returns the name of the \a MAT file
		\param trigFileName if epmty, starting from \a srcFileName file, returns the name of the \a EXT file
				containing the \a triegger names
		
		\return boolean
	 */
	bool	IsValid(GazeEegEnv& gazeEegEnv, const std::string& srcFilename); 

	//! function that processes the mandatary \a ASC file
    /*! \param ascFileName source \a ASC acquisition file
		\param valNaN value of the invalid data called \a NaN (not a number)

		\return  boolean 
	 */
	const	AscHeader& Header() const {	return myHeader;	}
	bool	Process(GazeEegEnv& gazeEegEnv);
	
		//! function that creates the \a MAT file recognized by \a Matlab
    /*! \param matFileName \a MAT destination file name	
		\param compress boolean that controls the data compression (avoid it)
		\param forceRaw for big anount of data, put the raw data in a separate file instead of inserting it in the resulting \a MAT file

		\return  boolean 
	 */
	bool	SaveMatFile(GazeEegEnv& gazeEegEnv);
	
	//! returns the acquisition sampling rate
	double	AcqSamplingRate() const				{	return 1000.0 / myHeader.msecPerSample;	}
	
	//! returns the number of acquisition samples
	uint32	GetNbSamples() const				{	return myNbSamples;						}
	
	//! returns the number of acquisition channels
	uint16	GetNbChannels()	const				{	return myNbChannels;					}
	
	//! returns the data vector
	std::vector<Parser::double_t>&	Data()		{	return myData;							}
	
	//! returns a vector of channel names
	const std::vector<std::string>&	ChannelNames() const {	return myChannelNames;			}
	
	//! returns the screen width in pixels
	uint16	ScreenWidth() const					{	return myHeader.screenWidth;			}			
	
	//! returns the screen height in pixels
	uint16	ScreenHeight() const				{	return myHeader.screenHeight;			}
	
	//! returns the trigger marker list
	void	GetTriggerMarkers(MarkerList_t& markerList);
	void	GetEventMarkers(MarkerList_t& markerList);
	
	Parser::double_t		GetValNaN()					{	return myValNaN;				}
	bool					SaveMarkers(markerMap_t& markerMap, const std::string& destFile, const MarkerList_t& markers);
	bool					SaveStimulations(const std::string& destFile, const MarkerList_t& markers);
	static bool				SaveMarkers(markerMap_t& markerMap, const std::string& destFile);
	static bool				LoadMarkers(markerMap_t& markerMap, const std::string& srcFile);
	uint32					BeginTimestamp()			{	return myStartTimestamp;		}
	uint32					EndTimestamp()				{	return myEndTimestamp;			}
	matvar_t*				GetDataParamsComponentsForSynchro() const;	
	matvar_t*				GetDataComponentsForSynchro() const;	
	bool					GetEventComponentsForSynchro(MatComponents& eventComponents);
	bool					GetDebugMode()				{	return myDoDebug;				}
	void					SetLogFile(std::string name) //allows the proper path (and filename) to the log file to be supplied at a later stage
	                             { myLogFileName = name; 
	                               myLogFile = std::ofstream(name); }

private:
	bool			ExtractTriggers(const std::string& triggersFileName);
	void			ProcessBlink();
	void			CorrectData(std::vector<Parser::double_t>& data, const Parser::double_t maxVal);
	void			RemoveFalseBlinks(std::vector<Blink*>& blinks, std::vector<Parser::double_t>& xPos, std::vector<Parser::double_t>& yPos);
	void			InterpolateFalseBlink(std::vector<Parser::double_t>& pos, const size_t start, const size_t stop);
	bool			OrganizeData(GazeEegEnv& gazeEegEnv);
	bool			SaveDebugData(const GazeEegEnv& gazeEegEnv);
	bool			LoadDebugData(const GazeEegEnv& gazeEegEnv);
	bool			Process(const size_t firstPos, const std::vector<char*> parseContent, bool doVelocityData);
	bool			MergeSubstrings(std::vector<char*>& subStrings);
	matvar_t*		GetParamsComponents(const uint16 msecPerSample, const uint32 nbChannels, const uint32 nbSamples, const bool forSynchro = false) const;
	void			GetEventTypeComponents(MatComponents& eventComponents, const GazeEegEnv& gazeEegEnv);
	void			GetChannelNamesComponents(MatComponents& matComponents) const;
	inline	bool	AreFlagsOn(const uint8 flags, const uint8 mask)		{	return (flags & mask) == mask;			}
	inline	bool	IsFlagOn(const uint8 flags, const uint8 mask)		{	return (flags & mask) != 0;				}
	inline	void	FlagsSet(uint8& flags,		const uint8 mask)		{	flags	|= mask;						}
	inline	void	FlagsClear(uint8& flags,	const uint8 mask)		{	flags	&= ~mask;						}
	inline	void	FlagsSet(uint16& flags,		const uint16 mask)		{	flags	|= mask;						}
	inline	uint32	GetPos(char* str)
					{	return Parser::IsValidInt(str) ? ((atoi(str) - myStartTimestamp)/myHeader.msecPerSample) : -1;
					}
	inline	int16	GetValueInt16(char* str)
					{	return int16(Parser::strcmp(str, ".") ? -1 : atoi(str));
					}
	inline	uint32	GetValueUint32(char* str)
					{	return uint32(Parser::strcmp(str, ".") ? -1 : atoi(str));
					}
	inline	double	GetValueDouble(char* str) 
					{	return Parser::strcmp(str, ".") ? -1 : atof(str);
					}
			bool	SortEvents(const bool inDebugMode = false);
			bool	ProcessFrames();
			void	ClearComponents();
	template <class T> bool	IsValid(const std::vector<T>& data, const T invalidValue)
	{	for(std::vector<T>::const_iterator it=data.begin(); it != data.end(); it++)
		{	if(*it != invalidValue)
				return true;
		}
		
		return false;
	}
	bool	IsNotNaN(const std::vector<Parser::double_t>& data, const Parser::double_t valNaN)
	{	for(std::vector<Parser::double_t>::const_iterator it=data.begin(); it != data.end(); it++)
		{	if(*it != valNaN)
				return true;
		}
		
		return false;
	}
	void	Exit(const std::string& file, const uint32 line, const uint32 pos, const std::string& text = "Corrupted ASC file ");
	void	Warning(const uint32 pos, const std::string& text);
	bool	Warning(bool& status, const std::string& header, const bool refStatus, const uint32 pos = 0);

private:
	std::string						myAscFileName;			//!< source \a ASC file name
	AscHeader						myHeader;				//!< acquisition header
	Parser::double_t				myValNaN;				//!< the \a NaN value for invalid data samples
	Parser::double_t				myBlinkNaN;				//!< the \a NaN value for the blink

	//! seeks section
	size_t							mySeekInd;
	std::vector<uint32>				myFileSeeks;

	//! raw data section
	std::vector<uint8>				myFlags;				//!< vector of acquisition flags; \see acqFlags_t
	std::vector<uint16>				myFrames;				//!< vector of video frames
	std::vector<uint16>				myTriggerTab;			//!< vector of triggers

	bool							myHasLeft;				//!< has left eye data samples
	std::vector<Parser::double_t>	myXleft;				//!< transient X coordinates for the left eye
	std::vector<Parser::double_t>	myYleft;				//!< transient Y coordinates for the left eye
	std::vector<Parser::double_t>	myPleft;				//!< transient pupil size for the left eye

	std::vector<Parser::double_t>	myXleftVel;		        //!< Velocity X for the left eye
	std::vector<Parser::double_t>	myYleftVel;			    //!< Velocity Y for the left eye

	bool							myHasRight;				//!< has right eye data samples
	std::vector<Parser::double_t>	myXright;				//!< transient X coordinates for the right eye
	std::vector<Parser::double_t>	myYright;				//!< transient Y coordinates for the right eye
	std::vector<Parser::double_t>	myPright;				//!< transient pupil size for the right eye
	
	std::vector<Parser::double_t>	myXrightVel;		    //!< Velocity X for the right eye
	std::vector<Parser::double_t>	myYrightVel;			//!< Velocity Y for the right eye

	bool							myOnPupil;				//!< process the pupil size
	bool							myOnVelocityDetected = false;   //!< Set if velocity is detected - check is performed using the "VEL" string in the .ASC file
	bool                            myIsVelocityMessagePrinted = false; //to avoid printing multiple messages

	std::vector<Parser::double_t>	myBlink;				//!< transient vector for blinks
	std::vector<Parser::double_t>	myData;					//!< final data vector that combines the precedent vectors

	//! event section
	std::vector<CSequence*>			mySequences;			//!< vector of acquisition sequences
	std::vector<Displayable*>		myDisplayables;			//!< vector of acquisition displayables
	std::vector<Frame*>				myDisplayableFrames;	//!< vector of acquisition video frames
	bool							myOnVideoframe;			//!< process or not the video frames
	std::vector<CTransition*>		myTransitions;			//!< vector of acquisition transitions
	std::vector<FixationSaccade*>	myFixSaccLeft;			//!< vector of acquisition left eye saccades & fixations
	std::vector<FixationSaccade*>	myFixSaccRight;			//!< vector of acquisition eye saccades & fixations 
	std::vector<Blink*>				myBlinksLeft;			//!< vector of acquisition left eye blinks
	std::vector<Blink*>				myBlinksRight;			//!< vector of acquisition right eye blinks
	std::vector<AcqPhase*>			myAcqPhases;			//!< vector of acquisition phases (optional learning phase & mandatory testing phase)
	std::vector<Record*>			myRecords;				//!< vector of record steps
	std::vector<Trigger*>			myTriggers;				//!< vector of acquisition triggers
	markerMap_t						myTriggerDefs;			//!< list containing the trigger definitions
	std::vector<Event*>				mySortedEvents;			//!< sorted combination of the precedent events
	std::vector<std::string>		myChannelNames;			//!< vector of channel names

	//! auxiliary variables
	uint32							myStartTimestamp;		//!< timestamp of the first acquisition sample
	uint32							myEndTimestamp;			//!< timestamp of the last acquisition sample
	uint32							myCurentPos;			//!< sample position in the buffer
	uint32							myNbSamples;			//!< number of acquired samples
	uint32							myNbChannels;			//!< number of acquired channels

	//! log file
	std::string						myLogFileName;			//!< log file name
	std::ofstream					myLogFile;				//!< log file containing acquisition information
	bool							myInfoInLogFile;		//!< is some useful information in the log file ???
	bool							myDoDebug;

public:
	Synchronization					mySynchronization;
	static EventTypeMap				theEventTypeMap;		//!< list of event types a( as, string) std::map::pair
};
