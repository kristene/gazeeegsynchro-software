/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

#include "StdAfx.h"

#include "BrainampVmrk.h"
#include "BrainampVhdr.h"
#include "BrainampEeg.h"
#include "Parser.h"
#include "MemoryFile.h"
#include "Event.h"
#include "GazeEegEnv.h"
#include "EventStimMap.h"

BrainampVmrk::BrainampVmrk()
	: myNbExtraStimulations(0)
{
}

BrainampVmrk::~BrainampVmrk()
{
}

bool BrainampVmrk::Process(const bool doAll, const std::string& fileName)
{
	MSendDebug1("Processing : ", fileName);

	MemoryFile	memoryFile;
	memoryFile.Read(fileName);

	std::string	retrieve;
	std::string	toFind("Mk1=");
	int	ii		= 1;
	int	markers	= 0;
	std::vector<int>	segments;

	while(memoryFile.GetString(retrieve, toFind)) 
	{	std::string			text(retrieve);
		
		std::vector<char*>	strings;
		Parser::ParseChar(strings, (char*) retrieve.c_str(), retrieve.length(), ',');

		if(Parser::strcmp(*strings.begin(), "New Segment"))
		{	uint32	position;

			std::istringstream iss1(*(strings.begin()+1));
			iss1 >> position;

			position--;
			MarkerList_t::reverse_iterator itSeg = myMarkers.rend();
			for(MarkerList_t::reverse_iterator it=myMarkers.rbegin(); it != myMarkers.rend(); it++)
			{	if(it->offset != position)
					break;
				
				itSeg = it;
			}

			if(itSeg == myMarkers.rend())
			{	int pos = myMarkers.size();
				
			myMarkers.push_back(CMarker(text, position, *strings.rbegin()));

			segments.push_back(pos);
			}
			else
			{	MarkerList_t::iterator it=itSeg.base() - 1;
				
				int pos = it - myMarkers.begin();
				myMarkers.insert(it, CMarker(text, position, *strings.rbegin()));
				segments.push_back(pos);
			}

			markers++;
		}
		else if (Parser::strcmp(*strings.begin(), "Stimulus") //process all stimulus
			|| (Parser::strcmp(*strings.begin(), "Comment") && Parser::strcmp(strings[1], "Buffer Overflow")) //and comment "Buffer Overflow"
			)
		{
			char	ch;
			uint16	stimulation;
			uint32	position;
			uint32	duration;

			bool isValid = true;

			{
				//Anton: Improved to load the HG data correctly
				std::string value(strings[1]);
				if (value[0] == 'S') value = value.substr(1);
				if (value[0] == ' ') value = value.substr(1);
				stimulation = (uint16)atoi(value.c_str());

				if (stimulation == 0 && strings[1] != "0")
				{
					MSendWarning("Stimulation: '" + std::string(strings[1]) + "' ignored. Stimulation code is not numeric.\n");
					isValid = false;
				}
			}

			{
				std::istringstream iss(*(strings.begin() + 2));
				iss >> position;
			}

			{
				std::istringstream iss(*(strings.begin() + 3));
				iss >> duration;
			}


			if (Parser::strcmp(*strings.begin(), "Stimulus") && isValid)
			{
				myMarkers.push_back(CMarker(text, stimulation, --position, duration, 0));
			}
			else if (Parser::strcmp(*strings.begin(), "Comment") && Parser::strcmp(*(strings.begin() + 1), "Buffer Overflow"))
				myMarkers.push_back(CMarker(text, CMarker::MARKER_OVERFLOW_VAL, --position, duration, 0)); //add marker 257 if overflow

			markers++;

			if ((myMarkers.size() > 1) && (myMarkers.rbegin() + 1)->IsTrigger() && myMarkers.rbegin()->IsTrigger() && ((myMarkers.rbegin() + 1)->offset == (myMarkers.rbegin()->offset - 1)))
			{
				myMarkers.rbegin()->offset--;
				(myMarkers.rbegin() + 1)->isExtra = true;
				myNbExtraStimulations++;
			}
		}
		else if(doAll)
		{	uint32	position;

			std::istringstream iss1(*(strings.begin()+1));
			iss1 >> position;
			
			myMarkers.push_back(CMarker(text, --position));
		}

		std::ostringstream oss;
		oss << "Mk" << ++ii << '=';
		toFind = oss.str();
	}

	mySegmentsIt.clear();
	
	for(std::vector<int>::iterator it=segments.begin(); it != segments.end(); it++)
		mySegmentsIt.push_back(myMarkers.begin() + *it);

	return true;
}

bool BrainampVmrk::ProcessDataSegments(BrainampVhdr& vhdr, const size_t eegDataSize)
{	
	uint32	eegNbSamples			= eegDataSize/vhdr.GetNbChannels();
	vhdr.SetNbSamples(eegNbSamples);
	
	for(std::vector<MarkerList_t::iterator>::iterator it=mySegmentsIt.begin(); it != mySegmentsIt.end(); it++)
		(*it)->ProcessSegmentTimestamp(vhdr.SamplingRate());

	if(mySegmentsIt.size() <= 1)
		return true;

	if(vhdr.segmentationType != BrainampVhdr::ST_NOSEGMENTED)
		return true;

	for(std::vector<MarkerList_t::iterator>::iterator it=mySegmentsIt.begin()+1; it != mySegmentsIt.end(); it++)
	{	uint32	dSamplesTime			 = uint32(::round((*it)->startSampleD - (*(it-1))->startSampleD));
		uint32	segDuration				 = (*it)->offset - (*(it-1))->offset;
		uint32	samplesInsertedBefore	 = dSamplesTime - segDuration;
		(*it)->samplesInsertedBefore	 = samplesInsertedBefore;

#if 0 // DIG
	}
#else
		MarkerList_t::iterator it1		 = *it;
		for(it1; it1 != myMarkers.end(); it1++)
		{	if(it1->IsSegment())
				continue;
			else if(it1->IsStimulation())
				it1->offset	+= samplesInsertedBefore;
	}	}
#endif

	uint32	samplesInsertedBefore	= 0;
	uint32	duration				= 0;

	for(std::vector<MarkerList_t::iterator>::iterator it=mySegmentsIt.begin()+1; it != mySegmentsIt.end(); it++)
	{	(*(it-1))->segmentCorrection.stop	= (*it)->offset + samplesInsertedBefore;
		(*(it-1))->duration					= (*it)->offset - duration;
		(*it)->segmentCorrection.start		= (*(it-1))->segmentCorrection.stop;
		(*it)->segmentCorrection.inflexion	= (*it)->segmentCorrection.start + (*it)->samplesInsertedBefore;
		samplesInsertedBefore			   += (*it)->samplesInsertedBefore;
		duration						   += (*(it-1))->duration;
	}

	(*mySegmentsIt.rbegin())->segmentCorrection.stop	= eegNbSamples + samplesInsertedBefore;
	(*mySegmentsIt.rbegin())->duration					= eegNbSamples - duration;

	vhdr.SetNbSamples((*mySegmentsIt.rbegin())->segmentCorrection.stop);
	
	return true;
}

size_t BrainampVmrk::NbStimulations()
{
	size_t nbStimulations = 0;
	for(MarkerList_t::iterator it=myMarkers.begin(); it != myMarkers.end(); it++)
	{	if(it->IsExtra())
			continue;

		nbStimulations++;
	}

	return nbStimulations;
}

size_t BrainampVmrk::NbSegments()
{
	size_t nbSegments = 0;
	for(MarkerList_t::iterator it=myMarkers.begin(); it != myMarkers.end(); it++)
	{	if(it->IsSegment())
			nbSegments++;
	}

	return nbSegments;
}

// saves the new .VMRK file
bool BrainampVmrk::SaveGazeEeg(const std::string& src, const std::string& dest, const MarkerList_t& markers, const bool forceDuration1)
{
	std::string srcFile		= Parser::ReplaceExtention(src,  "vmrk");
	std::string destFile	= dest + ".vmrk";
	
	MSendDebug1("Saving : ", destFile);

	MemoryFile	memFile;
	if(!memFile.Read(srcFile))
		return MSendError("BrainampVmrk::SaveGazeEeg::MemoryFile::Read");

	memFile.ReplaceBetween(Parser::GetFileName(destFile) + ".eeg", "DataFile=");
	memFile.EraseAfter("Mk1=");

	std::ostringstream oss;
	int	ii = 2;
	for(MarkerList_t::const_iterator it=markers.begin(); it != markers.end(); it++, ii++)
	{	oss << "Mk" << ii << "=Stimulus,S";
		oss.fill(' '); 
		oss.width(4);
		if(forceDuration1 || (it->value < 256))
			oss << int(it->value) << ',' << (it->offset + 1) << ",1,0\r\n";
		else
			oss << int(it->value) << ',' << (it->offset + 1) << ',' << it->duration << ",0\r\n";
	}

	memFile += oss.str();

	return memFile.Write(destFile);
}

//saves a .VMRK file
bool BrainampVmrk::SaveEegLab(const std::string& eegSrcFileName, const std::string& eegDestFileName)
{
	std::string srcFile		= Parser::ReplaceExtention(eegSrcFileName,  "vmrk");
	std::string destFile	= Parser::ReplaceExtention(eegDestFileName, "vmrk");
	
	MSendDebug1("Saving : ", destFile);

	MemoryFile	memFile;
	if(!memFile.Read(srcFile))
		return MSendError("BrainampVhdr::SaveGazeEeg::MemoryFile::Read");

	std::string		pathS, nameS, extS;
	Parser::ParsePath(pathS, nameS, extS, eegSrcFileName);

	std::string		path, name, ext;
	Parser::ParsePath(path, name, ext, destFile);
	
	memFile.ReplaceBetween(name + '.' + extS,	"DataFile=");

	std::istringstream	iss(memFile);
	std::string			s;
	bool				found = false;
	while (std::getline(iss, s))
	{	std::string::size_type pos = s.find("Mk");
		if(!found && (pos == 0))
			found = true;
		else if(found && (pos == std::string::npos))
			break;
	}

	std::stringstream tmp;
	tmp << iss.rdbuf();
	std::string footer = tmp.str();
	
	memFile.EraseBefore("Mk1=");

	std::ostringstream oss;
	int	ii = 1;
	for(MarkerList_t::const_iterator it=myMarkers.begin(); it != myMarkers.end(); it++, ii++)
		oss << "Mk" << ii << '=' << it->text << "\r\n";

	memFile += oss.str() + "\n\n" + footer;

	return memFile.Write(destFile);
}

bool BrainampVmrk::ProcessAddEventStim(GazeEegEnv& gazeEegEnv, const BrainampVhdr& vhdr)
{
	AddEventStimList::iterator&	aesIt	= gazeEegEnv.aes.it;	

	std::vector<size_t>		cleanedEpocs;
	if(!aesIt->GetCleanedEpochs(cleanedEpocs))
		return MSendError("Reading aesCleanFile file " + aesIt->Get("cleanFileFullPath"));

	if(!aesIt->ReadEvents(gazeEegEnv.aes.dictFile))
		return MSendError("Processing reference trigger in file " + gazeEegEnv.aes.dictFile);

	EventStimMap	eventStimMap;	
	if(!eventStimMap.Read(gazeEegEnv.eventStimFileName))
		return MSendError("Reading eventStimFileName file " + gazeEegEnv.eventStimFileName);
	
	if(eventStimMap.empty())
		return MSendError("EventStimFileName file " + gazeEegEnv.eventStimFileName + " is empty (size = 0)");

	std::string		synchroVmrkFile = Parser::ReplaceExtention(aesIt->Get("synchroFileFullPath"), "vmrk");
	BrainampVmrk	synchroVmrk;
	if(!synchroVmrk.Process(true, synchroVmrkFile))
		return MSendError("Processing synchro VMRK file " + synchroVmrkFile);

	std::vector<size_t> srcEpochsInd;
	srcEpochsInd.reserve(5000);
	
	MarkerList_t&	syncMarkers = synchroVmrk.Markers();
	
	uint32	offset	= eventStimMap.rbegin()->first.first - 1;
	uint32	offset1	= syncMarkers.rbegin()->offset;

	if(offset > offset1)
	{	std::ostringstream oss;
		oss  << std::endl << std::endl << "EventStim : " << eventStimMap.rbegin()->second << " (" << eventStimMap.rbegin()->first.second << ") : " << eventStimMap.rbegin()->first.first << std::endl;
		
		return MSendError(oss.str() + "Offset of EventStim in \n\t" + gazeEegEnv.eventStimFileName + "\nbigger than the offset of the last event in VMRK file \n\t" + synchroVmrkFile);
	}

	MarkerList_t::iterator	itSync	= syncMarkers.begin();
	size_t					iiSync	= 0;
	for(EventStimMap::iterator it=eventStimMap.begin(); it != eventStimMap.end(); it++)
	{	offset = it->first.first - 1;
		while(true)
		{	if(syncMarkers[iiSync].IsStimulation() &&  (syncMarkers[iiSync].value == aesIt->referenceTriggerNum))
				srcEpochsInd.push_back(iiSync);

			offset1 = syncMarkers[iiSync].offset;
			if(offset1 >= offset)
				break;

			iiSync++;
			itSync	= syncMarkers.begin() + iiSync;
		}

		if(offset1 > offset)
		{	std::ostringstream oss;
			oss  << std::endl << std::endl << "EventStim : " << eventStimMap.rbegin()->second << " (" << eventStimMap.rbegin()->first.second << ") : " << eventStimMap.rbegin()->first.first << std::endl;
			
			MSendWarning(oss.str() + "Offset of EventStim in \n\t" + gazeEegEnv.eventStimFileName + "\nimproper. It should meet at least one offset in the VMRK file \n\t" + synchroVmrkFile);
			continue;
		}

		uint16	evStimVal = it->first.second + eventStim;
		std::ostringstream oss;

		if(aesIt->isReferenceTriggerNum)
			oss << "Stimulus,S" << evStimVal	<< ',';
		else
			oss << "Stimulus," << it->second	<< ',';

		syncMarkers.insert(syncMarkers.begin() + ++iiSync, CMarker(false, evStimVal, it->second, oss.str(), offset));
		itSync	= syncMarkers.begin() + iiSync;
	}

	while(iiSync < syncMarkers.size())
	{	if(syncMarkers[iiSync].IsStimulation() &&  (syncMarkers[iiSync].value == aesIt->referenceTriggerNum))
			srcEpochsInd.push_back(iiSync);
		iiSync++;
		itSync	= syncMarkers.begin() + iiSync;
	}

	// verify referenceTriggerNum
	for(iiSync=0; iiSync < srcEpochsInd.size(); iiSync++)
	{	MarkerList_t::iterator	itSyncMarkers = syncMarkers.begin() + srcEpochsInd[iiSync];
		if(itSyncMarkers->value != aesIt->referenceTriggerNum)
			return MSendError(" Wrong referenceTriggerNum value after insertion of EventStim");
	}

	std::vector<MarkerList_t::iterator> srcEpochs;
	srcEpochs.reserve(srcEpochsInd.size());
	for(std::vector<size_t>::iterator it=srcEpochsInd.begin(); it != srcEpochsInd.end(); it++)
		srcEpochs.push_back(syncMarkers.begin() + *it);

	for(std::vector<size_t>::iterator it=cleanedEpocs.begin(); it != cleanedEpocs.end(); it++)
	{	if(*it >= srcEpochs.size())
			continue;

		MarkerList_t::iterator itSrc = srcEpochs[*it];
		while(true)
		{	itSrc->isExtra = true;
			itSrc++;
			if(itSrc == syncMarkers.end())
				break;
			else if(itSrc->IsStimulation() &&  (itSrc->value == aesIt->referenceTriggerNum))
				break;
	}	}
	srcEpochs.push_back(syncMarkers.end());

	std::vector<MarkerList_t::iterator>::iterator	srcEpochIt	= srcEpochs.begin();
	std::vector<MarkerList_t::iterator>::iterator	srcEpochLim	= srcEpochs.end() - 1;
	bool											foundSeg	= false;
	for(size_t ii=0; ii < myMarkers.size(); ii++)
	{	MarkerList_t::iterator it=myMarkers.begin() + ii;

		if(it->IsSegment())
		{	foundSeg	= true;
			continue;
		}
		
		if(foundSeg && it->IsStimulation() && (it->value == aesIt->referenceTriggerNum))
		{	foundSeg	= false;

			while((srcEpochIt != srcEpochLim) && (*srcEpochIt)->IsExtra())
				srcEpochIt++;

			if((srcEpochIt == srcEpochLim))
				break;

			AddEventStim(aesIt, ii, *srcEpochIt, *(srcEpochIt+1));
			srcEpochIt++;
	}	}

	return true;
}

void BrainampVmrk::AddEventStim(AddEventStimList::iterator& aesIt, size_t indMyMarkers, MarkerList_t::iterator itSrc, const MarkerList_t::iterator itSrcLim)
{
	uint32	offSrc	= itSrc->offset;
	uint32	offDst	= myMarkers[indMyMarkers].offset;
	while(itSrc != itSrcLim)
	{	if(!itSrc->IsExtra() && itSrc->IsOther())
		{	uint32					diffSrc	= itSrc->offset - offSrc;
			bool					found	= false;
			MarkerList_t::iterator	itDst	= myMarkers.begin() + indMyMarkers;
			while((itDst != myMarkers.end()) && !itDst->IsSegment())
			{	uint32 diffDst	= itDst->offset - offDst;
				if(diffDst == diffSrc)
				{	found = true;
					break;
				}

				itDst++;
				indMyMarkers++;
			}

			if(!found)
				return;

			std::ostringstream oss;
			oss << itSrc->text << (itDst->offset + 1) << ",1,0";
			
			myMarkers.insert(itDst+1, CMarker(false, itSrc->value, itSrc->evStimName, oss.str(), itDst->offset));
			aesIt->eventDictAll.Add(itSrc->value, itSrc->evStimName);
		}

		itSrc++;
	}
}
