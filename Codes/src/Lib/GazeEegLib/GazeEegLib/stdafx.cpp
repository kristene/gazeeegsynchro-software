/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

// stdafx.cpp�: fichier source incluant simplement les fichiers Include standard
// GazeEegLib.pch repr�sente l'en-t�te pr�compil�
// stdafx.obj contient les informations de type pr�compil�es

#include "stdafx.h"

std::string MyGetPrivateProfileString(const std::string& section, const std::string& key, const std::string& defVal, const std::string& fileName)
{
	std::vector<char> fileContent;
	if(!GazeEeg::File::Read(fileName, fileContent))
		return "";

	if(fileContent.empty())
		return "";

	std::vector<char*> parseContent;
	Parser::ParseChar(parseContent, &fileContent[0], fileContent.size(), '\n');

	bool found = false;
	
	std::vector<char*>::iterator it;
	for(it=parseContent.begin(); it != parseContent.end(); it++)
	{	if(strstr(*it, "[") && strstr(*it, "]") && ::strstr(*it, section.c_str()))
		{	found = true;
			break;
	}	}

	if(!found)
		return defVal;
	
	for(it++; it != parseContent.end(); it++)
	{	if(strstr(*it, "[") && strstr(*it, "]"))
			break;

		if(strstr(*it, "=") && ::strstr(*it, key.c_str()))
		{	std::vector<char*> strContent;
			Parser::ParseChar(strContent, *it, strlen(*it), '=');

			return Parser::Trim(std::string(*strContent.rbegin()));
	}	}
	
	return defVal;
}

int MyGetPrivateProfileInt(const std::string& section, const std::string& key, const int defVal, const std::string& fileName)
{
	std::ostringstream oss;
	oss << defVal;

	std::istringstream iss(MyGetPrivateProfileString(section, key, oss.str(), fileName));

	int retval;
	iss >> retval;

	return retval;
}

double MyGetPrivateProfileDouble(const std::string& section, const std::string& key, const double defVal, const std::string& fileName)
{
	std::ostringstream oss;
	oss << defVal;

	std::istringstream iss(MyGetPrivateProfileString(section, key, oss.str(), fileName));

	double retval;
	iss >> retval;

	return retval;
}

