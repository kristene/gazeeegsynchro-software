/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

#pragma once

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <fcntl.h>
#include <io.h>
#include <direct.h>
#include <time.h>

#include <iostream>
#include <fstream>
#include <strstream>
#include <string>
#include <vector>
#include <cctype>
#include <algorithm>
#include <functional>
#include <cstdlib>
#include <numeric>
#include <map>

#include "Types.h"

namespace GazeEeg
{
	class File
	{
	public:
		typedef enum
		{	TIME_CREATED,
			TIME_MODIFIED,
			TIME_LAST_ACCESS,
		} time_type;
		typedef enum
		{	OPEN_READ	= _O_RDONLY | _O_BINARY,
			OPEN_WRITE	= _O_CREAT | _O_TRUNC | _O_WRONLY | _O_BINARY,
		} open_type;

	public:
		File();
		File(const char* file, const int oflag, const int pmode = _S_IREAD | _S_IWRITE);
		File(const std::string& file, const int oflag, const int pmode = _S_IREAD | _S_IWRITE);
		virtual ~File();
		bool			operator !()	const { return myHandler == -1; }	// verify if the class is correctly constructed
		bool			IsValid()		const { return myHandler != -1; }	// verify if the class is correctly constructed

		/*	1. Statical functions allowing not opened file manipulation
		*/
		static bool		Exists(const char* file);   
		static bool		Exists(const std::string& file)
						{	return File::Exists(file.c_str());	}
		static bool		IsFile(const char* file);   
		static bool		IsFile(const std::string& file)
						{	return File::IsFile(file.c_str());	}
		static bool		IsDirectory(const char* file);   
		static bool		IsDirectory(const std::string& file)
						{	return File::IsDirectory(file.c_str());	}
		static unsigned long Size(const char* file);
		static unsigned long Size(const std::string& file)
						{	return File::Size(file.c_str());	}
		static bool		Mkdir(const char* file);
		static bool		Mkdir(const std::string& file)
						{	return File::Mkdir(file.c_str());	}
		static bool		Remove(const char* file);
		static bool		Remove(const std::string& file)
						{	return File::Remove(file.c_str());	}
		static bool		Copy(const char* src, const char* dest);
		static bool		Copy(const std::string& src, const std::string& dest)
						{	return File::Copy(src.c_str(), dest.c_str());	}
		static bool		Rename(const char* oldName, const char* newName);
		static bool		Rename(const std::string& oldName, const std::string& newName)
						{	return File::Rename(oldName.c_str(), newName.c_str());	}
		static bool		Move(const char* src, const char* dest);
		static bool		Move(const std::string& src, const std::string& dest)
						{	return File::Move(src.c_str(), dest.c_str());	}
		static std::string	GetTime(const char* file, const File::time_type timeType);
		static std::string	GetTime(const std::string& file, const File::time_type timeType)
						{	return File::GetTime(file.c_str(), timeType);	}
		static bool		IsEqual(const char* file1, const char* file2);
		static bool		IsEqual(const std::string& file1, const std::string& file2)
						{	return File::IsEqual(file1.c_str(), file2.c_str());	}
		template<class type> static bool Read(const char* file, std::vector<type>& data)
						{
							if(!File::Exists(file))
								return MSendError1("File don't extist", file);

							size_t			size	= size_t(File::Size(file));
							size_t			dataSz	= size_t(size/sizeof(type));
							data.resize(dataSz);
							
							if(size == 0)
								return true;

							File	pf(file, OPEN_READ, _S_IREAD);
							if(!pf)
								return MSendError1("File creating file", file);

							unsigned long dim = pf.Read((char*) &data[0], (unsigned long) size);

							bool b = dim == size;
							
							return b;
						}
		template<class type> static bool Read(const std::string& file, std::vector<type>& data)
						{	
							return Read(file.c_str(), data);
						}
		template<class type> static bool Read(const char* file, type& data)
						{
							if(!File::Exists(file))
								return MSendError1("File don't exist", file);

							File	pf(file, OPEN_READ, _S_IREAD);
							if(!pf)
								return MSendError1("File creating file", file);

							unsigned long dim = pf.Read((char*) &data, sizeof(data));

							return dim == sizeof(data);
						}
		template<class type> static bool Read(const std::string& file, type& data)
						{	
							return Read(file.c_str(), data);
						}
		static bool		Read(const std::string& file, void* data);
		static bool		ReadInString(std::string& data, const std::string& file);
		static bool		WriteFromString(std::string& data, const std::string& file);
		template<class type>	static bool	Write(const std::string& file, const std::vector<type>& data)
						{	return File::Write(file, &(*data.begin()), (unsigned long) data.size()*sizeof(type));	}
		template<class type>	static bool	Write(const std::string& file, const type data)
						{	return File::Write(file, &data, sizeof(type));											}
		static bool		Write(const std::string& file, const void* data, const unsigned long sz);

		/*	2. File oriented access : open/close, seek/tell, read/write (see typical file accession on UNIX)
		*/
		bool			Open(const char* file, const int oflag, const int pmode = _S_IREAD | _S_IWRITE );
		bool			Open(const std::string& file, const int oflag, const int pmode = _S_IREAD | _S_IWRITE );
		bool			Close();
		unsigned long	Read(void* ptr, const unsigned long dim);
		unsigned long	Write(const void* ptr, const unsigned long dim);
		size_t			Write(const void* ptr, const size_t dim)
						{	return size_t(Write(ptr, (unsigned long) dim));	}
		bool			Seek(const long pos, int whence = SEEK_SET);
		unsigned long	Tell();
		unsigned long	Size();
		std::string		Getline();

	protected:
		// private variables used internaly
		int				myHandler;		// opened file handler
	};
}