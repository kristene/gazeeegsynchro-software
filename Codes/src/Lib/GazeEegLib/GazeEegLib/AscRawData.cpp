/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

#include "StdAfx.h"

#include <iomanip>

#include "AscRawData.h"
#include "File.h"
#include "Ttl.h"

using GazeEeg::File;

AscRawData::EventTypeMap	AscRawData::theEventTypeMap;

AscRawData::EventTypeMap::EventTypeMap()
{
EnumMapInit(*this)
    (ENUM_ID_TO_STRING_PAIR(displayableSpecial))
    (ENUM_ID_TO_STRING_PAIR(displayableImage))
    (ENUM_ID_TO_STRING_PAIR(displayableText))
    (ENUM_ID_TO_STRING_PAIR(displayableVideoSound))
	(ENUM_ID_TO_STRING_PAIR(displayableVideoNoSound))
    (ENUM_ID_TO_STRING_PAIR(displayableFrame))
    (ENUM_ID_TO_STRING_PAIR(transitionTime))
    (ENUM_ID_TO_STRING_PAIR(transitionMouse))
    (ENUM_ID_TO_STRING_PAIR(transitionKeyboard))
    (ENUM_ID_TO_STRING_PAIR(transitionGaze))
    (ENUM_ID_TO_STRING_PAIR(transitionSSacc))
    (ENUM_ID_TO_STRING_PAIR(transitionESacc))
    (ENUM_ID_TO_STRING_PAIR(transitionSFix))
    (ENUM_ID_TO_STRING_PAIR(transitionEFix))
    (ENUM_ID_TO_STRING_PAIR(transitionSpecial))
    (ENUM_ID_TO_STRING_PAIR(flowSequence))
    (ENUM_ID_TO_STRING_PAIR(flowFixationLeft))
    (ENUM_ID_TO_STRING_PAIR(flowFixationRight))
    (ENUM_ID_TO_STRING_PAIR(flowSaccadeLeft))
    (ENUM_ID_TO_STRING_PAIR(flowSaccadeRight))
    (ENUM_ID_TO_STRING_PAIR(flowBlinkLeft))
    (ENUM_ID_TO_STRING_PAIR(flowBlinkRight))
    (ENUM_ID_TO_STRING_PAIR(phaseLearning))
    (ENUM_ID_TO_STRING_PAIR(phaseTesting))
    (ENUM_ID_TO_STRING_PAIR(recordStep))
	;
}

AscRawData::AscRawData()
	: myStartTimestamp(0)
	, myEndTimestamp(0)
	, myValNaN(Parser::doubleNaN)
	, myLogFileName("Log-AscRawData.txt")
	, myInfoInLogFile(false)
	, myHasLeft(false)
	, myHasRight(false)
	, myDoDebug(false)
{
}

AscRawData::~AscRawData()
{
	ClearComponents();
}

bool AscRawData::IsValid(GazeEegEnv& gazeEegEnv, const std::string& srcFilename)
{
	if(!File::Exists(srcFilename))
	{	gazeEegEnv.error = srcFilename;
		return false;
	}

	std::vector<std::string>	segments;
	Parser::ParsePath(segments, srcFilename);

	std::transform(segments[2].begin(), segments[2].end(), segments[2].begin(), ::tolower);
	if(segments[2] != "asc")
	{	std::string		line(1024, ' ');
		
		std::ifstream	ifs((char*) srcFilename.c_str());
		ifs.getline((char*) line.c_str(), line.length());

		if(line.find("** CONVERTED FROM") == std::string::npos)
		{	gazeEegEnv.error = srcFilename;
			return false;
	}	}

	if(gazeEegEnv.eventStimFileName.empty())
	{	if(File::Exists(srcFilename + ".ext"))
			gazeEegEnv.eventStimFileName	= srcFilename + ".ext";
	}
	else if(!File::Exists(gazeEegEnv.eventStimFileName))
	{	gazeEegEnv.error = gazeEegEnv.eventStimFileName;
		return false;
	}

	if(gazeEegEnv.triggerFileName.empty())
	{	if(File::Exists(srcFilename + ".trig.txt"))
			gazeEegEnv.triggerFileName	= srcFilename + ".trig.txt";
	}
	else if(!File::Exists(gazeEegEnv.triggerFileName))
	{	gazeEegEnv.error =  gazeEegEnv.triggerFileName;
		return false;
	}

	gazeEegEnv.gazeMatFileName	= (!gazeEegEnv.eventStimFileName.empty() ? gazeEegEnv.eventStimFileName : srcFilename) + ".mat";
	gazeEegEnv.gazeFileName		= srcFilename;

	return true;
}

void AscRawData::ClearComponents()
{
	if(myDoDebug)
		return;

	for(std::vector<CSequence*>::iterator it=mySequences.begin(); it < mySequences.end(); it++)
		delete *it;
	mySequences.clear();

	for(std::vector<Displayable*>::iterator it=myDisplayables.begin(); it < myDisplayables.end(); it++)
		delete *it;
	myDisplayables.clear();

	for(std::vector<Frame*>::iterator it=myDisplayableFrames.begin(); it < myDisplayableFrames.end(); it++)
		delete *it;
	myDisplayableFrames.clear();

	for(std::vector<CTransition*>::iterator it=myTransitions.begin(); it < myTransitions.end(); it++)
		delete *it;
	myTransitions.clear();

	for(std::vector<FixationSaccade*>::iterator it=myFixSaccLeft.begin(); it < myFixSaccLeft.end(); it++)
		delete *it;
	myFixSaccLeft.clear();

	for(std::vector<FixationSaccade*>::iterator it=myFixSaccRight.begin(); it < myFixSaccRight.end(); it++)
		delete *it;
	myFixSaccRight.clear();

	for(std::vector<Blink*>::iterator it=myBlinksLeft.begin(); it < myBlinksLeft.end(); it++)
		delete *it;
	myBlinksLeft.clear();

	for(std::vector<Blink*>::iterator it=myBlinksRight.begin(); it < myBlinksRight.end(); it++)
		delete *it;
	myBlinksRight.clear();

	for(std::vector<AcqPhase*>::iterator it=myAcqPhases.begin(); it < myAcqPhases.end(); it++)
		delete *it;
	myAcqPhases.clear();

	for(std::vector<Record*>::iterator it=myRecords.begin(); it < myRecords.end(); it++)
		delete *it;
	myRecords.clear();

	for(std::vector<Trigger*>::iterator it=myTriggers.begin(); it < myTriggers.end(); it++)
		delete *it;
	myTriggers.clear();

	myTriggerDefs.clear();
}

bool AscRawData::ExtractTriggers(const std::string& triggersFileName)
{
	MSendDebug1("Processing TRIG file: ", triggersFileName);
	
	for(uint16 ii=1; ii <= 255; ii++)
	{	std::ostringstream oss;
		oss << "trigger";
		oss.width(3);
		oss.fill('0');
		oss << ii;

		myTriggerDefs[ii]	= oss.str();
	}

	LoadMarkers(myTriggerDefs, triggersFileName);
	
	return true;
}

bool AscRawData::Process(GazeEegEnv& gazeEegEnv)
{
	myOnPupil		= gazeEegEnv.onPupil;
	myOnVideoframe	= gazeEegEnv.onVideoframe;
	myAscFileName	= gazeEegEnv.gazeFileName;
	myDoDebug		= gazeEegEnv.doDebug;

	if(!File::Exists(myAscFileName))
		return MSendError1("File don't extist", myAscFileName);

	myValNaN	= Parser::gazeNaN;

	MSendDebug1("Processing ASC file: ", myAscFileName);

	std::vector<char> fileContent;
	if(!File::Read(myAscFileName, fileContent))
		return MSendError1("File reading", myAscFileName);

	if(fileContent.size() == 0)
		return MSendError1("Empty ASC file", myAscFileName);
	
	if(std::find(fileContent.begin(), fileContent.end(), '\n') == fileContent.end())
		return MSendError1("Invalid ASC file", myAscFileName);

	std::vector<char*> parseContent;
	Parser::ParseChar(parseContent, &fileContent[0], fileContent.size(), '\n', &myFileSeeks);

	if(!myHeader.Process(mySeekInd, myStartTimestamp, myEndTimestamp, parseContent, gazeEegEnv.falseBlinkDuration))
		return MSendError("AscRawData::Process : processing header");

	if(!LoadDebugData(gazeEegEnv))
	{	myStartTimestamp	= (myStartTimestamp / myHeader.msecPerSample) * myHeader.msecPerSample;
		myEndTimestamp		= ((myEndTimestamp / myHeader.msecPerSample) + myHeader.msecPerSample) * myHeader.msecPerSample;		
		myNbSamples			= (myEndTimestamp - myStartTimestamp)/myHeader.msecPerSample;
	
		myFlags.resize(myNbSamples, uint8(0));
		myFrames.resize(myNbSamples, uint16(0));
		myTriggerTab.resize(myNbSamples, uint16(0));
		
		//LEFT eye
		myXleft.resize(myNbSamples, Parser::gazeNaN);
		myYleft.resize(myNbSamples, Parser::gazeNaN);
		myPleft.resize(myNbSamples, Parser::gazeNaN);
		myXleftVel.resize(myNbSamples, Parser::gazeNaN);
		myYleftVel.resize(myNbSamples, Parser::gazeNaN);
		
		//RIGHT eye
		myXright.resize(myNbSamples, Parser::gazeNaN);
		myYright.resize(myNbSamples, Parser::gazeNaN);
		myPright.resize(myNbSamples, Parser::gazeNaN);
		myXrightVel.resize(myNbSamples, Parser::gazeNaN);
		myYrightVel.resize(myNbSamples, Parser::gazeNaN);
		
		myBlink.resize(myNbSamples, 0);
		
		if(!ExtractTriggers(gazeEegEnv.triggerFileName))
			return MSendError("AscRawData::Process : extracting Triggers");

		if(!Process(mySeekInd, parseContent, gazeEegEnv.doVelocityData))
			return MSendError("AscRawData::Process : processing version");

		if(!OrganizeData(gazeEegEnv))
			return MSendError("AscRawData::Process::OrganizeData");

		if(gazeEegEnv.doTtl)
		{	Ttl	ttl;

			ttl.Debug(myTriggers, gazeEegEnv.gazeFileName);

		}

		if(myInfoInLogFile)
			std::cout << "Warning information is available in: " << myLogFileName << std::endl;

		SaveDebugData(gazeEegEnv);
	}

	return true;
}

bool AscRawData::ProcessFrames()
{
	size_t	nbTotFrames = 0;

	for(std::vector<Displayable*>::iterator it=myDisplayables.begin(); it != myDisplayables.end(); it++)
	{	Displayable& displayable = **it;
		if((displayable.value != displayableVideoSound) && (displayable.value != displayableVideoNoSound))
			continue;

		size_t	samplesOnFrame	= displayable.frameDuration/myHeader.msecPerSample;
		size_t	lastFrame		= 0;
		uint16	playedFrames	= 0;
		bool	first			= true;
		for(size_t ii=displayable.startTime; ii < displayable.endTime; ii++)
		{	if(myFrames[ii])
			{	myFlags[ii]		|= fFrame;
				nbTotFrames++;
				lastFrame		 = ii;
				playedFrames++;
				uint16	frames	 = myFrames[ii];
				size_t	jj		 = 1;
				while(--frames)
				{	size_t	pos	 = ii - jj++*samplesOnFrame;
					if(pos < displayable.startTime)
						break;

					myFlags[pos]	|= fFrame;
					nbTotFrames++;
					playedFrames++;
				}

				if(first)
				{	first					= false;
					displayable.firstFrame	= frames;
		}	}	}

		displayable.playedFrames	= playedFrames;
	}

	if(nbTotFrames)
	{	myDisplayableFrames.reserve(nbTotFrames);

		for(size_t ii=0; ii < myFlags.size(); ii++)
		{	if(myFlags[ii] & fFrame)
				myDisplayableFrames.push_back(new Frame(ii));
		}
	}

	return true;
}

bool AscRawData::MergeSubstrings(std::vector<char*>& subStrings)
{
	if(std::string(subStrings[9]) == "TEXT")
	{	size_t jj = 10;
		while(std::string(subStrings[jj]) != "TEXT")
			jj++;

		jj--;
		
		if(jj > 10)
		{	for(size_t ii=10; ii < jj; ii++)
				*(subStrings[10] + strlen(subStrings[10])) = '_';
			
			subStrings.erase(subStrings.begin() + 11, subStrings.begin() + 11 + (jj - 10)); 
	}	}

	return subStrings.size() == 18;
}

bool AscRawData::Process(const size_t firstPos, const std::vector<char*> parseContent, bool doVelocityData) // Asc file reading

{
	static  int pourc		= 0;
	uint8	flagsRecording	= 0;
	uint32	lastTrigger		= 0;
	uint16	frameIndex		= 0;
	uint32	posMemo			= 0;
	uint32	startRecord		= 0;
	uint32	stopRecord		= 0;

	bool	inDisplayable	= false;
	bool	inFixL			= false;
	bool	inFixR			= false;
	bool	inSaccL			= false;
	bool	inSaccR			= false;
	bool	inBlinkL		= false;
	bool	inBlinkR		= false;
	bool	inPhase			= false;
	bool	inSequence		= false;
	bool	inRecord		= false;

	size_t	indSequence		= 1;
	size_t	indSaccL, indSaccR;
	size_t	indFixL, indFixR;
	size_t	indBlinkL, indBlinkR;

	// Nathalie 20170529
	bool		trialIdNat		= false;
	std::string	imageNameNat;
	uint32		posStartNat;
	uint32		posStopNat;


	for(std::vector<char*>::const_iterator it=parseContent.begin()+firstPos; it != parseContent.end(); it++, mySeekInd++)
	{	std::string			str(*it);
		
		std::vector<char*> lineSubStrings;
		Parser::ParseSpaces(lineSubStrings, (char*) str.c_str(), str.length());
		
		if(lineSubStrings.size() < 3)
			continue;

		uint32 pos = GetPos(lineSubStrings.front());
		if ((pos != -1) && flagsRecording)
		{
			if (pos < posMemo)
				Exit(__FILE__, __LINE__, pos);
			else if (pos >= myNbSamples)
				Exit(__FILE__, __LINE__, pos);

			posMemo = pos;
			myCurentPos = pos;

			double	p = (100.0*pos) / myNbSamples;
			int		pI = int(p);

			if (pI != pourc)
			{
				printf("%3d %%\r", (100 * pos) / myNbSamples);
				pourc = pI;
			}

			//Start: checking velocity data
			//Added by Anton
			//1 + 3xyp + 1 = 5 columns one eye - no velocity
			//1 + 3xyp + 2(xv,yv) + 1 = 7 columns one eye with velocity
			//1 + 3xyp + 3xyp + 1 = 8 columns two eyes - no velocity
			//1 + 3xyp + 3xyp + 2(xv,yv) + 2(xv,yv) + 1 = 12 columns two eyes with velocity
			if (doVelocityData &&
				myOnVelocityDetected &&
				!myIsVelocityMessagePrinted &&
				(AreFlagsOn(flagsRecording, fEyeLR) || AreFlagsOn(flagsRecording, fEyeL) || AreFlagsOn(flagsRecording, fEyeR))
			   )
			{
				myIsVelocityMessagePrinted = true;
				std::cout << "NOTE: The .asc file contains velocity data";

				if (lineSubStrings.size() == 12) std::cout << " for both eyes.";
				else if (AreFlagsOn(flagsRecording, fEyeL)) std::cout << " for left eye.";
				else std::cout << " for right eye.";

				std::cout << std::endl;

				if ((lineSubStrings.size() != 7 && lineSubStrings.size() != 12)) //one eye or both eyes
					std::cout << "ERROR: Problem with velocity data columns detected!" << std::endl;
			}//end processing velocity data

			FlagsSet(myFlags[pos], flagsRecording);

			if (AreFlagsOn(flagsRecording, fEyeLR)) // START Processing BOTH eyes
			{
				/* START Validation*/
				if (!(Parser::IsValidFloat(*(lineSubStrings.begin() + 1)) && Parser::IsValidFloat(*(lineSubStrings.begin() + 2))))
					Exit(__FILE__, __LINE__, pos);

				if (!(Parser::IsValidFloat(*(lineSubStrings.begin() + 4)) && Parser::IsValidFloat(*(lineSubStrings.begin() + 5))))
					Exit(__FILE__, __LINE__, pos);

				/*if (Parser::IsNegative(*(lineSubStrings.begin() + 1)) || Parser::IsNegative(*(lineSubStrings.begin() + 2)))
					Warning(pos, "Negative value");*/

				/*if (Parser::IsNegative(*(lineSubStrings.begin() + 4)) || Parser::IsNegative(*(lineSubStrings.begin() + 5)))
					Warning(pos, "Negative value");*/
				/* END Validation*/

				// Process LEFT (both EYES section)
				if (!Parser::strcmp(*(lineSubStrings.begin() + 1), ".") && !Parser::strcmp(*(lineSubStrings.begin() + 2), "."))
				{
					myHasLeft = true;

					double	x = GetValueDouble(*(lineSubStrings.begin() + 1));
					double	y = GetValueDouble(*(lineSubStrings.begin() + 2));
					double	p = GetValueDouble(*(lineSubStrings.begin() + 3));
					myXleft[pos] = Parser::double_t(x);
					myYleft[pos] = Parser::double_t(y);
					myPleft[pos] = Parser::double_t(p);

					//START handle velocity
					if (doVelocityData && myOnVelocityDetected && lineSubStrings.size() > 8 && !Parser::strcmp(*(lineSubStrings.begin() + 7), ".") && !Parser::strcmp(*(lineSubStrings.begin() + 8), "."))
					{
						double	xvel = GetValueDouble(*(lineSubStrings.begin() + 7));
						double	yvel = GetValueDouble(*(lineSubStrings.begin() + 8));
						myXleftVel[pos] = Parser::double_t(xvel);
						myYleftVel[pos] = Parser::double_t(yvel);
					}
					//END velocity

					if ((x < 0) || (x >= myHeader.screenWidth))
						FlagsSet(myFlags[pos], fOutOfScreen);
					else if ((y < 0) || (y >= myHeader.screenHeight))
						FlagsSet(myFlags[pos], fOutOfScreen);

					if (inDisplayable)
					{
						Displayable&	displayable = **myDisplayables.rbegin();
						if ((x < displayable.leftRect) || (x >= displayable.rightRect))
							FlagsSet(myFlags[pos], fOutOfDisplayable);
						else if ((y < displayable.topRect) || (y >= displayable.bottomRect))
							FlagsSet(myFlags[pos], fOutOfDisplayable);
					}
				}

				// Process RIGHT (both EYES section)
				if (!Parser::strcmp(*(lineSubStrings.begin() + 4), ".") && !Parser::strcmp(*(lineSubStrings.begin() + 5), "."))
				{
					myHasRight = true;

					double	x = GetValueDouble(*(lineSubStrings.begin() + 4));
					double	y = GetValueDouble(*(lineSubStrings.begin() + 5));
					double	p = GetValueDouble(*(lineSubStrings.begin() + 6));
					myXright[pos] = Parser::double_t(x);
					myYright[pos] = Parser::double_t(y);
					myPright[pos] = Parser::double_t(p);

					//START handle velocity
					if (doVelocityData && myOnVelocityDetected && lineSubStrings.size() > 10 && !Parser::StringsAreEqual(*(lineSubStrings.begin() + 9), ".") && !Parser::StringsAreEqual(*(lineSubStrings.begin() + 10), "."))
					{
						double	xvel = GetValueDouble(*(lineSubStrings.begin() + 9));
						double	yvel = GetValueDouble(*(lineSubStrings.begin() + 10));
						myXrightVel[pos] = Parser::double_t(xvel);
						myYrightVel[pos] = Parser::double_t(yvel);
					}
					//END velocity

					if ((x < 0) || (x >= myHeader.screenWidth))
						FlagsSet(myFlags[pos], fOutOfScreen);
					else if ((y < 0) || (y >= myHeader.screenHeight))
						FlagsSet(myFlags[pos], fOutOfScreen);

					if (inDisplayable)
					{
						Displayable&	displayable = **myDisplayables.rbegin();
						if ((x < displayable.leftRect) || (x >= displayable.rightRect))
							FlagsSet(myFlags[pos], fOutOfDisplayable);
						else if ((y < displayable.topRect) || (y >= displayable.bottomRect))
							FlagsSet(myFlags[pos], fOutOfDisplayable);
					}
				}
			} //END Processing both eyes
			else 
			if (AreFlagsOn(flagsRecording, fEyeL)) //Start processing ONLY LEFT eye
			{
				if (!(Parser::IsValidFloat(*(lineSubStrings.begin() + 1)) && Parser::IsValidFloat(*(lineSubStrings.begin() + 2))))
					Exit(__FILE__, __LINE__, pos);

				/*if (Parser::IsNegative(*(lineSubStrings.begin() + 1)) || Parser::IsNegative(*(lineSubStrings.begin() + 2)))
					Warning(pos, "Negative value");*/

				if (!Parser::strcmp(*(lineSubStrings.begin() + 1), ".") && !Parser::strcmp(*(lineSubStrings.begin() + 2), "."))
				{
					myHasLeft = true;

					double	x = GetValueDouble(*(lineSubStrings.begin() + 1));
					double	y = GetValueDouble(*(lineSubStrings.begin() + 2));
					double	p = GetValueDouble(*(lineSubStrings.begin() + 3));
					myXleft[pos] = Parser::double_t(x);
					myYleft[pos] = Parser::double_t(y);
					myPleft[pos] = Parser::double_t(p);

					//START handle velocity
					if (doVelocityData && myOnVelocityDetected && lineSubStrings.size() > 5 && !Parser::strcmp(*(lineSubStrings.begin() + 4), ".") && !Parser::strcmp(*(lineSubStrings.begin() + 5), "."))
					{
						double	xvel = GetValueDouble(*(lineSubStrings.begin() + 4));
						double	yvel = GetValueDouble(*(lineSubStrings.begin() + 5));
						myXleftVel[pos] = Parser::double_t(xvel);
						myYleftVel[pos] = Parser::double_t(yvel);
					}
					//END velocity

					if ((x < 0) || (x >= myHeader.screenWidth))
						FlagsSet(myFlags[pos], fOutOfScreen);
					else if ((y < 0) || (y >= myHeader.screenHeight))
						FlagsSet(myFlags[pos], fOutOfScreen);

					if (inDisplayable)
					{
						Displayable&	displayable = **myDisplayables.rbegin();
						if ((x < displayable.leftRect) || (x >= displayable.rightRect))
							FlagsSet(myFlags[pos], fOutOfDisplayable);
						else if ((y < displayable.topRect) || (y >= displayable.bottomRect))
							FlagsSet(myFlags[pos], fOutOfDisplayable);
					}
				}
			}
			else 
			if (AreFlagsOn(flagsRecording, fEyeR)) //START processing ONLY RIGHT eye
			{
				if (!(Parser::IsValidFloat(*(lineSubStrings.begin() + 1)) && Parser::IsValidFloat(*(lineSubStrings.begin() + 2))))
					Exit(__FILE__, __LINE__, pos);

				/*if (Parser::IsNegative(*(lineSubStrings.begin() + 1)) || Parser::IsNegative(*(lineSubStrings.begin() + 2)))
					Warning(pos, "Negative value");*/

				if (!Parser::strcmp(*(lineSubStrings.begin() + 1), ".") && !Parser::strcmp(*(lineSubStrings.begin() + 2), "."))
				{
					myHasRight = true;

					double	x = GetValueDouble(*(lineSubStrings.begin() + 1));
					double	y = GetValueDouble(*(lineSubStrings.begin() + 2));
					double	p = GetValueDouble(*(lineSubStrings.begin() + 3));
					myXright[pos] = Parser::double_t(x);
					myYright[pos] = Parser::double_t(y);
					myPright[pos] = Parser::double_t(p);

                    //START handle velocity
					if (doVelocityData && myOnVelocityDetected && lineSubStrings.size() > 5 && !Parser::StringsAreEqual(*(lineSubStrings.begin() + 4), ".") && !Parser::StringsAreEqual(*(lineSubStrings.begin() + 5), "."))
					{
						double	xvel = GetValueDouble(*(lineSubStrings.begin() + 4));
						double	yvel = GetValueDouble(*(lineSubStrings.begin() + 5));
						myXrightVel[pos] = Parser::double_t(xvel);
						myYrightVel[pos] = Parser::double_t(yvel);
					}
					//END velocity

					if ((x < 0) || (x >= myHeader.screenWidth))
						FlagsSet(myFlags[pos], fOutOfScreen);
					else if ((y < 0) || (y >= myHeader.screenHeight))
						FlagsSet(myFlags[pos], fOutOfScreen);

					if (inDisplayable)
					{
						Displayable&	displayable = **myDisplayables.rbegin();
						if ((x < displayable.leftRect) || (x >= displayable.rightRect))
							FlagsSet(myFlags[pos], fOutOfDisplayable);
						else if ((y < displayable.topRect) || (y >= displayable.bottomRect))
							FlagsSet(myFlags[pos], fOutOfDisplayable);
					}
				}
			}
		}
		else 
		if (Parser::strcmp(*lineSubStrings.begin(), "MSG")) //START processing messages
		{
			pos = GetPos(*(lineSubStrings.begin() + 1));

			// Nathalie 20170529
			if (Parser::strcmp(*(lineSubStrings.begin() + 2), "TRIALID"))
				trialIdNat = true;
			else if (trialIdNat && Parser::strcmp(*(lineSubStrings.begin() + 2), "\'Stimulus"))
			{
				imageNameNat = *(lineSubStrings.begin() + 3);
				imageNameNat.erase(imageNameNat.length() - 1);
			}
			else if (trialIdNat && std::string(*(lineSubStrings.begin() + 2)).find("Image_Start") == 0)
			{
				posStartNat = pos;

				FlagsSet(myFlags[pos], fEvent);

				myDisplayables.push_back(new Displayable(pos, imageNameNat));
				(*myDisplayables.rbegin())->fseek = mySeekInd;
			}
			else if (trialIdNat && std::string(*(lineSubStrings.begin() + 2)).find("reponse") == 0)
			{
				trialIdNat = false;
				posStopNat = pos;

				Displayable&	displayable = **myDisplayables.rbegin();
				displayable.End(pos, uint16(**(lineSubStrings.begin() + 3)));
			}
			else if (Parser::strcmp(*(lineSubStrings.begin() + 2), "BEGIN"))
			{
				if (Parser::strcmp(*(lineSubStrings.begin() + 3), "TRANSITION"))
				{
					bool isValidTransistion = true;
					if (lineSubStrings.size() < 18)
					{
						Warning(pos, "Invalide TRANSITION syntax ( !!! verify the EDF2ASC version !!! )\nExactly 18 substrings should be available in ");
						isValidTransistion = false;
					}
					else
						if (lineSubStrings.size() > 18)
						{
							if (!MergeSubstrings(lineSubStrings))
							{
								Warning(pos, "Invalide TRANSITION syntax (can not merge substrings)");
								isValidTransistion = false;
							}
							   
						}

					if (isValidTransistion)
					{
						if (Parser::strcmp(*(lineSubStrings.begin() + 9), "IMAGE"))
							Warning(inDisplayable, "InDisplayable", false, pos);

						FlagsSet(myFlags[pos], fEvent);

						frameIndex = 0;
						

						myDisplayables.push_back(new Displayable(pos, lineSubStrings.begin() + 4));
						(*myDisplayables.rbegin())->fseek = mySeekInd;
					}
				}
				else if (Parser::strcmp(*(lineSubStrings.begin() + 3), "FRAME"))
				{
					if (inDisplayable)
					{
						uint16	lastFrame = uint16(GetValueInt16(*(lineSubStrings.begin() + 4)));
						uint16	deltaFr = uint16(lastFrame - frameIndex);
						frameIndex = lastFrame;

						FlagsSet(myFrames[pos], deltaFr);
					}
				}
				else if (Parser::strcmp(*(lineSubStrings.begin() + 3), "SEQUENCE"))
				{
					Warning(inSequence, "InSequence", false, pos);

					FlagsSet(myFlags[pos], fEvent);

					mySequences.push_back(new CSequence(pos, lineSubStrings.begin() + 4));
					(*mySequences.rbegin())->fseek = mySeekInd;

					indSequence = (*mySequences.rbegin())->index;
				}
				else if (Parser::strcmp(*(lineSubStrings.begin() + 3), "PHASE"))
				{
					Warning(inPhase, "InPhase", false, pos);

					FlagsSet(myFlags[pos], fEvent);

					myAcqPhases.push_back(new AcqPhase(pos, lineSubStrings.begin() + 4));
					(*myAcqPhases.rbegin())->fseek = mySeekInd;
				}
			}
			else if (Parser::strcmp(*(lineSubStrings.begin() + 2), "END"))
			{
				if (Parser::strcmp(*(lineSubStrings.begin() + 3), "TRANSITION"))
				{
					if (myDisplayables.size() > 0)
					{
						Warning(inDisplayable, "InDisplayable", true, pos);

						Displayable&	displayable = **myDisplayables.rbegin();
						displayable.End(pos, lineSubStrings.begin() + 4);
					}
					else
						MSendWarning("TRANSITION myDisplayables = 0");
				}
				else if (Parser::strcmp(*(lineSubStrings.begin() + 3), "SEQUENCE"))
				{
					if (!Warning(inSequence, "InSequence", true, pos))
					{
						mySequences.push_back(new CSequence(flowSequence, 0, ++indSequence, "unknown"));

						CSequence&	sequence = **mySequences.rbegin();
						Record&		record = **myRecords.rbegin();

						sequence.startTime = record.startTime;
						sequence.fseek = record.fseek;
					}

					CSequence&		sequence = **mySequences.rbegin();
					sequence.endTime = pos;

					Displayable&	displayable = **myDisplayables.rbegin();
					if (displayable.startTime == displayable.endTime)
						displayable.endTime = pos;
				}
				else if ((Parser::strcmp(*(lineSubStrings.begin() + 3), "PHASE")) && (myAcqPhases.size() != 0))
				{
					Warning(inPhase, "InPhase", true, pos);

					AcqPhase& acqPhase = **myAcqPhases.rbegin();
					acqPhase.endTime = pos;
				}

			}
		} //END processing messages
		else 
		if(Parser::strcmp(*lineSubStrings.begin(), "SFIX"))
		{	uint32 pos		= GetPos(*(lineSubStrings.begin()+2));
			
			if(Parser::strcmp(*(lineSubStrings.begin() + 1), "L"))
			{	Warning(inFixL, "InFixL", false, pos);
				
				indFixL = mySeekInd;
			}
			else
			{	Warning(inFixR, "InFixR", false, pos);
				
				indFixR = mySeekInd;
		}	}
		else if(Parser::strcmp(*lineSubStrings.begin(), "EFIX")) //START processing FIXATION
		{	pos				= GetPos(*(lineSubStrings.begin()+2));
			uint32 pos1		= GetPos(*(lineSubStrings.begin()+3));
			double startX	= GetValueDouble(*(lineSubStrings.begin()+5));
			double startY	= GetValueDouble(*(lineSubStrings.begin()+6));
			double avgPupilSize = GetValueDouble(*(lineSubStrings.begin() + 7));
				
			if(Parser::strcmp(*(lineSubStrings.begin() + 1), "L"))
			{	Warning(inFixL, "InFixL", true, pos1);

				myFixSaccLeft.push_back(new FixationSaccade(flowFixationLeft, pos, pos1, startX, startY, startX, startY, avgPupilSize));
				(*myFixSaccLeft.rbegin())->fseek = indFixL;
			}
			else
			{	Warning(inFixR, "InFixR", true, pos1);
				
				myFixSaccRight.push_back(new FixationSaccade(flowFixationRight, pos, pos1, startX, startY, startX, startY, avgPupilSize));
				(*myFixSaccRight.rbegin())->fseek = indFixR;
		}	}
		else 
		if (Parser::strcmp(*lineSubStrings.begin(), "SSACC")) //START processing SACCADE
		{
			uint32 pos = GetPos(*(lineSubStrings.begin() + 2));

			if (Parser::strcmp(*(lineSubStrings.begin() + 1), "L"))
			{
				Warning(inSaccL, "InSaccL", false, pos);

				indSaccL = mySeekInd;
			}
			else
			{
				Warning(inSaccR, "InSaccR", false, pos);

				indSaccR = mySeekInd;
			}
		}
		else if(Parser::strcmp(*lineSubStrings.begin(), "ESACC"))
		{	pos				= GetPos(*(lineSubStrings.begin()+2));
			uint32 pos1		= GetPos(*(lineSubStrings.begin()+3));
			double startX	= GetValueDouble(*(lineSubStrings.begin()+5));
			double startY	= GetValueDouble(*(lineSubStrings.begin()+6));
			double endX		= GetValueDouble(*(lineSubStrings.begin()+7));
			double endY		= GetValueDouble(*(lineSubStrings.begin()+8));

			double amplitude = GetValueDouble(*(lineSubStrings.begin() + 9)); 
			double peakVelocity = GetValueDouble(*(lineSubStrings.begin() + 10));
				
			if(Parser::strcmp(*(lineSubStrings.begin() + 1), "L"))
			{	Warning(inSaccL, "InSaccL", true, pos1);
				
				myFixSaccLeft.push_back(new FixationSaccade(flowSaccadeLeft, pos, pos1, startX, startY, endX, endY, amplitude, peakVelocity));
				(*myFixSaccLeft.rbegin())->fseek = indSaccL;
			}
			else
			{	Warning(inSaccR, "InSaccR", true, pos1);
				
				myFixSaccRight.push_back(new FixationSaccade(flowSaccadeRight, pos, pos1, startX, startY, endX, endY, amplitude, peakVelocity));
				(*myFixSaccRight.rbegin())->fseek = indSaccR;
		}	}
		else 
		if(Parser::strcmp(*lineSubStrings.begin(), "SBLINK")) //START processing BLINKS
		{	uint32 pos		= GetPos(*(lineSubStrings.begin()+2));
			
			if(Parser::strcmp(*(lineSubStrings.begin() + 1), "L"))
			{	Warning(inBlinkL, "InBlinkL", false, pos);
				
				indBlinkL = mySeekInd;
			}
			else
			{	Warning(inBlinkR, "InBlinkR", false, pos);
				
				indBlinkR = mySeekInd;
		}	}
		else if(Parser::strcmp(*lineSubStrings.begin(), "EBLINK"))
		{	pos				= GetPos(*(lineSubStrings.begin()+2));
			uint32 pos1		= GetPos(*(lineSubStrings.begin()+3));

			if(Parser::strcmp(*(lineSubStrings.begin() + 1), "L"))
			{	Warning(inBlinkL, "InBlinkL", true, pos1);
				
				myBlinksLeft.push_back(new Blink(flowBlinkLeft, pos, pos1));
				(*myBlinksLeft.rbegin())->fseek = indBlinkL;
			}
			else
			{	Warning(inBlinkR, "InBlinkR", true, pos);
				
				myBlinksRight.push_back(new Blink(flowBlinkRight, pos, pos1));
				(*myBlinksRight.rbegin())->fseek = indBlinkR;
		}	}
		else //END processing SACCADE
		if(Parser::strcmp(*lineSubStrings.begin(), "INPUT"))
		{	uint32	trigger	= GetValueInt16(*lineSubStrings.rbegin());
			if(trigger == lastTrigger)
				continue;

			lastTrigger	= trigger;
			if(trigger)
			{	uint32	pos			= GetPos(*(lineSubStrings.begin()+1));
				myTriggerTab[pos]	= uint16(trigger);
				myTriggers.push_back(new Trigger(pos, uint16(trigger)));
				(*myTriggers.rbegin())->fseek = mySeekInd;
		}	}
		else 
		if(Parser::strcmp(*lineSubStrings.begin(), "START") && Parser::strcmp(*(lineSubStrings.rbegin()+1), "SAMPLES") && Parser::strcmp(*lineSubStrings.rbegin(), "EVENTS"))
		{	startRecord	= GetPos(*(lineSubStrings.begin()+1));	

			if(startRecord == 0)
				Warning(inRecord, "InRecord", false, 1);
			else
			Warning(inRecord, "InRecord", false, startRecord);
		
			flagsRecording	= fRecording;
		
			if(strstr(*it, "LEFT"))
				FlagsSet(flagsRecording, fEyeL);
			if(strstr(*it, "RIGHT"))
				FlagsSet(flagsRecording, fEyeR);
			
			myRecords.push_back(new Record(startRecord));
			Record& record	= **myRecords.rbegin();
			record.fseek	= mySeekInd;
		}
		//added by Anton
		if (Parser::strcmp(*lineSubStrings.begin(), "SAMPLES") && Parser::strcmp(*(lineSubStrings.begin() + 1), "GAZE"))
		{
			startRecord = GetPos(*(lineSubStrings.begin() + 1));

			if (startRecord == 0)
				Warning(inRecord, "InRecord", false, 1);
			else
				Warning(inRecord, "InRecord", false, startRecord);

			if (strstr(*it, "VEL"))
				myOnVelocityDetected = true;
		}
		else if(Parser::strcmp(*lineSubStrings.begin(), "END") && Parser::strcmp(*(lineSubStrings.begin()+2), "SAMPLES") && Parser::strcmp(*(lineSubStrings.begin()+3), "EVENTS"))
		{	stopRecord		= GetPos(*(lineSubStrings.begin()+1));				
			Warning(inRecord, "InRecord", true, stopRecord);
		
			flagsRecording	= 0;
			
			if(myRecords.size())
			{	Record& record	= **myRecords.rbegin();
				record.endTime	= stopRecord;
	}	}	}

	Warning(inDisplayable,	"End - inDisplayable",	false);
	Warning(inFixL,			"End - inFixL",			false);
	Warning(inFixR,			"End - inFixR",			false);
	Warning(inSaccL,		"End - inSaccL",		false);
	Warning(inSaccR,		"End - inSaccR",		false);
	Warning(inBlinkL,		"End - inBlinkL",		false);
	Warning(inBlinkR,		"End - inBlinkR",		false);
	Warning(inPhase,		"End - inPhase",		false);
	Warning(inSequence,		"End - inSequence",		false);
	Warning(inRecord,		"End - inRecord",		false);

	printf("100 %%\n");

	/*if (doVelocityData && !myOnVelocityDetected)
		std::cout << "Warning: Velocity data requested, but no velocity data gas been detected in the .ASC file.\r\n";*/
				
	return SortEvents();
}

static int compare(const void* a, const void* b)
{
	const Event*	event1	= *((const Event**) a);
	const Event*	event2	= *((const Event**) b);

	if(event1->startTime < event2->startTime)
		return -1;
	else if(event1->startTime == event2->startTime)
	{	if(event2->value == recordStep)
			return -1;
		else
			return 0;
	}

	return 1;
}

bool AscRawData::SortEvents(const bool inDebugMode /*= false*/)
{
	if(myHeader.IsFromSoftEye() && (myAcqPhases.size() == 0))
		return MSendError("AscRawData::SortEvents myAcqPhases.size() == 0");

	if(!inDebugMode)
	{	if(myAcqPhases.size())
		{
		Event& acqEvent = **myAcqPhases.rbegin();
			if(acqEvent.endTime <= acqEvent.startTime)
				acqEvent.endTime = myFlags.size() - 1;

			if(myRecords.size())
			{
				Event& event = **myRecords.rbegin();
				if(event.endTime <= event.startTime)
					event.endTime = acqEvent.endTime;
			}

			if(mySequences.size())
			{
				Event& event = **mySequences.rbegin();
				if(event.endTime <= event.startTime)
					event.endTime = acqEvent.endTime;
			}
			if(myDisplayables.size())
			{
				Event& event = **myDisplayables.rbegin();
				if(event.endTime <= event.startTime)
					event.endTime = acqEvent.endTime;
			}

			if(!ProcessFrames())
				return MSendError("Can not process frames");

			if(myTransitions.size() == 0)
			{	for(std::vector<Displayable*>::iterator it=myDisplayables.begin(); it != myDisplayables.end(); it++)
					myTransitions.push_back(new CTransition(**it));
			}
			else
			{	for(std::vector<CTransition*>::iterator it=myTransitions.begin(); it != myTransitions.end(); it++)
					(*it)->startTime	= (*it)->endTime;
			}
		}
		else
		{
			if(myRecords.size())
			{
				Event& event = **myRecords.rbegin();
				if(event.endTime != myCurentPos)
					event.endTime = myCurentPos;
			}
		}

		ProcessBlink();
	}

	mySortedEvents.insert(mySortedEvents.end(), mySequences.begin(),			mySequences.end());
	mySortedEvents.insert(mySortedEvents.end(), myDisplayables.begin(),			myDisplayables.end());
	if(myOnVideoframe)
		mySortedEvents.insert(mySortedEvents.end(), myDisplayableFrames.begin(),myDisplayableFrames.end());
	mySortedEvents.insert(mySortedEvents.end(), myTransitions.begin(),			myTransitions.end());
	mySortedEvents.insert(mySortedEvents.end(), myFixSaccLeft.begin(),			myFixSaccLeft.end());
	mySortedEvents.insert(mySortedEvents.end(), myFixSaccRight.begin(),			myFixSaccRight.end());
	mySortedEvents.insert(mySortedEvents.end(), myBlinksLeft.begin(),			myBlinksLeft.end());
	mySortedEvents.insert(mySortedEvents.end(), myBlinksRight.begin(),			myBlinksRight.end());
	mySortedEvents.insert(mySortedEvents.end(), myAcqPhases.begin(),			myAcqPhases.end());
	mySortedEvents.insert(mySortedEvents.end(), myRecords.begin(),				myRecords.end());

	::qsort(&mySortedEvents[0], mySortedEvents.size(), sizeof(void*), compare);

	int nbWrongSeekT = 0;
	int nbWrongSeekN = 0;
	int nbWrongTime = 0;
	for(std::vector<Event*>::iterator it=mySortedEvents.begin(), it1=it+1; it1 != mySortedEvents.end(); it1++, it++)
	{	if((*it1)->fseek < (*it)->fseek)
		{	if(	(((*it1)->value >= transitionTime) && ((*it1)->value <= transitionSpecial)) ||
				(((*it)->value >= transitionTime) && ((*it)->value <= transitionSpecial)))
				nbWrongSeekT++;
			else
				nbWrongSeekN++;
		}
		if((*it1)->startTime < (*it)->startTime)
			nbWrongTime++;		
	}

	return true;
}

void AscRawData::InterpolateFalseBlink(std::vector<Parser::double_t>& pos, const size_t start, const size_t stop)
{
	double s	= pos[start];
	double e	= pos[stop];
	double diff	= e - s;
	size_t pix	= stop - start;

	for(size_t ii=start+1, jj=1; ii < stop; ii++, jj++)
		pos[ii]	= s + (jj*diff)/pix;
}

void AscRawData::RemoveFalseBlinks(std::vector<Blink*>& blinks, std::vector<Parser::double_t>& xPos, std::vector<Parser::double_t>& yPos)
{
	size_t sizeBefore = blinks.size();

	bool found = true;
	while(found)
	{	found	= false;
		for(std::vector<Blink*>::iterator it=blinks.begin(); it != blinks.end(); it++)
		{	Blink*	blink = *it;

			if((blink->endTime - blink->startTime) > myHeader.falseBlinkSamples)
				continue;

			double sx_1	= xPos[blink->startTime-1];
			double sy_1	= yPos[blink->startTime-1];
			
			double ex1	= xPos[blink->endTime+1];
			double ey1	= yPos[blink->endTime+1];

			if((sx_1 == myValNaN) || (sy_1 == myValNaN) || (ex1 == myValNaN) || (ey1 == myValNaN))
				continue;

			InterpolateFalseBlink(xPos, blink->startTime-1, blink->endTime+1);
			InterpolateFalseBlink(yPos, blink->startTime-1, blink->endTime+1);

			size_t sBefore = blinks.size();
			delete blink;			
			blinks.erase(it);
			size_t sAfter = blinks.size();

			found = true;
			break;
	}	}

	size_t sizeAfter = blinks.size();
}


void AscRawData::ProcessBlink()
{
	RemoveFalseBlinks(myBlinksLeft, myXleft, myYleft);
	RemoveFalseBlinks(myBlinksRight, myXright, myYright);

	//myBlinkNaN = -abs(myValNaN)/5;
	Parser::double_t blinkAvailable = 1;

	size_t nb = 0;
	for (std::vector<Blink*>::iterator it = myBlinksLeft.begin(); it != myBlinksLeft.end(); it++, nb++)
	{
		Blink&	blink = **it;
		for (size_t ii = blink.startTime; ii <= blink.endTime; ii++)
			//myBlink[ii]	= myBlinkNaN;
			myBlink[ii] = blinkAvailable;
	}
	for (std::vector<Blink*>::iterator it = myBlinksRight.begin(); it != myBlinksRight.end(); it++, nb++)
	{
		Blink&	blink = **it;
		for (size_t ii = blink.startTime; ii <= blink.endTime; ii++)
			//myBlink[ii]	= myBlinkNaN;
			myBlink[ii] = blinkAvailable;
	}
}

void AscRawData::CorrectData(std::vector<Parser::double_t>& data, const Parser::double_t maxVal)
{
	for(std::vector<Parser::double_t>::iterator it=data.begin(), it1; it != data.end(); it++)
	{	Parser::double_t val = *it;
		if((val != myValNaN) && ((val < 0) || (val >= maxVal)))
		{	for(it1=it; it1 != data.end(); it1++)
			{	Parser::double_t val1 = *it1;
				if(!((val1 != myValNaN) && ((val1 < 0) || (val1 >= maxVal))))
					break;
			}

			size_t	before	= it - data.begin();
			size_t	after	= data.end() - it1;
			std::vector<Parser::double_t>::iterator itB		= myBlink.begin() + before;
			std::vector<Parser::double_t>::iterator itB1	= myBlink.begin() + (it1 - data.begin());
			size_t	before1	= (before > CORRECT_DIM_PLUS)	? CORRECT_DIM_PLUS : before;
			size_t	after1	= (after > CORRECT_DIM_PLUS)	? CORRECT_DIM_PLUS : after;

			std::vector<Parser::double_t> correctDB(it-before1, it);
			std::vector<Parser::double_t> correctD(it, it1);
			std::vector<Parser::double_t> correctDA(it1, it1+after1);

			std::vector<Parser::double_t> correctBB(itB-before1, itB);
			std::vector<Parser::double_t> correctB(itB, itB1);
			std::vector<Parser::double_t> correctBA(itB1, itB1+after1);

			if(std::find(correctBB.begin(), correctBB.end(), myBlinkNaN) != correctBB.end())
			{	size_t	nb1	 = (after1 > CORRECT_DIM) ? CORRECT_DIM : after1;
				size_t	nb	 = before1 + (it1 - it) + nb1;
				itB			-= before1;
				while(nb--)
					*itB++ = myBlinkNaN;
				it = it1 - 1;
			}
			else if(std::find(correctBA.begin(), correctBA.end(), myBlinkNaN) != correctBA.end())
			{	size_t	nb1	 = (before1 > CORRECT_DIM) ? CORRECT_DIM : before1;
				size_t	nb	 = after1 + (it1 - it) + nb1;
				itB			-= nb1;
				while(nb--)
					*itB++ = myBlinkNaN;
				it = it1 - 1;
			}
			else
			{
				it = it1 - 1;
	}	}	}
}

bool AscRawData::OrganizeData(GazeEegEnv& gazeEegEnv)
{			
	myNbChannels	= 0;
	
	Parser::double_t*	pChan[12]; //increased max number of channels (by Anton)
	
	if (myHasLeft)
	{
		myChannelNames.push_back("eyeLx");
		pChan[myNbChannels++] = &myXleft[0];

		myChannelNames.push_back("eyeLy");
		pChan[myNbChannels++] = &myYleft[0];

		if (myOnPupil)
		{
			myChannelNames.push_back("eyeLp");
			pChan[myNbChannels++] = &myPleft[0];
		}
	}
	
	if (myHasRight)
	{
		myChannelNames.push_back("eyeRx");
		pChan[myNbChannels++] = &myXright[0];

		myChannelNames.push_back("eyeRy");
		pChan[myNbChannels++] = &myYright[0];

		if (myOnPupil)
		{
			myChannelNames.push_back("eyeRp");
			pChan[myNbChannels++] = &myPright[0];
		}
	}
	
	if (gazeEegEnv.doVelocityData && myHasLeft && myOnVelocityDetected)
	{
		myChannelNames.push_back("eyeLxVel");
		pChan[myNbChannels++] = &myXleftVel[0];
		myChannelNames.push_back("eyeLyVel");
		pChan[myNbChannels++] = &myYleftVel[0];
	}

	if (gazeEegEnv.doVelocityData && myHasRight && myOnVelocityDetected)
	{
		myChannelNames.push_back("eyeRxVel");
		pChan[myNbChannels++] = &myXrightVel[0];
		myChannelNames.push_back("eyeRyVel");
		pChan[myNbChannels++] = &myYrightVel[0];
	}

	myChannelNames.push_back("blink");
	pChan[myNbChannels++]	= &myBlink[0];

	// DIG myNbSamples				= myCurentPos + 1;

	if(!(myNbSamples && myNbChannels))
		return MSendError("AscRawData::OrganizeData !(myNbSamples && myNbChannels)");

	myData.resize(myNbSamples * myNbChannels);
	Parser::double_t*	pData = &myData[0];

	for (uint32 ii = 0; ii < myNbSamples; ii++)
	{
		for (uint32 jj = 0; jj < myNbChannels; jj++)
		{
			if (gazeEegEnv.doRounding) 
				*pData++ = round(*(pChan[jj])); //perform rounding to avoid truncating data when saving it to the he .eeg file from float/double to int16
			else *pData++ = *(pChan[jj]);
			(pChan[jj])++;
		}
	}

	myXleft.clear();
	myYleft.clear();
	myPleft.clear();
	myXleftVel.clear();

	myXright.clear();
	myYright.clear();
	myPright.clear();
	myXrightVel.clear();
	
	myBlink.clear();

	return true;
}

bool AscRawData::SaveDebugData(const GazeEegEnv& gazeEegEnv)
{
	if(!gazeEegEnv.doDebug)
		return true;

	std::ofstream	fs((gazeEegEnv.gazeFileName + ".dbg").c_str(), std::ios::out | std::ios::binary);
	if(!fs.good())
		return false;

	uint32	size;
	double	val;

	val		= myValNaN;
	fs.write((const char *) &val, sizeof(val));
	
	val		= myBlinkNaN;
	fs.write((const char *) &val, sizeof(val));

	size = mySeekInd;
	fs.write((const char *) &size, sizeof(size));
	
	size = myFileSeeks.size();
	fs.write((const char *) &size, sizeof(size));
	size	*= sizeof(uint32);
	fs.write((const char *) &myFileSeeks[0], size);

	size = myFlags.size();
	fs.write((const char *) &size, sizeof(size));
	size	*= sizeof(uint8);
	fs.write((const char *) &myFlags[0], size);

	size = myFrames.size();
	fs.write((const char *) &size, sizeof(size));
	size	*= sizeof(uint16);
	fs.write((const char *) &myFrames[0], size);

	size = myTriggerTab.size();
	fs.write((const char *) &size, sizeof(size));
	size	*= sizeof(uint16);
	fs.write((const char *) &myTriggerTab[0], size);

	size = myHasLeft ? 1 : 0;
	fs.write((const char *) &size, sizeof(size));
	
	size = myHasRight ? 1 : 0;
	fs.write((const char *) &size, sizeof(size));

	size = myData.size();
	fs.write((const char *) &size, sizeof(size));
	size	*= sizeof(Parser::double_t);
	fs.write((const char *) &myData[0], size);

	size = mySequences.size();
	fs.write((const char *) &size, sizeof(size));
	for(std::vector<CSequence*>::iterator it=mySequences.begin(); it != mySequences.end(); it++)
	{	CSequence& obj = **it;
		fs.write((const char *) &obj, sizeof(obj));
	}

	size = myDisplayables.size();
	fs.write((const char *) &size, sizeof(size));
	for(std::vector<Displayable*>::iterator it=myDisplayables.begin(); it != myDisplayables.end(); it++)
	{	Displayable& obj = **it;
		fs.write((const char *) &obj, sizeof(DisplayableNum));
	}

	size = myDisplayableFrames.size();
	fs.write((const char *) &size, sizeof(size));
	for(std::vector<Frame*>::iterator it=myDisplayableFrames.begin(); it != myDisplayableFrames.end(); it++)
	{	Frame& obj = **it;
		fs.write((const char *) &obj, sizeof(obj));
	}

	size = myTransitions.size();
	fs.write((const char *) &size, sizeof(size));
	for(std::vector<CTransition*>::iterator it=myTransitions.begin(); it != myTransitions.end(); it++)
	{	CTransition& obj = **it;
		fs.write((const char *) &obj, sizeof(CTransitionNum));
	}

	size = myFixSaccLeft.size();
	fs.write((const char *) &size, sizeof(size));
	for(std::vector<FixationSaccade*>::iterator it=myFixSaccLeft.begin(); it != myFixSaccLeft.end(); it++)
	{	FixationSaccade& obj = **it;
		fs.write((const char *) &obj, sizeof(obj));
	}

	size = myFixSaccRight.size();
	fs.write((const char *) &size, sizeof(size));
	for(std::vector<FixationSaccade*>::iterator it=myFixSaccRight.begin(); it != myFixSaccRight.end(); it++)
	{	FixationSaccade& obj = **it;
		fs.write((const char *) &obj, sizeof(obj));
	}

	size = myBlinksLeft.size();
	fs.write((const char *) &size, sizeof(size));
	for(std::vector<Blink*>::iterator it=myBlinksLeft.begin(); it != myBlinksLeft.end(); it++)
	{	Blink& obj = **it;
		fs.write((const char *) &obj, sizeof(obj));
	}

	size = myBlinksRight.size();
	fs.write((const char *) &size, sizeof(size));
	for(std::vector<Blink*>::iterator it=myBlinksRight.begin(); it != myBlinksRight.end(); it++)
	{	Blink& obj = **it;
		fs.write((const char *) &obj, sizeof(obj));
	}

	size = myAcqPhases.size();
	fs.write((const char *) &size, sizeof(size));
	for(std::vector<AcqPhase*>::iterator it=myAcqPhases.begin(); it != myAcqPhases.end(); it++)
	{	AcqPhase& obj = **it;
		fs.write((const char *) &obj, sizeof(obj));
	}

	size = myRecords.size();
	fs.write((const char *) &size, sizeof(size));
	for(std::vector<Record*>::iterator it=myRecords.begin(); it != myRecords.end(); it++)
	{	Record& obj = **it;
		fs.write((const char *) &obj, sizeof(obj));
	}

	size = myTriggers.size();
	fs.write((const char *) &size, sizeof(size));
	for(std::vector<Trigger*>::iterator it=myTriggers.begin(); it != myTriggers.end(); it++)
	{	Trigger& obj = **it;
		fs.write((const char *) &obj, sizeof(obj));
	}

	size = myStartTimestamp;
	fs.write((const char *) &size, sizeof(size));

	size = myEndTimestamp;
	fs.write((const char *) &size, sizeof(size));

	size = myNbSamples;
	fs.write((const char *) &size, sizeof(size));

	size = myNbChannels;
	fs.write((const char *) &size, sizeof(size));

	size = myChannelNames.size();
	fs.write((const char *) &size, sizeof(size));

	for(std::vector<std::string>::iterator it=myChannelNames.begin(); it != myChannelNames.end(); it++)
	{	std::string& obj = *it;
		size = obj.length();
		fs.write((const char *) &size, sizeof(size));
		fs.write((const char *) &obj[0], size);
	}

	return true;
}

bool AscRawData::LoadDebugData(const GazeEegEnv& gazeEegEnv)
{
	if(!gazeEegEnv.doDebug)
		return false;

	std::ifstream	fs((gazeEegEnv.gazeFileName + ".dbg").c_str(), std::ios::in | std::ios::binary);
	if(!fs.good())
		return false;

	uint32 size;
	double	val;

	fs.read((char *) &val, sizeof(val));
	myValNaN = val;

	fs.read((char *) &val, sizeof(val));
	myBlinkNaN = val;

	fs.read((char *) &size, sizeof(size));
	mySeekInd = size;
	
	fs.read((char *) &size, sizeof(size));
	myFileSeeks.resize(size, 0);
	size	*= sizeof(uint32);
	fs.read((char *) &myFileSeeks[0], size);

	fs.read((char *) &size, sizeof(size));
	myFlags.resize(size, 0);
	size	*= sizeof(uint8);
	fs.read((char *) &myFlags[0], size);

	fs.read((char *) &size, sizeof(size));
	myFrames.resize(size, 0);
	size	*= sizeof(uint16);
	fs.read((char *) &myFrames[0], size);

	fs.read((char *) &size, sizeof(size));
	myTriggerTab.resize(size, 0);
	size	*= sizeof(uint16);
	fs.read((char *) &myTriggerTab[0], size);

	fs.read((char *) &size, sizeof(size));
	myHasLeft = size != 0;
	
	fs.read((char *) &size, sizeof(size));
	myHasRight = size != 0;
	
	fs.read((char *) &size, sizeof(size));
	myData.resize(size, 0);
	size	*= sizeof(Parser::double_t);
	fs.read((char *) &myData[0], size);

	fs.read((char *) &size, sizeof(size));
	mySequences.clear();
	mySequences.reserve(size);
	for(size_t ii=0; ii < size; ii++)
	{	CSequence* obj = new CSequence(0, 0, 0, "");
		fs.read((char *) obj, sizeof(*obj));
		mySequences.push_back(obj);
	}

	fs.read((char *) &size, sizeof(size));
	myDisplayables.clear();
	myDisplayables.reserve(size);
	for(size_t ii=0; ii < size; ii++)
	{	Displayable*	obj		= new Displayable(0, 0, "", "");
		fs.read((char *) obj, sizeof(DisplayableNum));
		myDisplayables.push_back(obj);
	}

	fs.read((char *) &size, sizeof(size));
	myDisplayableFrames.clear();
	myDisplayableFrames.reserve(size);
	for(size_t ii=0; ii < size; ii++)
	{	Frame* obj = new Frame(0);
		fs.read((char *) obj, sizeof(*obj));
		myDisplayableFrames.push_back(obj);
	}

	fs.read((char *) &size, sizeof(size));
	myTransitions.clear();
	myTransitions.reserve(size);
	for(size_t ii=0; ii < size; ii++)
	{	CTransition* obj = new CTransition(0, 0, "", 0);
		fs.read((char *) obj, sizeof(CTransitionNum));
		myTransitions.push_back(obj);
	}

	fs.read((char *) &size, sizeof(size));
	myFixSaccLeft.clear();
	myFixSaccLeft.reserve(size);
	for(size_t ii=0; ii < size; ii++)
	{	FixationSaccade* obj = new FixationSaccade(0, 0, 0, 0, 0, 0, 0);
		fs.read((char *) obj, sizeof(*obj));
		myFixSaccLeft.push_back(obj);
	}

	fs.read((char *) &size, sizeof(size));
	myFixSaccRight.clear();
	myFixSaccRight.reserve(size);
	for(size_t ii=0; ii < size; ii++)
	{	FixationSaccade* obj = new FixationSaccade(0, 0, 0, 0, 0, 0, 0);
		fs.read((char *) obj, sizeof(*obj));
		myFixSaccRight.push_back(obj);
	}

	fs.read((char *) &size, sizeof(size));
	myBlinksLeft.clear();
	myBlinksLeft.reserve(size);
	for(size_t ii=0; ii < size; ii++)
	{	Blink* obj = new Blink(0, 0, 0);
		fs.read((char *) obj, sizeof(*obj));
		myBlinksLeft.push_back(obj);
	}

	fs.read((char *) &size, sizeof(size));
	myBlinksRight.clear();
	myBlinksRight.reserve(size);
	for(size_t ii=0; ii < size; ii++)
	{	Blink* obj = new Blink(0, 0, 0);
		fs.read((char *) obj, sizeof(*obj));
		myBlinksRight.push_back(obj);
	}

	fs.read((char *) &size, sizeof(size));
	myAcqPhases.clear();
	myAcqPhases.reserve(size);
	for(size_t ii=0; ii < size; ii++)
	{	AcqPhase* obj = new AcqPhase(0, 0);
		fs.read((char *) obj, sizeof(*obj));
		myAcqPhases.push_back(obj);
	} 

	fs.read((char *) &size, sizeof(size));
	myRecords.clear();
	myRecords.reserve(size);
	for(size_t ii=0; ii < size; ii++)
	{	Record* obj = new Record(0, 0);
		fs.read((char *) obj, sizeof(*obj));
		myRecords.push_back(obj);
	}

	fs.read((char *) &size, sizeof(size));
	myTriggers.clear();
	myTriggers.reserve(size);
	for(size_t ii=0; ii < size; ii++)
	{	Trigger* obj = new Trigger(0, 0);
		fs.read((char *) obj, sizeof(*obj));
		myTriggers.push_back(obj);
	}

	fs.read((char *) &size, sizeof(size));
	myStartTimestamp	= size;

	fs.read((char *) &size, sizeof(size));
	myEndTimestamp	= size;

	fs.read((char *) &size, sizeof(size));
	myNbSamples	= size;

	fs.read((char *) &size, sizeof(size));
	myNbChannels	= size;
	
	fs.read((char *) &size, sizeof(size));
	myChannelNames.clear();
	myChannelNames.reserve(size);
	for(size_t ii=0; ii < size; ii++)
	{	uint32 sz;
		fs.read((char *) &sz, sizeof(sz));
		std::string str(sz, ' ');
		fs.read((char *) &str[0], sz);
		myChannelNames.push_back(str);
	}

	SortEvents(true);

	return true;
}

void AscRawData::GetTriggerMarkers(MarkerList_t& markerList)
{
	markerList.clear();
	markerList.reserve(myTriggers.size());

	int	index = 0;
	for(std::vector<Trigger*>::const_iterator it=myTriggers.begin(); it < myTriggers.end(); it++, index++)
		markerList.push_back(CMarker("", uint16((*it)->value), uint32((*it)->startTime), uint32((*it)->endTime - (*it)->startTime), (*it)->fseek));
}

void AscRawData::GetEventMarkers(MarkerList_t& markerList)
{
	markerList.clear();
	markerList.reserve(mySortedEvents.size());

	int	index = 0;
	for(std::vector<Event*>::const_iterator it=mySortedEvents.begin(); it < mySortedEvents.end(); it++, index++)
	{	markerList.push_back(CMarker("", uint16((*it)->value), uint32((*it)->startTime), uint32((*it)->endTime - (*it)->startTime), 0));
		markerList.rbegin()->srcEvent = *it;
	}
}

bool AscRawData::SaveMatFile(GazeEegEnv& gazeEegEnv)
{
	mat_t* matFile	= Mat_Create(gazeEegEnv.gazeMatFileName.c_str(), 0);
	if(!matFile)
		return MSendError1("AscRawData::SaveMatFile creating MAT file", gazeEegEnv.gazeMatFileName);

	MSendDebug1("Saving     : ", gazeEegEnv.gazeMatFileName);
	
	std::string	path, name, ext;
	Parser::ParsePath(path, name, ext, gazeEegEnv.gazeMatFileName);
	gazeEegEnv.eventFileName = path + "\\" + name + MEventFile;

	MatComponents allComponents;

	// header section
	allComponents.push_back(myHeader.GetComponents());

	// data section
	{
		MatComponents dataComponents;

		{
			MatComponents flagComponents;

			flagComponents.AddVariable("tInt16",		MAT_C_UINT16,	uint16(Parser::int16_type));
			flagComponents.AddVariable("tFloat32",		MAT_C_UINT16,	uint16(Parser::float_type));
			flagComponents.AddVariable("tFloat64",		MAT_C_UINT16,	uint16(Parser::double_type));

			matvar_t* pStruct = flagComponents.GetStructure("BinaryFormat");
			dataComponents.push_back(pStruct);
		}

		{
			MatComponents flagComponents;

			flagComponents.AddVariable("MULTIPLEXED",	MAT_C_UINT16,	uint16(0));
			flagComponents.AddVariable("VECTORIZED",	MAT_C_UINT16,	uint16(1));

			matvar_t* pStruct = flagComponents.GetStructure("DataOrientation");
			dataComponents.push_back(pStruct);
		}

		{
			MatComponents flagComponents;

			flagComponents.AddVariable("FREQUENCYDOMAIN",	MAT_C_UINT16,	uint16(0));
			flagComponents.AddVariable("TIMEDOMAIN",		MAT_C_UINT16,	uint16(1));

			matvar_t* pStruct = flagComponents.GetStructure("DataType");
			dataComponents.push_back(pStruct);
		}

		{
			MatComponents flagComponents;

			flagComponents.AddVariable("NOSEGMENTED",	MAT_C_UINT16,	uint16(0));
			flagComponents.AddVariable("MARKERBASED",	MAT_C_UINT16,	uint16(1));

			matvar_t* pStruct = flagComponents.GetStructure("SegmentationType");
			dataComponents.push_back(pStruct);
		}

		dataComponents.AddVariable("fseek",			MAT_C_UINT32,	myFileSeeks);

		dataComponents.push_back(GetParamsComponents(1000/myHeader.msecPerSample, myNbChannels, myNbSamples));			

		dataComponents.AddVariable("raw", myNbSamples,	MAT_C_DOUBLE,	myData);

		for(std::vector<Event*>::iterator it=mySortedEvents.begin(); it < mySortedEvents.end(); it++)
		{	(*it)->startTime++;	// adapt to MATLAB indexing
			(*it)->endTime++;	// adapt to MATLAB indexing
		}

		for(std::vector<Trigger*>::iterator it=myTriggers.begin(); it < myTriggers.end(); it++)
		{	(*it)->startTime++;	// adapt to MATLAB indexing
			(*it)->endTime++;	// adapt to MATLAB indexing
		}

		matvar_t* pStruct = dataComponents.GetStructure("Data");
		allComponents.push_back(pStruct);
	}

	// event section
	{
		MatComponents eventComponents;

		GetEventTypeComponents(eventComponents, gazeEegEnv);
		
		if(myAcqPhases.size())
		{
			AcqPhases	events(myAcqPhases.size());
			size_t		ii = 0;
			for(std::vector<AcqPhase*>::iterator it=myAcqPhases.begin(); it != myAcqPhases.end(); it++, ii++)
			{	events.value[ii]			= uint16((*it)->value);
				events.time[2*ii]			= (*it)->startTime;
				events.time[2*ii+1]			= (*it)->endTime;
				events.fseek[ii]			= (*it)->fseek;
			}
		
			matvar_t* pStruct = events.GetComponents("AcqPhases", false);
			eventComponents.push_back(pStruct);
		}

		if(myRecords.size())
		{
			Records		events(myRecords.size());
			size_t		ii = 0;
			for(std::vector<Record*>::iterator it=myRecords.begin(); it != myRecords.end(); it++, ii++)
			{	events.value[ii]			= uint16((*it)->value);
				events.time[2*ii]			= (*it)->startTime;
				events.time[2*ii+1]			= (*it)->endTime;
				events.fseek[ii]			= (*it)->fseek;
			}
		
			matvar_t* pStruct = events.GetComponents("RecordSteps", false);
			eventComponents.push_back(pStruct);
		}

		if(mySequences.size())
		{
			Sequences	events(mySequences.size());
			size_t		ii = 0;
			for(std::vector<CSequence*>::iterator it=mySequences.begin(); it != mySequences.end(); it++, ii++)
			{	events.value[ii]			= uint16((*it)->value);
				events.time[2*ii]		= (*it)->startTime;
				events.time[2*ii+1]		= (*it)->endTime;
				events.fseek[ii]		= (*it)->fseek;
				events.index[ii]		= (*it)->index;
				memcpy(&events.name[ii*NameSize], (*it)->name.c_str(), (*it)->name.length());
			}
		
			Parser::Transpose(events.name, NameSize);
			matvar_t* pStruct = events.GetComponents("Sequences", false);
			eventComponents.push_back(pStruct);
		}

		if(myDisplayables.size())
		{
			Displayables	events(myDisplayables.size());
			size_t		ii = 0;
			for(std::vector<Displayable*>::iterator it=myDisplayables.begin(); it != myDisplayables.end(); it++, ii++)
			{	
				events.value[ii]			= uint16((*it)->value);
				events.time[2*ii]			= (*it)->startTime;
				events.time[2*ii+1]			= (*it)->endTime;
				events.fseek[ii]			= (*it)->fseek;
				events.frameDuration[ii]	= (*it)->frameDuration;
				events.frameNb[ii]			= (*it)->nbFrames;
				events.framePlayed[ii]		= (*it)->playedFrames;
				events.frameFirst[ii]		= (*it)->firstFrame;
				events.leftRect[ii]			= (*it)->leftRect;
				events.topRect[ii]			= (*it)->topRect;
				events.rightRect[ii]		= (*it)->rightRect;
				events.bottomRect[ii]		= (*it)->bottomRect;
				memcpy(&events.name[ii*NameSize], (*it)->name.c_str(), (*it)->name.length());
				memcpy(&events.relativePath[ii*PathSize], (*it)->relativePath.c_str(), (*it)->relativePath.length());
			}

			Parser::Transpose(events.name, NameSize);
			Parser::Transpose(events.relativePath, PathSize);
			matvar_t* pStruct = events.GetComponents("Displayables", false);
			eventComponents.push_back(pStruct);
		}

		if(myOnVideoframe && myDisplayableFrames.size())
		{
			Frames	events(myDisplayableFrames.size());
			size_t		ii = 0;
			for(std::vector<Frame*>::iterator it=myDisplayableFrames.begin(); it != myDisplayableFrames.end(); it++, ii++)
			{	
				events.value[ii]			= uint16((*it)->value);
				events.time[2*ii]			= (*it)->startTime;
				events.time[2*ii+1]			= (*it)->endTime;
				events.fseek[ii]			= (*it)->fseek;
			}

			matvar_t* pStruct = events.GetComponents("Frames", false);
			eventComponents.push_back(pStruct);
		}

		if(myTransitions.size())
		{
			Transitions	events(myTransitions.size());
			size_t		ii = 0;
			for(std::vector<CTransition*>::iterator it=myTransitions.begin(); it != myTransitions.end(); it++, ii++)
			{	
				events.value[ii]			= uint16((*it)->value);
				events.time[2*ii]			= (*it)->startTime;
				events.time[2*ii+1]			= (*it)->endTime;
				events.fseek[ii]			= (*it)->fseek;
				events.typeAsked[ii]		= (*it)->typeAsked;
				events.timeout[ii]			= (*it)->timeout;
				events.nbEvents[ii]			= (*it)->nbEvents;
				events.stopAttribute[ii]	= (*it)->stopAttribute;
				events.posX[ii]				= (*it)->posX;
				events.posY[ii]				= (*it)->posY;
				memcpy(&events.name[ii*NameSize], (*it)->name.c_str(), (*it)->name.length());
			}
		
			Parser::Transpose(events.name, NameSize);
			matvar_t* pStruct = events.GetComponents("Transitions", false);
			eventComponents.push_back(pStruct);
		}

		//START Add Fixations and Saccades - LEFT and RIGHT
		//LEFT
		if(myFixSaccLeft.size())
		{
			FixationSaccades	events(myFixSaccLeft.size());
			size_t		ii = 0;
			for(std::vector<FixationSaccade*>::iterator it=myFixSaccLeft.begin(); it != myFixSaccLeft.end(); it++, ii++)
			{	
				events.value[ii]			= uint16((*it)->value); //code of the event ex: 1017 (flowFixationRight)
				events.time[2*ii]			= (*it)->startTime;
				events.time[2*ii+1]			= (*it)->endTime;
				events.fseek[ii]			= (*it)->fseek;
				events.startPos[2*ii]		= (*it)->startX;
				events.startPos[2*ii+1]		= (*it)->startY;
				events.endPos[2*ii]			= (*it)->endX;
				events.endPos[2*ii+1]		= (*it)->endY;

				//3 added by Anton later
				events.averagePupilSizeFixation[ii] = (*it)->averagePupilSizeFixation;
				events.amplitudeSaccade[ii]         = (*it)->amplitudeSaccade;
				events.peakVelocitySaccade[ii]      = (*it)->peakVelocitySaccade;
			}
		
			matvar_t* pStruct = events.GetComponents("FixationSaccadesLeft", false);
			eventComponents.push_back(pStruct);
		}
		//RIGHT
		if(myFixSaccRight.size())
		{
			FixationSaccades	events(myFixSaccRight.size());
			size_t		ii = 0;
			for(std::vector<FixationSaccade*>::iterator it=myFixSaccRight.begin(); it != myFixSaccRight.end(); it++, ii++)
			{	
				events.value[ii]			= uint16((*it)->value); //code of the event ex: 1017 (flowFixationRight)
				events.time[2*ii]			= (*it)->startTime;
				events.time[2*ii+1]			= (*it)->endTime;
				events.fseek[ii]			= (*it)->fseek;
				events.startPos[2*ii]		= (*it)->startX;
				events.startPos[2*ii+1]		= (*it)->startY;
				events.endPos[2*ii]			= (*it)->endX;
				events.endPos[2*ii+1]		= (*it)->endY;

				//3 added by Anton later
				events.averagePupilSizeFixation[ii] = (*it)->averagePupilSizeFixation;
				events.amplitudeSaccade[ii]         = (*it)->amplitudeSaccade;
				events.peakVelocitySaccade[ii]      = (*it)->peakVelocitySaccade;
			}
		
			matvar_t* pStruct = events.GetComponents("FixationSaccadesRight", false);
			eventComponents.push_back(pStruct);
		}
		//END Add Fixations and Saccades

		if(myBlinksLeft.size())
		{
			Blinks	events(myBlinksLeft.size());
			size_t		ii = 0;
			for(std::vector<Blink*>::iterator it=myBlinksLeft.begin(); it != myBlinksLeft.end(); it++, ii++)
			{	events.value[ii]			= uint16((*it)->value);
				events.time[2*ii]			= (*it)->startTime;
				events.time[2*ii+1]			= (*it)->endTime;
				events.fseek[ii]			= (*it)->fseek;
			}
		
			matvar_t* pStruct = events.GetComponents("BlinksLeft", false);
			eventComponents.push_back(pStruct);
		}

		if(myBlinksRight.size())
		{
			Blinks	events(myBlinksRight.size());
			size_t		ii = 0;
			for(std::vector<Blink*>::iterator it=myBlinksRight.begin(); it != myBlinksRight.end(); it++, ii++)
			{	events.value[ii]			= uint16((*it)->value);
				events.time[2*ii]			= (*it)->startTime;
				events.time[2*ii+1]			= (*it)->endTime;
				events.fseek[ii]			= (*it)->fseek;
			}
		
			matvar_t* pStruct = events.GetComponents("BlinksRight", false);
			eventComponents.push_back(pStruct);
		}

		if(myTriggers.size())
		{
			Triggers	events(myTriggers.size());
			size_t		ii = 0;
			for(std::vector<Trigger*>::iterator it=myTriggers.begin(); it != myTriggers.end(); it++, ii++)
			{	events.value[ii]			= uint16((*it)->value);
				events.time[2*ii]			= (*it)->startTime;
				events.time[2*ii+1]			= (*it)->endTime;
				events.fseek[ii]			= (*it)->fseek;
				events.value[ii]			= (*it)->value;
			}
		
			matvar_t* pStruct = events.GetComponents("Triggers", false);
			eventComponents.push_back(pStruct);
		}

		if(mySortedEvents.size())
		{
			mySortedEvents.insert(mySortedEvents.end(), myTriggers.begin(),	myTriggers.end());
			::qsort(&mySortedEvents[0], mySortedEvents.size(), sizeof(void*), compare);
			
			Triggers	events(mySortedEvents.size());
			size_t		ii = 0;
			for(std::vector<Event*>::iterator it=mySortedEvents.begin(); it != mySortedEvents.end(); it++, ii++)
			{	events.value[ii]			= uint16((*it)->value);
				events.time[2*ii]			= (*it)->startTime;
				events.time[2*ii+1]			= (*it)->endTime;
				events.fseek[ii]			= (*it)->fseek;
			}
		
			matvar_t* pStruct = events.GetComponents("AllEvents", false);
			eventComponents.push_back(pStruct);
		}

		matvar_t* pStructEvents = eventComponents.GetStructure("Events");
		allComponents.push_back(pStructEvents);
	}
		
	matvar_t* pStructEyelink = allComponents.GetStructure("EyetrackerAcq");
	
//DIG	Mat_VarWrite(matFile, pStructEyelink, gazeEegEnv.doCompression ? COMPRESSION_ZLIB : 0);
	Mat_VarWrite(matFile, pStructEyelink, 0);
	Mat_VarFree(pStructEyelink);
	
	Mat_Close(matFile);
	
	return true;
}

bool AscRawData::GetEventComponentsForSynchro(MatComponents& eventComponents)
{
	MarkerList_t&		currentMarkers	= mySynchronization.withDrift ? mySynchronization.currentMarkersS : mySynchronization.currentMarkersN;

	// event section
	AcqPhases			acqPhases;
	Records				records;
	Sequences			sequences;
	Displayables		displayables;
	Frames				frames;
	Transitions			transitions;
	FixationSaccades	fixSaccLeft;
	FixationSaccades	fixSaccRight;
	Blinks				blinksLeft;
	Blinks				blinksRight;
	Triggers 			segments;
	Triggers 			triggers;
	Triggers 			allEvents;

	for(MarkerList_t::iterator it=currentMarkers.begin(); it != currentMarkers.end(); it++)
	{	if(it->srcEvent == 0)
		{	uint32 fseek = it->fseek;
			if(it->offset == 0)
			{	segments.Add(CMarker::MARKER_SEGMENT_VAL, uint32(it->offset), 1, fseek);
				allEvents.Add(CMarker::MARKER_SEGMENT_VAL, uint32(it->offset), 1, fseek);
			}
			
			triggers.Add(it->value, uint32(it->offset), 1, fseek);
			allEvents.Add(it->value, uint32(it->offset), 1, fseek);
		}
		else
		{	Event* ev = dynamic_cast<Event*>(it->srcEvent);
			uint32 fseek = ev->fseek;
			allEvents.Add(ev->value, uint32(it->offset), uint32(it->duration), fseek);
			
			switch(it->srcEvent->value)
			{	case phaseLearning:
				case phaseTesting:
					{	AcqPhase* evnt = dynamic_cast<AcqPhase*>(it->srcEvent);
						acqPhases.Add(evnt->value, uint32(it->offset), uint32(it->duration), ev->fseek);
					}
					break;
				case recordStep:
					{	Record* evnt = dynamic_cast<Record*>(it->srcEvent);
						records.Add(evnt->value, uint32(it->offset), uint32(it->duration), ev->fseek);
					}
					break;
				case flowSequence:
					{	CSequence* evnt = dynamic_cast<CSequence*>(it->srcEvent);
						sequences.Add(evnt->value, uint32(it->offset), uint32(it->duration), ev->fseek, uint16(evnt->index), evnt->name);
					}
					break;
				case displayableSpecial:
				case displayableImage:
				case displayableText:
				case displayableVideoSound:
				case displayableVideoNoSound:
					{	Displayable* evnt = dynamic_cast<Displayable*>(it->srcEvent);
						displayables.Add(	evnt->value, uint32(it->offset), uint32(it->duration), ev->fseek,
											evnt->nbFrames, evnt->playedFrames, evnt->firstFrame,
											evnt->frameDuration, 
											evnt->leftRect, evnt->topRect, evnt->rightRect, evnt->bottomRect, 
											evnt->name, 
											evnt->relativePath);
					}
					break;
				case displayableFrame:
					{	Frame* evnt = dynamic_cast<Frame*>(it->srcEvent);
						frames.Add(evnt->value, uint32(it->offset), uint32(it->duration), ev->fseek);
					}
					break;
				case transitionTime:
				case transitionMouse:
				case transitionKeyboard:
				case transitionGaze:
				case transitionSSacc:
				case transitionESacc:
				case transitionSFix:
				case transitionEFix:
				case transitionSpecial:
					{	CTransition* evnt = dynamic_cast<CTransition*>(it->srcEvent);
						transitions.Add(	evnt->value, uint32(it->offset), uint32(it->duration), ev->fseek,
											evnt->name, 
											evnt->typeAsked, evnt->timeout, evnt->nbEvents, evnt->stopAttribute, 
											evnt->posX, evnt->posY);
					}
					break;
				case flowFixationLeft:
				case flowSaccadeLeft:
					{	FixationSaccade* evnt = dynamic_cast<FixationSaccade*>(it->srcEvent);
						/*fixSaccLeft.Add(	evnt->value, uint32(it->offset), uint32(it->duration), ev->fseek,
											evnt->startX, evnt->endX, 
											evnt->startY, evnt->endY);*/
						fixSaccLeft.Add( evnt->value, uint32(it->offset), uint32(it->duration), ev->fseek,
							             evnt->startX, evnt->endX,
							             evnt->startY, evnt->endY,
							             evnt->averagePupilSizeFixation, evnt->amplitudeSaccade, evnt->peakVelocitySaccade
						               );
					}
					break;
				case flowFixationRight:
				case flowSaccadeRight:
					{	FixationSaccade* evnt = dynamic_cast<FixationSaccade*>(it->srcEvent);
						/*fixSaccRight.Add(	evnt->value, uint32(it->offset), uint32(it->duration), ev->fseek,
											evnt->startX, evnt->endX, 
											evnt->startY, evnt->endY);*/
					   fixSaccRight.Add( evnt->value, uint32(it->offset), uint32(it->duration), ev->fseek,
										 evnt->startX, evnt->endX,
										 evnt->startY, evnt->endY,
					                     evnt->averagePupilSizeFixation, evnt->amplitudeSaccade, evnt->peakVelocitySaccade
					                   );
					
					}
					break;
				case flowBlinkLeft:
					{	Blink* evnt = dynamic_cast<Blink*>(it->srcEvent);
						blinksLeft.Add(	evnt->value, uint32(it->offset), uint32(it->duration), ev->fseek);
					}
					break;
				case flowBlinkRight:
					{	Blink* evnt = dynamic_cast<Blink*>(it->srcEvent);
						blinksRight.Add(	evnt->value, uint32(it->offset), uint32(it->duration), ev->fseek);
					}
					break;
	}	}	}

	if(acqPhases.IsValid())
	{	matvar_t* pStruct = acqPhases.GetComponents("AcqPhases", false);
		eventComponents.push_back(pStruct);
	}
	if(records.IsValid())
	{	matvar_t* pStruct = records.GetComponents("RecordSteps", false);
		eventComponents.push_back(pStruct);
	}
	if(sequences.IsValid())
	{	Parser::Transpose(sequences.name, NameSize);

		matvar_t* pStruct = sequences.GetComponents("Sequences", false);
		eventComponents.push_back(pStruct);
	}
	if(displayables.IsValid())
	{	Parser::Transpose(displayables.name,			NameSize);
		Parser::Transpose(displayables.relativePath,	PathSize);

		matvar_t* pStruct = displayables.GetComponents("Displayables", false);
		eventComponents.push_back(pStruct);
	}
	if(frames.IsValid())
	{	matvar_t* pStruct = frames.GetComponents("Frames", false);
		eventComponents.push_back(pStruct);
	}
	if(transitions.IsValid())
	{	Parser::Transpose(transitions.name, NameSize);
		
		matvar_t* pStruct = transitions.GetComponents("Transitions", false);
		eventComponents.push_back(pStruct);
	}
	if(fixSaccLeft.IsValid())
	{	matvar_t* pStruct = fixSaccLeft.GetComponents("FixationSaccadesLeft", false);
		eventComponents.push_back(pStruct);
	}
	if(fixSaccRight.IsValid())
	{	matvar_t* pStruct = fixSaccRight.GetComponents("FixationSaccadesRight", false);
		eventComponents.push_back(pStruct);
	}
	if(blinksLeft.IsValid())
	{	matvar_t* pStruct = blinksLeft.GetComponents("BlinksLeft", false);
		eventComponents.push_back(pStruct);
	}
	if(blinksRight.IsValid())
	{	matvar_t* pStruct = blinksRight.GetComponents("BlinksRight", false);
		eventComponents.push_back(pStruct);
	}
	if(segments.IsValid())
	{	matvar_t* pStruct = segments.GetComponents("Segments", false);
		eventComponents.push_back(pStruct);
	}
	if(triggers.IsValid())
	{	matvar_t* pStruct = triggers.GetComponents("Triggers", false);
		eventComponents.push_back(pStruct);
	}
	if(allEvents.IsValid())
	{	matvar_t* pStruct = allEvents.GetComponents("AllEvents", false);
		eventComponents.push_back(pStruct);
	}

	return true;
}

void AscRawData::GetEventTypeComponents(MatComponents& eventComponents, const GazeEegEnv& gazeEegEnv)
{
	MatComponents typeComponents;

	StimulationMap	currentStim;

	// triggers
	for(std::vector<Trigger*>::const_iterator it=myTriggers.begin(); it < myTriggers.end(); it++)
		currentStim.Add((**it).value, myTriggerDefs[(**it).value]);
	
	// internal events
	for(std::vector<Event*>::const_iterator it=mySortedEvents.begin(); it < mySortedEvents.end(); it++)
		currentStim.Add((*it)->value, theEventTypeMap[(**it).value]);

	currentStim.Write(gazeEegEnv.eventFileName);

	currentStim.AddComponents(typeComponents);
		
	matvar_t* pStruct = typeComponents.GetStructure("EventTypes");
	eventComponents.push_back(pStruct);
}

void AscRawData::GetChannelNamesComponents(MatComponents& matComponents) const
{
	std::vector<char>	channelNames(myChannelNames.size()*ChannelNameSize, 0);

	for(size_t ii=0; ii < myChannelNames.size(); ii++)
		memcpy(&channelNames[ii*ChannelNameSize], myChannelNames[ii].c_str(), myChannelNames[ii].length());
	
	Parser::Transpose(channelNames, ChannelNameSize);

	matComponents.AddVariable("channelNames",	ChannelNameSize,	MAT_C_CHAR,  channelNames,	false);
}

matvar_t* AscRawData::GetParamsComponents(const uint16 samplingRate, const uint32 nbChannels, const uint32 nbSamples, const bool forSynchro /*= false*/) const
{
	MatComponents matComponents;

	matComponents.AddVariable("binaryFormat",		MAT_C_UINT16,		Parser::double_type);
	matComponents.AddVariable("dataOrientation",	MAT_C_UINT16,		0);
	matComponents.AddVariable("dataType",			MAT_C_UINT16,		0);
	matComponents.AddVariable("segmentationType",	MAT_C_UINT16,		0);
	matComponents.AddVariable("segmentDataPoints",	MAT_C_UINT32,		0);
	
	matComponents.AddVariable("samplingRate",		MAT_C_DOUBLE,		double(samplingRate));
	matComponents.AddVariable("nbOfChannels",		MAT_C_UINT32,		nbChannels);
	matComponents.AddVariable("nbOfSamples",		MAT_C_UINT32,		nbSamples);

	matComponents.AddVariable("valNaN",				MAT_C_DOUBLE,		myValNaN);

	matComponents.AddVariable("startTimestamp",		MAT_C_UINT32,		myStartTimestamp);
	matComponents.AddVariable("endTimestamp",		MAT_C_UINT32,		myEndTimestamp);

	if(!forSynchro)
		GetChannelNamesComponents(matComponents);

	return matComponents.GetStructure("Params");
}

matvar_t* AscRawData::GetDataParamsComponentsForSynchro() const
{
	return GetParamsComponents(1000/myHeader.msecPerSample, myNbChannels, myNbSamples, true);			
}

matvar_t* AscRawData::GetDataComponentsForSynchro() const
{
	MatComponents dataComponents;

	dataComponents.AddVariable("fseek",			MAT_C_UINT32,	myFileSeeks);

	return dataComponents.GetStructure("Data");
}

void AscRawData::Exit(const std::string& file, const uint32 line, const uint32 pos, const std::string& text /*= "Corrupted ASC file "*/)
{	
	if (!myLogFile.is_open()) std::cout << "Warning: " << myLogFileName << " is not open." << std::endl;

	myLogFile << file << "::" << line << " " << text << myAscFileName	<< " at position " << (myStartTimestamp + pos)	<< std::endl;

	std::cout << text << myAscFileName	<< " at position " << (myStartTimestamp + pos)	<< std::endl;
	std::cout << "WARNING > Useful information in "		<< myLogFileName								<< std::endl;
	
	system("pause");
	exit(-1);
}

void AscRawData::Warning(const uint32 pos, const std::string& text)
{	
	if (!myLogFile.is_open()) std::cout << "Warning: " << myLogFileName << " is not open." << std::endl;

	if (pos!=-3)
		myLogFile << text << " ( at position " << (myStartTimestamp + pos) << " )" << std::endl;
	else myLogFile << text << std::endl;
	
	myInfoInLogFile = true;
}

bool AscRawData::Warning(bool& status, const std::string& header, const bool refStatus, const uint32 pos /*= 0*/)
{
	bool	ret = true;

	if(status != refStatus)
	{	std::string mess = status ? "BEGIN" : "END";
	    std::string mess_inv = !status ? "BEGIN" : "END";
		
		//TODO: to be removed
		//Warning(pos, header + " = " + (status ? "true" : "false") + ". It means a detection of an event " + mess + " after a previous event " + mess);
		//Warning(pos, header + " = " + (status ? "true" : "false") + ". It means a new " + mess + " has been detected before the expected " + mess_inv);
		
		std::string text = header + " = " + (status ? "true" : "false") + ". It means a new " + mess + " ( at position " + std::to_string(myStartTimestamp + pos) + " )" + " has been detected before the expected " + mess_inv;
		Warning(-3, text);
		
		ret = false;
	}

	if(pos != 0)
		status	= !refStatus;

	return ret;
}

bool AscRawData::SaveStimulations(const std::string& destFile, const MarkerList_t& markers)
{
	StimulationMap	currentStim;
	
	currentStim.Add(CMarker::MARKER_SEGMENT_VAL, "segment256");

	for(MarkerList_t::const_iterator it=markers.begin(); it != markers.end(); it++)
	{	const	CMarker& marker = *it;

		int			value	= marker.value;
		std::string	name;
		
		
		if(marker.value < displayableSpecial)
			name	= myTriggerDefs[marker.value];
		else
			name	= theEventTypeMap[marker.value];
		
		currentStim.Add(value, name);
	}

	return currentStim.Write(destFile);
}

bool AscRawData::SaveMarkers(markerMap_t& markerMap, const std::string& destFile)
{
	GazeEeg::File::Remove(destFile);
	
	std::ofstream ofs(destFile.c_str());
	if(ofs.bad())
		return MSendError1("Opening log file : ", destFile);

	for(markerMapIt_t it=markerMap.begin(); it != markerMap.end(); it++)
	{	ofs.width(10);
		ofs.fill(' ');	
		ofs << it->first << ' ' << it->second << std::endl;
	}

	return true;
}

bool AscRawData::LoadMarkers(markerMap_t& markerMap, const std::string& srcFile)
{
	if(!GazeEeg::File::Exists(srcFile))
		return false;

	std::ifstream ifs(srcFile.c_str());
	if(ifs.bad())
		return MSendError1("Opening log file : ", srcFile);

	while(ifs.good())
	{	int			trigger;
		std::string	alias;
		ifs >> trigger >> alias;
		if(ifs.bad() || alias.empty())
			continue;

		markerMap[trigger]	= alias;
	}

	return markerMap.size() != 0;
}
