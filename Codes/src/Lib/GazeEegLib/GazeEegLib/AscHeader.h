/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

/*!	\file	AscHeader.h
 	\author	Gelu Ionescu.
 
 	\brief	Support for \a Eyelink header acquisition
*/

#pragma once

class MatComponents;

/*!	\class	AscHeader
 
 	\brief	\a Eyelink header acquisition structure
 */
 
class AscHeader
{
public:
	//! public constructor & destructor
	AscHeader();
	~AscHeader();

	bool			IsFromSoftEye() {	return softwareName.find("SoftEye") == 0;	}

	//! build the acquisition header for the \a Eyelink device
    /*! \param parseContent container of \a ASC file lines
		
		\param firstPos returns the position of the first acquisition line found in the \a ASC file
		\param startTimestamp returns the timestamp of the first acquisition sample found in the \a ASC file
		\param endTimestamp returns the timestamp of the last acquisition sample found in the \a ASC file
	 */
	bool			Process(size_t& firstPos, uint32& startTimestamp, uint32& endTimestamp, const std::vector<char*> parseContent, const size_t falseBlinkDuration);
	matvar_t*		GetComponents() const;

public:
	uint16			msecPerSample;		//!< \a Eyelink sampling rate; see \a SoftEye configuration dialog box
	std::string		fileName;			//!< file name of the  \a ASC source file
	std::string		softwareName;		//!< software name; should be \a SoftEye for ASC file controlled by \a SoftEye
	std::string		softwareVersion;	//!< software version; should be \a software \a version for ASC file controlled by SoftEye
	std::string		subjectName;		//!< subject name; see \a SoftEye software front GUI
	uint16			subjectSession;		//!< session of the same subject; see \a SoftEye software front GUI
	std::string		experimentName;		//!< experiment name; see \a SoftEye software front GUI
	uint16			screenWidth;		//!< screen width in pixels
	uint16			screenHeight;		//!< screen height in pixels
	uint16			screenDimXmm;		//!< screen width in millimeters
	uint16			screenDimYmm;		//!< screen height in millimeters
	uint16			screenDistance;		//!< distance eyes to screen in millimeters
	uint16			frameRate;			//!< frame rate
	uint16			falseBlinkSamples;	//!< duration in samples of the false blink (0 means no detection)
	std::string		guidingEye;			//!< guiding eye : can be "LEFT" or "RIGHT"; see \a SoftEye software front GUI
};
