/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

#include "StdAfx.h"

#include "Marker.h"

CMarker::CMarker(const std::string& text, const uint16 value, const uint32 offset, const uint32 duration, const uint32 fseek)
	: type(MARKER_STIMULUS)
	, isExtra(false)
	, text(text)
	
	// for stimulations
	, value(value)
	, offset(offset)
	, duration(duration)
	, fseek(fseek)
	, samplesInsertedBefore(0)
	, correctionInsertedBefore(0)
	
	// for segments
	, startSampleD(0.0)
	
	// segment synchro info
	, start(0)
	, stop(0)
	, srcEvent(0)
{
}

CMarker::CMarker(const std::string& text, const uint32 offset, const std::string& ts)
	: type(MARKER_SEGMENT)
	, isExtra(true)
	, text(text)
	
	// for stimulations
	, value(MARKER_SEGMENT_VAL)
	, offset(offset)
	, duration(0)
	, fseek(0)
	, samplesInsertedBefore(0)
	, correctionInsertedBefore(0)
	
	// for segments
	, startTimestamp(ts)
	, startSampleD(0.0)
	
	// segment synchro info
	, start(0)
	, stop(0)
	, srcEvent(0)
{
}

CMarker::CMarker(const std::string& text, const uint32 offset)
	: type(MARKER_OTHER)
	, isExtra(true)
	, text(text)
	, value(MARKER_OTHER_VAL)
	, offset(offset)
	, srcEvent(0)
	, fseek(0)
	, samplesInsertedBefore(0)
	, correctionInsertedBefore(0)
{
}

CMarker::CMarker(const bool extra, const uint16 value, const std::string& evStimName, const std::string& text, const uint32 offset)
	: type(MARKER_OTHER)
	, isExtra(extra)
	, text(text)
	, evStimName(evStimName)
	, value(value)
	, offset(offset)
	, srcEvent(0)
	, fseek(0)
	, samplesInsertedBefore(0)
	, correctionInsertedBefore(0)
{
}

uint32 CMarker::ProcessSegmentTimestamp(const double samplingRate)
{
	std::string	str;

	double				hour, min, sec, usec, oUsec;
	std::istringstream	issHour(std::string(startTimestamp.c_str() + 8, 2));
	issHour >> hour;
	std::istringstream	issMin(std::string(startTimestamp.c_str() + 10, 2));
	issMin >> min;
	std::istringstream	issSec(std::string(startTimestamp.c_str() + 12, 2));
	issSec >> sec;
	std::istringstream	issuSec(std::string(startTimestamp.c_str() + 14));
	issuSec >> usec;

	oUsec				 = 1000000.0*(sec + 60*(min + 60*hour));
	usec				+= oUsec;
	startSampleD		 = (samplingRate*usec)/1000000.0;
	
	return uint32(::round(startSampleD));
}
