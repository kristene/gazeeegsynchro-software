/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

#include "StdAfx.h"

#include "Displayables.h"
#include "Event.h"

Displayables::Displayables(const size_t size /*= 0*/)
	: Events(size)
	
	// displayable section
	, frameDuration(size, 0)
	, frameNb(size, 0)
	, framePlayed(size, 0)
	, frameFirst(size, 0)
	, leftRect(size, 0)
	, topRect(size, 0)
	, rightRect(size, 0)
	, bottomRect(size, 0)
	, name(NameSize*size, 0)
	, relativePath(PathSize*size, 0)
{
}

Displayables::~Displayables()
{
}

void Displayables::Add(const uint16 value, const uint32 time, const uint32 duration, const uint32 fseek, 
				const uint16 nbFrames, const uint16 playedFrames, const uint16 firstFrame,
				const uint16 frameDuration,
				const uint16 leftRect, const uint16 topRect, const uint16 rightRect, const uint16 bottomRect,
				const std::string& name,
				const std::string& relativePath)
{
	Events::Add(value, time, duration, fseek);
	
	this->frameDuration.push_back(frameDuration);
	this->frameNb.push_back(nbFrames);
	this->framePlayed.push_back(playedFrames);
	this->frameFirst.push_back(firstFrame);
	
	this->leftRect.push_back(leftRect);
	this->topRect.push_back(topRect);
	this->rightRect.push_back(rightRect);
	this->bottomRect.push_back(bottomRect);
	
	std::vector<char> vName(NameSize);
	memcpy(&vName[0], name.c_str(), name.length());
	this->name.insert(this->name.end(), vName.begin(), vName.end());

	std::vector<char> vPath(PathSize);
	memcpy(&vPath[0], relativePath.c_str(), relativePath.length());
	this->relativePath.insert(this->relativePath.end(), vPath.begin(), vPath.end());
}

matvar_t* Displayables::GetComponents(char* structName, const bool conserve)
{
	MatComponents matComponents;
	Events::GetComponents(matComponents, conserve);

	// displayable section
	matComponents.AddVariable("frameDuration",							MAT_C_UINT16,	frameDuration,	conserve);
	matComponents.AddVariable("frameNb",		frameNb.size(),			MAT_C_UINT16,	frameNb,		conserve);
	matComponents.AddVariable("framePlayed",	framePlayed.size(),		MAT_C_UINT16,	framePlayed,	conserve);
	matComponents.AddVariable("frameFirst",		frameFirst.size(),		MAT_C_UINT16,	frameFirst,		conserve);
	matComponents.AddVariable("leftRect",								MAT_C_UINT16,	leftRect,		conserve);
	matComponents.AddVariable("topRect",								MAT_C_UINT16,	topRect,		conserve);
	matComponents.AddVariable("rightRect",								MAT_C_UINT16,	rightRect,		conserve);
	matComponents.AddVariable("bottomRect",								MAT_C_UINT16,	bottomRect,		conserve);
	matComponents.AddVariable("name",			NameSize,				MAT_C_CHAR,		name,			conserve);
	matComponents.AddVariable("relativePath",	PathSize,				MAT_C_CHAR,		relativePath,	conserve);

	return matComponents.GetStructure(structName);
}
