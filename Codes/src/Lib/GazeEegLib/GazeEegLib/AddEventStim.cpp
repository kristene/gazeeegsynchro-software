/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

// AddEventStim.cpp�: d�finit le point d'entr�e pour l'application console.
//

#include "stdafx.h"

#include "AscRawData.h"
#include "Brainamp.h"
#include "Parser.h"
#include "File.h"
#include "CAddEventStim.h"

typedef enum
{	_ERROR_ARGUMENTS,
	_ERROR_FILE,
} error_t;

extern int	MatBuilder(int argc, char* argv[]);

static void Help(int argc, char* argv[], const std::string& text)
{
	std::cout		<< "Software version : " << SOFTWARE_VERSION << std::endl << std::endl;
	
	std::cout		<< text;
	for(int ii=0; ii < argc; ii++)
		std::cout	<< ' ' << argv[ii];
	std::cout	<< std::endl << std::endl;
}

static void Help(int argc, char* argv[], const error_t reason, const std::string& error, const std::string& text = "")
{
	Help(argc, argv, "AddEventStim");

	switch(reason)
	{	case _ERROR_ARGUMENTS:
			std::cout << "Invalid argument list";
			break;
		case _ERROR_FILE:
			std::cout << error << " : " << text	<< std::endl;
			break;
	}

	std::cout  << std::endl << std::endl << std::endl;

	std::cout << "Insert EVENT_STIM events in VMRK file"					<< std::endl << std::endl;
	std::cout << "Calling convention : AddEventStim AddEventStimFileName"	<< std::endl << std::endl;
	std::cout << "AddEventStimFileName    - a valid text file name containing one ore multiple process environments"	<< std::endl;
	std::cout << std::endl << std::endl;

	system("pause");

	exit(-1);
}

void ProcessingDetails(int argc, char* argv[], const GazeEegEnv& gazeEegEnv)
{
	Help(argc, argv, "AddEventStim");

	std::cout	<< "Processing " << std::endl
				<< "  AddEventStimFileName    : " << gazeEegEnv.aes.srcFile	<< std::endl;
		
	std::cout << std::endl;
}

int AddEventStim(int argc, char* argv[])
{	
	if(argc == 0)
		Help(argc, argv, _ERROR_ARGUMENTS, "");
	
	GazeEegEnv	gazeEegEnv;
		
	// check the processing conditions	
	for(int ii=0; ii < argc; ii++)
	{	if(argv[ii][0] != '-')
			gazeEegEnv.aes.srcFile			= argv[ii];
		else if(strcmp(argv[ii], "-P") == 0)
			gazeEegEnv.doPause				= true;
		else if(strcmp(argv[ii], "-M") == 0)
			gazeEegEnv.doMatbuilder			= false;
	}

	if(!GazeEeg::File::Exists(gazeEegEnv.aes.srcFile))
		Help(argc, argv, _ERROR_FILE, "Source file does not exist", gazeEegEnv.aes.srcFile);

	ProcessingDetails(argc, argv, gazeEegEnv);
	
	AddEventStimList	addEventStimList(gazeEegEnv.aes.srcFile);

	for(AddEventStimList::iterator it=addEventStimList.begin(); it != addEventStimList.end(); it++)
	{	if(!it->IsValid())
			continue;

		gazeEegEnv.aes.doIt				= true;
		gazeEegEnv.aes.it				= it;
		gazeEegEnv.eegFileName			= it->Get("eegRejectFileFullPath");
		gazeEegEnv.eventStimFileName	= it->Get("eventStimFileFullPath");

		Brainamp	myBrainamp;
		if(!myBrainamp.IsValid(gazeEegEnv, gazeEegEnv.eegFileName))
		{	::Error("Invalid BRAINAMP file", gazeEegEnv.eegFileName);
			continue;
		}
		
		gazeEegEnv.aes.dictFile = Parser::ReplaceExtention(it->Get("synchroFileFullPath"), std::string("eeg") + MEventFile);
		if(!GazeEeg::File::Exists(gazeEegEnv.aes.dictFile))
		{	::Error("Invalid EVENT file", gazeEegEnv.aes.dictFile);
			continue;
		}

		// build result file name
		Parser::binaryFormat_t	binaryFormat = myBrainamp.GetBinaryFormat(gazeEegEnv.eegFileName);
	
		bool	bProcess = false;
		switch(binaryFormat)
		{	case Parser::int16_type:
				bProcess = myBrainamp.Process(gazeEegEnv, Parser::int16NaN);
				break;
			case Parser::float_type:
				bProcess = myBrainamp.Process(gazeEegEnv, Parser::floatNaN);
				break;
			case Parser::double_type:
				bProcess = myBrainamp.Process(gazeEegEnv, Parser::doubleNaN);
				break;
		}

		if(!bProcess)
		{	::Error("Processing BRAINAMP file", gazeEegEnv.eegFileName);
			continue;
		}

		BrainampVmrk& vmrk = myBrainamp.Vmrk();
		if(!vmrk.ProcessAddEventStim(gazeEegEnv, myBrainamp.Vhdr()))
		{	::Error("Inserting EVENT_STIM in file");
			continue;
		}

		std::string		pathE, nameE, extE;
		Parser::ParsePath(pathE, nameE, extE, gazeEegEnv.eventStimFileName);

		std::string		pathS, nameS, extS;
		Parser::ParsePath(pathS, nameS, extS, gazeEegEnv.eegFileName);

		std::string		resultFile = it->Get("outputFullPath") + '\\' + nameE + '-' + nameS + '.' + extS;

		if(!myBrainamp.SaveEegLab(gazeEegEnv, resultFile))
		{
			std::cerr << "!!! Error saving data as  EegLab format -> " << resultFile << std::endl;
			
			system("pause");

			exit(-3);
		}

		if(gazeEegEnv.doMatbuilder)
		{	char* pChar = (char*) resultFile.c_str();
			::MatBuilder(1, &pChar);
	}	}

	std::cout << "!!! AddEventStim END !!! " << std::endl;

	if(gazeEegEnv.doPause)
		system("pause");

	return 0;
}
