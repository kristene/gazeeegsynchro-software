/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

/*!	\file	MatComponents.h
 	\author	Gelu Ionescu.
 
 	\brief	\a MatComponents class support
*/

#pragma once

/*!	\class	MatComponents
 
 	\brief	Converts a user variable towards a Matlab structure
 */
 
class MatComponents : public std::vector<matvar_t*>
{
private:
	bool	myFreeComponents;	//!< clean-up the content when the destructor is called

public:
	//! public constructor
    /*! \param freeComponents determine the class behaviour when the destructor is called
	 */
	MatComponents(const bool freeComponents = true);
	~MatComponents();
	
	//! call when a class clean-up is necessary
	void	Clean();

	//! build a string variable
    /*! \param fieldName name of the Matlab structure
		\param data content of the Matlab structure
	 */
	void	AddVariable(const std::string& fieldName, const std::string& data)
	{	
		int dims[]	= {1, data.length()};

		push_back(Mat_VarCreate(fieldName.c_str(), MAT_C_CHAR, MAT_T_UINT8, 2,
                       dims, (void*) data.c_str(), MEM_CONSERVE));
	}

	//! build a template variable
    /*! \param fieldName name of the Matlab structure
		\param classType type of the class
		\see \a matio library for accepted types
		\param data content of the Matlab structure
	 */
	template <class T> bool	AddVariable(const std::string& fieldName, const int classType, const T data)
	{	
		int dims[]	= {1, 1};

		int	dataType;
		switch(classType)
		{	case MAT_C_UINT8:
				dataType	= MAT_T_UINT8;
				break;
			case MAT_C_INT8:
				dataType	= MAT_T_INT8;
				break;
			case MAT_C_CHAR:
				dataType	= MAT_T_INT8;
				break;
			case MAT_C_UINT16:
				dataType	= MAT_T_UINT16;
				break;
			case MAT_C_INT16:
				dataType	= MAT_T_INT16;
				break;
			case MAT_C_UINT32:
				dataType	= MAT_T_UINT32;
				break;
			case MAT_C_INT32:
				dataType	= MAT_T_INT32;
				break;
			case MAT_C_DOUBLE:
				dataType	= MAT_T_DOUBLE;
				break;
			case MAT_C_SINGLE:
				dataType	= MAT_T_SINGLE;
				break;
			default:
				return MSendError("MatComponents::AddVariable : Unknown MAT_T type");
		}

		push_back(Mat_VarCreate(fieldName.c_str(), classType, dataType, 2,
                       dims, (void*) &data, 0));

		return true;
	}
	
	//! build a template vector
    /*! \param fieldName name of the Matlab structure
		\param classType type of the class
		\see \a matio library for accepted types
		\param data content of the Matlab vector
		\param conserve
			- false	: compress the data
			- true	: do not compress the data
	 */
	template <class T> bool	AddVariable(const std::string& fieldName, const int classType, const std::vector<T>& data, const bool conserve = true)
	{
		return AddVariable(fieldName, data.size(), classType, data, conserve);
	}
	
	//! build a template vector
    /*! \param fieldName name of the Matlab structure
		\param dimLine line dimension
		\param classType type of the class
		\see \a matio library for accepted types
		\param data content of the Matlab vector
		\param conserve
			- false	: compress the data
			- true	: do not compress the data
	 */
	template <class T> bool	AddVariable(const std::string& fieldName, const size_t dimLine, const int classType, const std::vector<T>& data, const bool conserve = true)
	{	
		int dims[]	= {data.size()/dimLine, dimLine };

		int	dataType;
		switch(classType)
		{	case MAT_C_UINT8:
				dataType	= MAT_T_UINT8;
				break;
			case MAT_C_INT8:
				dataType	= MAT_T_INT8;
				break;
			case MAT_C_CHAR:
				dataType	= MAT_T_INT8;
				break;
			case MAT_C_UINT16:
				dataType	= MAT_T_UINT16;
				break;
			case MAT_C_INT16:
				dataType	= MAT_T_INT16;
				break;
			case MAT_C_UINT32:
				dataType	= MAT_T_UINT32;
				break;
			case MAT_C_INT32:
				dataType	= MAT_T_INT32;
				break;
			case MAT_C_DOUBLE:
				dataType	= MAT_T_DOUBLE;
				break;
			case MAT_C_SINGLE:
				dataType	= MAT_T_SINGLE;
				break;
			default:
				return MSendError("MatComponents::AddVariable : Unknown MAT_T type");
		}

		push_back(Mat_VarCreate(fieldName.c_str(), classType, dataType, 2, dims, (void*) &data[0], conserve ? MEM_CONSERVE : 0));

		return true;
	}
	
	//! get the matlab structure
    /*! \param structName name of the Matlab structure

		\return a pointer to the matlab structure
	 */
	matvar_t* GetStructure(const std::string& structName)
	{	
		int dims[]	= {1, 1};

		push_back(0);

		return Mat_VarCreate(structName.c_str(), MAT_C_STRUCT, MAT_T_STRUCT, 2,
                       dims, (void*) &((*this)[0]), 0);
	}
};
