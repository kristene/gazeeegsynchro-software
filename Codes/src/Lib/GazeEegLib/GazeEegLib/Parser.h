/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

#pragma once

#include "File.h"

#include "Types.h"

class Parser
{
public:
	typedef enum
	{	unknown_type	= -1,
		int16_type,
		float_type,
		double_type,
	} binaryFormat_t;
	
	typedef	double	double_t;
	typedef	float	float_t;
	typedef	int16	int16_t;

public:
	Parser(void);
	~Parser(void);

			static void			ParseChar(std::vector<char*>& parsedContent, char* pStr, const size_t szStr, const char delimiter, std::vector<uint32>* pSeek = 0);
			static void			ParsePath(std::vector<std::string>& segments, const std::string& fullPathName);
			static void			ParseSpaces(std::vector<char*>& parsedContent, char* pStr, const size_t szStr);
			static void			Transpose(std::vector<char>& data, const size_t lineSize);
			static bool			IsValidInt(char* pStr); 
			static bool			IsValidFloat(char* pStr); 
			static bool			IsNegative(char* pStr)	{	return *pStr == '-';	}
			static int			GetInt(const std::string& str, const size_t pos, const size_t len);
			static void			Replace(std::string& target, const std::string& what, const std::string& by)
								{
									std::string::size_type	pos			= target.find(what);
									if(pos == std::string::npos)
										return;

									target.replace(pos, what.length(), by);
								}
	inline	static bool			strcmp(const char *string1, const char *string2)	      {	return ::strcmp(string1, string2)  == 0;	}
	inline	static bool			StringsAreEqual(const char *string1, const char *string2) { return strcmp(string1, string2); }
	inline	static bool			stricmp(const char *string1, const char *string2)	      {	return ::_stricmp(string1, string2) == 0;	}
	inline	static int16		GetValueInt16(char* str)
								{	
									return int16(Parser::strcmp(str, ".") ? -1 : atoi(str));
								}
	inline	static uint32		GetValueUint32(char* str)
								{	
									return uint32(Parser::strcmp(str, ".") ? -1 : atoi(str));
								}
	inline	static double		GetValueDouble(char* str)
								{	
									return Parser::strcmp(str, ".") ? -1 : atof(str);
								}

			// file manipulations
			static bool			CreatePath(const std::string& pathName);
			static std::string	ParseFileName(char* pStr);
			static void			ParsePath(std::string& path, std::string& name, std::string& ext, const std::string& src);
			static std::string	FullPath(const std::string& path, const std::string& name, const std::string& ext)
								{	
									return path + '\\' + name + '.' + ext;
								}
			static std::string	GetFileName(const std::string& srcFile)
								{
									std::string	path, name, ext;
									ParsePath(path, name, ext, srcFile);

									return name;
								}
			static std::string	ReplaceExtention(const std::string& srcFile, const std::string& newExt)
								{
									std::string	path, name, ext;
									ParsePath(path, name, ext, srcFile);

									return FullPath(path, name, newExt);
								}
			static bool			GetPair(std::string& first, std::string& second, const std::string& src, const char target)
								{
									std::string::size_type pos = src.find(target);
									if(pos == std::string::npos)
										return false;

									first	=  Trim(src.substr(0, pos));
									second	=  Trim(src.substr(pos+1));

									return true;
								}
	inline	static std::string& Ltrim(std::string &s)
								{
									s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
									return s;
								}
	inline static std::string&	Rtrim(std::string &s)
								{
									s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
									return s;
								}
	inline static std::string&	Trim(std::string &s)
								{
									return Ltrim(Rtrim(s));
								}

	static	double_t	doubleNaN;
	static	float_t		floatNaN;
	static	int16_t		int16NaN;
	static	double_t	gazeNaN;
};
