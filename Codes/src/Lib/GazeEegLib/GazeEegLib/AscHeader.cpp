/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

#include "StdAfx.h"

#include "AscHeader.h"
#include "Parser.h"
#include "MatComponents.h"

AscHeader::AscHeader()
	: msecPerSample(1)
	, subjectSession(0)
	, screenWidth(0)
	, screenHeight(0)
	, screenDimXmm(0)
	, screenDimYmm(0)
	, screenDistance(0)
	, frameRate(0)
	, falseBlinkSamples(0)
{
}

AscHeader::~AscHeader()
{
}

bool AscHeader::Process(size_t& firstPos, uint32& startTimestamp, uint32& endTimestamp, const std::vector<char*> parseContent, const size_t falseBlinkDuration)
{
	firstPos	= 0;

	bool								isDetected = false;
	std::vector<char*>::const_iterator	it;
	for(it=parseContent.begin(); it != parseContent.end(); it++)
	{	std::string			str(*it);
		
		std::vector<char*> subStrings;
		Parser::ParseSpaces(subStrings, (char*) str.c_str(), str.length());

		if(subStrings.size() <= 1)
			continue;

		if(Parser::strcmp(subStrings.front(), "**"))
		{	if(Parser::strcmp(*(subStrings.begin()+1), "CONVERTED"))
			{	for(std::vector<char*>::const_iterator it1=subStrings.begin()+2; it1 != subStrings.end(); it1++)
				{	if(strstr(*it1, "edf"))
					{	fileName	= Parser::ParseFileName(*it1);
						break;
		}	}	}	}
		else if(strstr(*it, "SOFTWARE NAME"))
			softwareName	= subStrings.back();
		else if(strstr(*it, "SOFTWARE VERSION"))
			softwareVersion	= subStrings.back();
		else if(strstr(*it, "SUBJECT NAME"))
			subjectName		= subStrings.back();
		else if(strstr(*it, "SUBJECT SESSION"))
			subjectSession	= uint16(atoi(subStrings.back()));
		else if(strstr(*it, "EXPERIMENT NAME"))
			experimentName	= subStrings.back();
		else if(strstr(*it, "SCREEN WIDTH"))
			screenWidth		= uint16(atoi(subStrings.back()));
		else if(strstr(*it, "SCREEN HEIGHT"))
			screenHeight	= uint16(atoi(subStrings.back()));
		else if(strstr(*it, "SCREEN PHYS DIMX MM"))
			screenDimXmm	= uint16(atoi(subStrings.back()));
		else if(strstr(*it, "SCREEN PHYS DIMY MM"))
			screenDimYmm	= uint16(atoi(subStrings.back()));
		else if(strstr(*it, "SCREEN DISTANCE"))
			screenDistance	= uint16(atoi(subStrings.back()));
		else if(strstr(*it, "GUIDING EYE"))
			guidingEye		= subStrings.back();
		else if(strstr(*it, "FRAMERATE"))
		{	if(subStrings.size() == 5)
				frameRate		= uint16(atoi(*(subStrings.rbegin() + 1)));
			else if(subStrings.size() == 4)	// Nathalie 20170529
				frameRate		= uint16(atoi(*(subStrings.rbegin())));
		}
		else if(strstr(*it, "DISPLAY_COORDS"))	// Nathalie 20170529
		{	screenWidth		= uint16(atoi(*(subStrings.rbegin() + 1)));
			screenHeight	= uint16(atoi(*(subStrings.rbegin())));			
		}
		else if(strstr(*it, "START CALIBRATION INITIAL"))
		{	if(isDetected)
				continue;

			isDetected		= true;
			firstPos		= it - parseContent.begin();
			startTimestamp	= atol(*(subStrings.begin()+1));
		}
		else if(strstr(*it, "BEGIN CALIBRATION INITIAL"))
		{	if(isDetected)
				continue;

			isDetected		= true;
			firstPos		= it - parseContent.begin();
			startTimestamp	= atol(*(subStrings.begin()+1));
		}
		else if((subStrings.size() == 3) && Parser::strcmp(subStrings.front(), "INPUT"))
		{	if(isDetected)
				continue;

			isDetected		= true;
			firstPos		= it - parseContent.begin();
			startTimestamp	= atol(*(subStrings.begin()+1));
		}
		else if(Parser::strcmp(subStrings.front(), "MSG") && Parser::strcmp(subStrings.back(), "points:") && Parser::strcmp(*(subStrings.begin()+2), "!CAL") && Parser::strcmp(*(subStrings.begin()+3), "Calibration"))
		{	if(isDetected)
				continue;

			isDetected		= true;
			firstPos		= it - parseContent.begin();
			startTimestamp	= atol(*(subStrings.begin()+1));
		}
		else if(Parser::strcmp(subStrings.front(), "START") && Parser::strcmp(subStrings.back(), "EVENTS") && Parser::strcmp(*(subStrings.rbegin()+1), "SAMPLES"))
		{	if(isDetected)
				continue;

			isDetected		= true;
			firstPos		= it - parseContent.begin();
			startTimestamp	= atol(*(subStrings.begin()+1));
		}
		else if(Parser::strcmp(subStrings.front(), "SAMPLES") && Parser::strcmp(*(subStrings.begin()+1), "GAZE"))
		{	bool found = false;

			for(std::vector<char*>::iterator it1=subStrings.begin()+2; it1 != subStrings.end(); it1++)
			{	if(Parser::strcmp(*it1, "RATE"))
					found = true;
				else if(found)
				{	msecPerSample		= uint16(1000/(atof(*it1)));
					falseBlinkSamples	= falseBlinkDuration/msecPerSample;
					break;
			}	}
			if(!isDetected)
				continue;

			break;
	}	}
	
	bool	ret = false;
	for(std::vector<char*>::const_reverse_iterator it=parseContent.rbegin(); it != parseContent.rend(); it++)
	{	std::string			str(*it);
		
		std::vector<char*> subStrings;
		Parser::ParseSpaces(subStrings, (char*) str.c_str(), str.length());

		if(subStrings.size() < 1)
			continue;

		if(strstr(subStrings.front(), "MSG"))
		{	endTimestamp	= atol(*(subStrings.begin()+1));
			ret = true;
			break;
		}
		else if(strstr(subStrings.front(), "CMD") && strstr(*(subStrings.begin() + 2), "close_data_file"))
		{	endTimestamp	= atol(*(subStrings.begin()+1));
			ret = true;
			break;
		}
		else if(strstr(subStrings.front(), "INPUT"))
		{	endTimestamp	= atol(*(subStrings.begin()+1));
			ret = true;
			break;
		}
		else if(strstr(subStrings.front(), "END") && strstr(*(subStrings.begin() + 2), "SAMPLES") && strstr(*(subStrings.begin() + 3), "EVENTS"))
		{	endTimestamp	= atol(*(subStrings.begin()+1));
			ret = true;
			break;
		}
		else if(Parser::IsValidInt(subStrings.front()))
		{	endTimestamp	= atol(subStrings.front());
			ret = true;
			break;
	}	}

	return ret;
}

matvar_t* AscHeader::GetComponents() const
{
	MatComponents matComponents;
	matComponents.AddVariable("softwareName",		softwareName);
	matComponents.AddVariable("softwareVersion",	softwareVersion);
	matComponents.AddVariable("fileName",			fileName);
	matComponents.AddVariable("subjectName",		subjectName);
	matComponents.AddVariable("subjectSession",		MAT_C_UINT16, subjectSession);
	matComponents.AddVariable("experimentName",		experimentName);
	matComponents.AddVariable("screenWidth",		MAT_C_UINT16, screenWidth);
	matComponents.AddVariable("screenHeight",		MAT_C_UINT16, screenHeight);
	matComponents.AddVariable("screenDimXmm",		MAT_C_UINT16, screenDimXmm);
	matComponents.AddVariable("screenDimYmm",		MAT_C_UINT16, screenDimYmm);
	matComponents.AddVariable("screenDistance",		MAT_C_UINT16, screenDistance);
	matComponents.AddVariable("frameRate",			MAT_C_UINT16, frameRate);
	matComponents.AddVariable("falseBlinkSamples",	MAT_C_UINT16, falseBlinkSamples);
	matComponents.AddVariable("guidingEye",			guidingEye);

	return matComponents.GetStructure("AscHeader");
}
