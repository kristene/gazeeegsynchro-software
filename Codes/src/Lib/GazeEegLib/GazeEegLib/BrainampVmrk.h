/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

/*!	\file	BrainampVmrk.h
 	\author	Gelu Ionescu.
 
 	\brief	Support for a \a Brainamp \a VMRK file
*/
#pragma once

#include "MatComponents.h"
#include "Parser.h"
#include "Marker.h"
#include "CAddEventStim.h"

class BrainampVhdr;
class GazeEegEnv;

/*!	\class	BrainampVmrk
 
 	\brief	Mirror structure for a \a Brainamp \a VMRK file
 */
 
class BrainampVmrk
{
public:
	//! public constructor & destructor
	BrainampVmrk();
	~BrainampVmrk();
	
	//! function that processes the \a Brainamp \a VMRK file
    /*! \param fileName	source file name	
				
		\return  boolean 
	 */
	bool	Process(const bool doAll, const std::string& fileName);
	
	//! function that processes the segments detected
    /*! \param vhdr	the header file structure	
		\param eegDataSize the true \a EEG data size	
				
		\return  boolean 
	 */
	bool	ProcessDataSegments(BrainampVhdr& vhdr, const size_t eegDataSize);
	
	//! fonction that saves synchronized .VMRK with \a Eyelink
	/*! \param src source file name
		\param dest destination file name
		\param markers the synchronized marker list 

		\return boolean
	 */
	bool	SaveGazeEeg(const std::string& src, const std::string& dest, const MarkerList_t& markers, const bool forceDuration1);
	bool	SaveEegLab(const std::string& eegSrcFileName, const std::string& eegDestFileName);
	bool	ProcessAddEventStim(GazeEegEnv& gazeEegEnv, const BrainampVhdr& vhdr);
	
	//! returns all the markers
	size_t									NbStimulations();
	size_t									NbSegments();
	MarkerList_t&							Markers()				{	return	myMarkers;				}
	std::vector<MarkerList_t::iterator>&	Segments()				{	return	mySegmentsIt;			}
	size_t									NbExtraStimulations()	{	return myNbExtraStimulations;	}

private:
	void									AddEventStim(AddEventStimList::iterator& aesIt, size_t indMyMarkers, MarkerList_t::iterator itSrc, const MarkerList_t::iterator itSrcLim);

private:
	MarkerList_t							myMarkers;		//!< the marker table
	std::vector<MarkerList_t::iterator>		mySegmentsIt;
	size_t									myNbExtraStimulations;
};
