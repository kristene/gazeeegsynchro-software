/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

/*!	\file	BrainampVhdr.h
 	\author	Gelu Ionescu.
 
 	\brief	Support for a \a Brainamp \a VHDR file
*/
#pragma once
#include "MatComponents.h"

/*!	\class	BrainampVhdr
 
 	\brief	Mirror structure for a \a Brainamp \a VHDR file
 */
 
class AscRawData;
class BrainampVhdr
{
public:
	typedef enum
	{	CP_UNCKNOWN	= -1,
		CP_UTF_8,
	} codePage_t;
	typedef enum
	{	DF_UNCKNOWN	= -1,
		DF_BINARY,
	} dataFormat_t;
	typedef enum
	{	DO_UNCKNOWN	= -1,
		DO_MULTIPLEXED,
		DO_VECTORIZED,
	} dataOrientation_t;
	typedef enum
	{	DT_UNCKNOWN	= -1,
		DT_FREQUENCYDOMAIN,
		DT_TIMEDOMAIN,
	} dataType_t;
	typedef enum
	{	ST_NOSEGMENTED	= 0,
		ST_MARKERBASED,
	} segmentationType_t;

public:
	//! public constructor & destructor
	BrainampVhdr();
	~BrainampVhdr();
	
	//! function that returns the sample format (int16, float or double)
    /*!	\param	fileName source file name containing the information (actualy \a VHDR file)
		
		\return	the sample format
		
		\see	binaryFormat_t for sample formats
	 */
	Parser::binaryFormat_t		GetBinaryFormat(const std::string& fileName);

	//! function that processes the \a Brainamp \a VHDR file
    /*! \param fileName	source file name	
				
		\return  boolean 
	 */
	bool						Process(const std::string& fileName);

	//! function that sets the true number of semples; usefull when multiple acquired segments exists
	void						SetNbSamples(const size_t nbSamples)	{	this->nbSamples = nbSamples;		}

	//! returns the number of acquisition samples
	uint32						GetNbSamples()	const					{	return nbSamples;					}
	
	//! returns the number of acquisition channels
	uint16						GetNbChannels()	const					{	return nbChannels;					}
	
	//! returns the sample dimension in microseconds
	double						UsecPerSample()	const					{	return usecPerSample;				}
	double						SamplingRate() const					{	return 1000000.0 / usecPerSample;	}
	
	//! fonction that converts the acquisition parameters in a \a Matlab structure
	/*! \param valNaN the value for the invalid samples

		//! return acquisition parameters as a \a Matlab format
		/see	"matio" undocumented library
	 */
	matvar_t*					GetParamsComponents(const Parser::double_t valNaN, AscRawData* eyelink = 0);
	
	//! fonction that converts channel names a \a Matlab structure
	/*! \param matComponents destination structure
	 */
	void						GetChannelNamesComponents(MatComponents& matComponents, AscRawData* eyelink = 0) const;
	
	//! fonction that converts channel gains a \a Matlab structure
	/*! \param matComponents destination structure
	 */
	void						GetChannelGainsComponents(MatComponents& matComponents, AscRawData* eyelink = 0);
	
	//! fonction that saves synchronized data with \a Eyelink
	/*! \param src source file name
		\param dest destination file name
		\param extraChannelNames channel names for the \a Eyelink 
		\param samplingRate sampling rate for the synchronized data

		\return boolean
	 */
	bool						SaveGazeEeg(const std::string& src, const std::string& dest, const std::vector<std::string>& extraChannelNames, const double samplingRate, const int nbOfSamples);
	bool						SaveEegLab(const std::string& eegSrcFileName, const std::string& eegDestFileName);

public:
	uint32						nbSamples;		//!< number of samples
	
	//[Common Infos]
	codePage_t					codePage;
	dataFormat_t				dataFormat;
	dataOrientation_t			dataOrientation;
	dataType_t					dataType;
	uint16						nbChannels;		//!< number of channels
	uint32						dataPoints;		//!< number of channels
	double						usecPerSample;	//!< sample dimension in microseconds
	segmentationType_t			segmentationType;
	uint32						segmentDataPoints;		//!< number of channels

	//[User Infos]

	//[Binary Infos]
	Parser::binaryFormat_t		binaryFormat;	//!< type of data samples
	
	//[Channel Infos]
	std::vector<std::string>	nameChannels;	//!< table of channel names
	std::vector<double>			gainChannels;	//!< table of channel gains
};
