/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

#pragma once

#include <string>
#include <iostream>

typedef signed		char	int8;
typedef unsigned	char	uint8;
typedef signed		short	int16;
typedef unsigned	short	uint16;
typedef signed		int		int32;
typedef unsigned	int		uint32;

extern bool Error(const std::string& text, const std::string& text1 = "", const std::string& file = __FILE__, const int line = __LINE__);
extern void Warning(const std::string& text, const std::string& text1 = "", const std::string& file = __FILE__, const int line = __LINE__);

#define MSendError(text)			::Error(text, "", __FILE__, __LINE__)
#define MSendError1(text, text1)	::Error(text, text1, __FILE__, __LINE__)

#define MSendWarning(text)			::Warning(text, "", __FILE__, __LINE__)
#define MSendWarning1(text, text1)	::Warning(text, text1, __FILE__, __LINE__)

#ifdef NDEBUG
#define MSendDebug(text)
#define MSendDebug1(text, text1)
#else
#define MSendDebug(text)			{	std::cout << text << std::endl;	}
#define MSendDebug1(text, text1)	{	std::cout << text << " " << text1 << std::endl;	}
#endif
