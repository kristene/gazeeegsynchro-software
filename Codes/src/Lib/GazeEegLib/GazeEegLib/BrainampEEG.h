/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

/*!	\file	BrainampEegVirt.h
 	\author	Gelu Ionescu.
 
 	\brief	Support for \a Brainamp virtual data type (int16, float or double)
*/
#pragma once

#include "MatComponents.h"
#include "BrainampVmrk.h"
#include "BrainampVhdr.h"

/*!	\class	BrainampEegVirt
 
 	\brief	Pure virtual \a BrainampEegVirt EEG data acquisition structure
 */
 
class BrainampEegVirt
{
public:
	//! public constructor & destructor
	BrainampEegVirt(){}
	virtual ~BrainampEegVirt(){}
	
	//! function that processes the \a Brainamp EEG
    /*! \param fileName	source file name	
				
		\return  boolean 
	 */
	virtual bool				Process(const std::string& fileName)								= 0;
	virtual size_t				GetDataSize()														= 0;
	virtual bool				ProcessDataSegments(BrainampVmrk& vmrk, const BrainampVhdr& vhdr)	= 0;
	virtual bool				IsDataValid(const uint32 nbSamples, const uint16 nbChannels)		= 0;
	virtual bool				SaveRawData()														= 0;
	virtual bool				SaveGazeEeg(const std::string& dest)								= 0;
	virtual bool				SaveEegLab(const std::string& dest)									= 0;
	virtual Parser::double_t	GetNan()															= 0;
	virtual BrainampEegVirt&	EegVirt()															= 0;
};

/*!	\class	BrainampEEG
 
 	\brief	Template \a BrainampEEG EEG data acquisition structure
 */
 
template <class type> class BrainampEEG : public BrainampEegVirt
{
public:
	//! public constructor & destructor
	BrainampEEG(){}
	~BrainampEEG(){}
		
	//! function that processes the \a Brainamp EEG with different types of samples (i.e. int16, float or double)
    /*! \param fileName	source file name	
				
		\return  boolean 
	 */
	bool	Process(const std::string& fileName)
	{
		MSendDebug1("Processing : ", fileName);
		myFileName	= fileName;

		if(!GazeEeg::File::Exists(fileName))
			return MSendError1("BrainampEEG::Process::File::Exists", fileName);

		if(!GazeEeg::File::Read(fileName, data))
			return MSendError1("BrainampEEG::Process::File::Read", fileName);

		return true;
	}
		
	//! function that returns the size of acquired samples
	size_t	GetDataSize()
	{	
		return data.size();
	}

	//! function that processes the acquisition samples
    /*! \brief	The \a Brainamp device can be put in \a pause mode.
				It generates several data segments
		
		\param vhdr	information comming from \a VHDR file	
		\param vmrk	information comming from \a VMRK file	
				
		\return  boolean 
	 */
	bool	ProcessDataSegments(BrainampVmrk& vmrk, const BrainampVhdr& vhdr)
	{
		modifiedData	= false;

		if(vhdr.segmentationType != BrainampVhdr::ST_NOSEGMENTED)
			return true;

		const std::vector<MarkerList_t::iterator>& segments = vmrk.Segments();

		if(segments.size() <= 1)
			return true;
	
		uint32	nbSamples	= vhdr.GetNbSamples();
		uint32	nbChannels	= vhdr.GetNbChannels();
		modifiedData		= false;

		for(std::vector<MarkerList_t::iterator>::const_reverse_iterator it=segments.rbegin(); it != segments.rend(); it++)
		{	if(!(*it)->samplesInsertedBefore)
				continue;
			
			uint32	pos	= (*it)->offset*nbChannels;
			uint32	nb	= (*it)->samplesInsertedBefore*nbChannels;
			
			data.insert(data.begin() + pos, nb, valNaN);

			modifiedData	= true;
		}

		return true;
	}

	//! tests the data valability
    /*! \return  boolean 
	 */
	bool	IsDataValid(const uint32 nbSamples, const uint16 nbChannels)
	{	
		return data.size() == nbSamples*nbChannels;
	}

	//! saves the raw data
    /*! \return  boolean 
	 */
	bool	SaveRawData()
	{
		std::string		rawFileName = Parser::ReplaceExtention(myFileName, "raw");
		bool b = GazeEeg::File::Remove(rawFileName);

		if(!modifiedData)
			return true;

		return GazeEeg::File::Write(rawFileName, data);
	}

	//! saves the synchronized \a EEG and \a Eyelink data
    /*! \return  boolean 
	 */
	bool	SaveGazeEeg(const std::string& dest)
	{
		std::string dstEeg	= dest + ".eeg";
		
		MSendDebug1("Saving : ", dstEeg);

		return GazeEeg::File::Write(dstEeg, data);
	}

	//saves a .EEG file
	bool	SaveEegLab(const std::string& dest)
	{
		MSendDebug1("Saving : ", dest);

		return GazeEeg::File::Write(dest, data);
	}

	//! returns the \a NaN value
	Parser::double_t GetNan()
	{
		return valNaN;
	}

	//! returns the data structure
	BrainampEegVirt&	EegVirt()	{	return *this;	}
	
private:
	std::string			myFileName;		//!< source file name (internal variable)
	bool				modifiedData;	//!< true when several acquisition segments exist (internal variable)

public:
	std::vector<type>	data;			//!< \a EEG acquired data (template)
	type				valNaN;			//!< \a NaN for invalid data samples (template)
};
