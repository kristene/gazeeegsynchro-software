/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

// MatBuilder.cpp�: d�finit le point d'entr�e pour l'application console.
//

#include "stdafx.h"

#include "AscRawData.h"
#include "Brainamp.h"
#include "File.h"

typedef enum
{	_ERROR_ARGUMENTS,
	_ERROR_FILE_EXISTS,
	_ERROR_CONVERSION,
} error_t;

static void Help(int argc, char* argv[], const std::string& text)
{
	std::cout		<< "Software version : " << SOFTWARE_VERSION << std::endl << std::endl;
	
	std::cout		<< text;
	for(int ii=0; ii < argc; ii++)
		std::cout	<< ' ' << argv[ii];
	std::cout	<< std::endl << std::endl;
}

static void Help(int argc, char* argv[], const error_t reason, const std::string& text = "")
{
	Help(argc, argv, "MatBuilder");

	switch(reason)
	{	case _ERROR_ARGUMENTS:
			std::cout << "Invalid argument list";
			break;
		case _ERROR_FILE_EXISTS:
			std::cout << "======================" << std::endl;
			std::cout << "Invalid Source file : " << text << std::endl;
			std::cout << "======================" << std::endl;
			break;
		case _ERROR_CONVERSION:
			std::cout << "Invalid source file for conversion";
			break;
	}

	std::cout  << std::endl << std::endl << std::endl;

	std::cout << "Converts a valid ASC or BRAINAMP file towards MAT format"					<< std::endl << std::endl;
	std::cout << "Calling convention for ASC : MatBuilder fileName [-f valMsec] [-p] [-v] [-h outputPath]"		<< std::endl << std::endl;
	std::cout << "fileName            - A valid ASC or EXT source file name"				<< std::endl;

	std::cout << "[-f valMsec]        - False blink duration (0 = no detection)"			<< std::endl;
	std::cout << "[-t trigFileName]   - File name for the trigger names"					<< std::endl;
	std::cout << "[-p]                - pupil size processing (default : false)"			<< std::endl;
	std::cout << "[-v]                - process video frames as events (default : false)"	<< std::endl;
	std::cout << "[-h outputPath]     - Output path"										<< std::endl;

	std::cout << "Calling convention for EEG : MatBuilder fileName [-h]"					<< std::endl << std::endl << std::endl << std::endl;
	std::cout << "fileName            - A valid EEG | VHDR | VMRK source file name"			<< std::endl;

	system("pause");

	exit(-1);
}

void ExecEnvironment(int argc, char* argv[], const GazeEegEnv& gazeEegEnv)
{
	Help(argc, argv, "MatBuilder");

	std::cout		<< "  File name        : "	<< (gazeEegEnv.isAscFile ? gazeEegEnv.gazeFileName : gazeEegEnv.eegFileName)		<< std::endl
					<< "  MAT file name    : "	<< (gazeEegEnv.isAscFile ? gazeEegEnv.gazeMatFileName : gazeEegEnv.eegMatFileName)	<< std::endl
					<< (!gazeEegEnv.triggerFileName.empty()		?  ("  Triggers File    : " + gazeEegEnv.triggerFileName) : "")			<< std::endl;

	if(gazeEegEnv.falseBlinkDuration)
		std::cout	<< "  False blinks detection     : " << "YES unther "	<< gazeEegEnv.falseBlinkDuration << " msec"				<< std::endl;
	
	if(gazeEegEnv.onPupil)
		std::cout	<< "  Pupil size                 : " << "YES"																	<< std::endl;
}

int MatBuilder(int argc, char* argv[])
{
	if(argc < 1)
		Help(argc, argv, _ERROR_ARGUMENTS);

	GazeEegEnv			gazeEegEnv;
	std::string			srcFileName;

	for(int ii=0; ii < argc; ii++)
	{	// specific to MatBuilder
		if(strcmp(argv[ii], "-f") == 0)
			gazeEegEnv.falseBlinkDuration	= atoi(argv[++ii]);
		else if(strcmp(argv[ii], "-t") == 0)
			gazeEegEnv.triggerFileName		= argv[++ii];
		else if(strcmp(argv[ii], "-p") == 0)
			gazeEegEnv.onPupil				= true;
		else if(strcmp(argv[ii], "-v") == 0)
			gazeEegEnv.onVideoframe			= true;
		else if(strcmp(argv[ii], "-h") == 0)
			gazeEegEnv.outputPath			= argv[++ii];

		// hide options
		else if(strcmp(argv[ii], "-G") == 0)
			gazeEegEnv.doDebug				= true;
		else if(strcmp(argv[ii], "-P") == 0)
			gazeEegEnv.doPause				= true;
		else if(strcmp(argv[ii], "-T") == 0)
			gazeEegEnv.doTtl				= true;
		else if(argv[ii][0] != '-')
			srcFileName	= argv[ii];
	}

	std::string		path, name, ext;
	Parser::ParsePath(path, name, ext, srcFileName);

	if(gazeEegEnv.outputPath.empty())
		gazeEegEnv.outputPath = path;
	else
	{	if(!Parser::CreatePath(gazeEegEnv.outputPath))
		{
			std::cerr << "!!! Can not create output path -> " << gazeEegEnv.outputPath << std::endl;
			
			system("pause");

			exit(-3);
		}

		gazeEegEnv.ProcessOutputMatFiles();
	}

	AscRawData	myAscRawData;
	Brainamp	myBrainamp;
	
	if(myAscRawData.IsValid(gazeEegEnv, srcFileName))
	{	GazeEeg::File::Remove(gazeEegEnv.gazeMatFileName);
		
		gazeEegEnv.isAscFile	= true;
		ExecEnvironment(argc, argv, gazeEegEnv);

		if(!myAscRawData.Process(gazeEegEnv))
		{	std::cerr << "!!! Can not parse the source ASC file !!!" << std::endl;
				
			system("pause");

			exit(-2);
		}

		if(!myAscRawData.SaveMatFile(gazeEegEnv) /*|| !myAscRawData.SaveBinFile(matFile)*/)
		{
			std::cerr << "!!! Can not create a valid MAT file -> " << gazeEegEnv.gazeMatFileName << std::endl;
			
			system("pause");

			exit(-3);
	}	}
	else if(myBrainamp.IsValid(gazeEegEnv, srcFileName))
	{	GazeEeg::File::Remove(gazeEegEnv.eegMatFileName);
	
		Parser::binaryFormat_t	binaryFormat = myBrainamp.GetBinaryFormat(gazeEegEnv.eegFileName);

		bool	bProcess = false;
		switch(binaryFormat)
		{	case Parser::int16_type:
				{	ExecEnvironment(argc, argv, gazeEegEnv);

					bProcess = myBrainamp.Process(gazeEegEnv, Parser::int16NaN);
				}
				break;
			case Parser::float_type:
				{	ExecEnvironment(argc, argv, gazeEegEnv);

					bProcess = myBrainamp.Process(gazeEegEnv, Parser::floatNaN);
				}
				break;
			case Parser::double_type:
				{	ExecEnvironment(argc, argv, gazeEegEnv);

					bProcess = myBrainamp.Process(gazeEegEnv, Parser::doubleNaN);
				}
				break;
			default:
				std::cerr << "!!! Invalid EEG data format" << std::endl;
				system("pause");
				exit(-3);
				break;
		}
		
		if(!bProcess)
		{
			std::cerr << "!!! Can not process the EEG files -> " 
				<< (gazeEegEnv.isAscFile ? gazeEegEnv.gazeFileName : gazeEegEnv.eegFileName) << std::endl;
			
			system("pause");

			exit(-3);
		}

		gazeEegEnv.eventFileName	= gazeEegEnv.eegFileName + MEventFile;
		
		if(!myBrainamp.SaveMatFile(gazeEegEnv) /*|| !myAscRawData.SaveBinFile(matFile)*/)
		{
			std::cerr << "!!! Can not create a valid MAT file -> " << gazeEegEnv.eegMatFileName << std::endl;
			
			system("pause");

			exit(-3);
	}	}
	else
		Help(argc, argv, _ERROR_FILE_EXISTS, gazeEegEnv.error);

	std::cout << "!!! MatBuilder END !!! " << std::endl;

	if(gazeEegEnv.doPause)
		system("pause");

	return 0;
}