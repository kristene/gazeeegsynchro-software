/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

#pragma once

#include <string>
#include <vector>
#include <sstream>
#include <map>

typedef std::map<int,	std::string>					markerMap_t;
typedef std::pair<int,	std::string>					markerPair_t;
typedef std::map<int,	std::string>::iterator			markerMapIt_t;
typedef std::map<int,	std::string>::const_iterator	markerMapConstIt_t;

class Event;

class CMarker
{
public:
	typedef enum
	{	MARKER_SEGMENT,
		MARKER_STIMULUS,
		MARKER_OTHER,
		MARKER_OTHER_VAL	= 0,
		MARKER_SEGMENT_VAL	= 256,
		MARKER_OVERFLOW_VAL = 257,
	} marker_t;

	class Segment
	{
	public:
		Segment() 
			: start(0), inflexion(0), stop(0), slope(0.0) {}

		uint32	start;
		uint32	inflexion;
		uint32	stop;
		double	slope;
	};

public:
	CMarker(const std::string& text, const uint16 value, const uint32 offset, const uint32 duration, const uint32 fseek);
	CMarker(const std::string& text, const uint32 offset, const std::string& ts);
	CMarker(const std::string& text, const uint32 offset);
	CMarker(const bool extra, const uint16 value, const std::string& evStimName, const std::string& text, const uint32 offset);
	virtual ~CMarker(){}

	bool	IsStimulation() const	{	return type == MARKER_STIMULUS;				}
	bool	IsTrigger() const		{	return IsStimulation() && (value < 0x100);	}
	bool	IsSegment() const		{	return type == MARKER_SEGMENT;				}
	bool	IsOther() const			{	return type == MARKER_OTHER;				}
	bool	IsExtra() const			{	return isExtra;								}
	
	uint32	ProcessSegmentTimestamp(const double samplingRate);
	
public:	
	marker_t	type;
	bool		isExtra;
	std::string	text;
	std::string	evStimName;
	// for stimulations
	uint16		value;						// trigger value
	uint32		offset;						// offset in EEG file
	uint32		duration;					// event duration
	uint32		fseek;						//file position
	
	// for segments
	std::string	startTimestamp;
	double		startSampleD;				// position in absolute time
	uint32		samplesInsertedBefore;		// extra samples to add
	int32		correctionInsertedBefore;	// correction of samplesInsertedBefore
	Segment		segmentCorrection;

	// segment synchro info
	uint32		start;
	uint32		stop;

	// src event pointer
	Event*		srcEvent;
};

typedef std::vector<CMarker>	MarkerList_t;
