/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

#include "StdAfx.h"

#include "Parser.h"

const unsigned long long	_llNaN		= 0xFFFFFFFFFFFFFFFF;
const float					_flNaN		= *((float*)  &_llNaN);
const double				_doNaN		= *((double*) &_llNaN);

Parser::double_t	Parser::doubleNaN	= *((double*) &_llNaN);
Parser::double_t	Parser::gazeNaN		= -2000.0;
Parser::float_t		Parser::floatNaN	= *((float*) &_llNaN);
Parser::int16_t		Parser::int16NaN	= int16(0x08000);

Parser::Parser(void)
{
}

Parser::~Parser(void)
{
}

void Parser::ParseChar(std::vector<char*>& parsedContent, char* pStr, const size_t szStr, const char delimiter, std::vector<uint32>* pSeek /*= 0*/)
{
	size_t  parseSize       = 15;
	char*   last            = pStr + szStr;

	parseSize = 0;
	bool push = true;
	for(char* pCh=pStr; pCh < last; pCh++)
	{	if(*pCh == delimiter)
			push    = true;
		else if(push)
		{	push    = false;
			parseSize++;
	}	}

	parsedContent.clear();
	parsedContent.reserve(parseSize);

	if(pSeek)
	{	pSeek->clear();
		pSeek->reserve(parseSize + 5);
	}

	push = true;
	for(char* pCh=pStr; pCh < last; pCh++)
	{	if(*pCh == delimiter)
		{	push    = true;
			*pCh    = 0;
		}
		else if(push)
		{	push    = false;
			parsedContent.push_back(pCh);
			if(pSeek)
				pSeek->push_back(pCh - pStr);
	}	}
}

void Parser::ParsePath(std::vector<std::string>& segments, const std::string& fullPathName)
{
	segments.clear();
	std::string::size_type pos = fullPathName.find_last_of("/\\");

	std::string	fileName;
	if(pos == std::string::npos)
	{	segments.push_back(".");
		fileName	= fullPathName;
	}
	else
	{	segments.push_back(fullPathName.substr(0, pos));
		fileName	= fullPathName.substr(pos+1);
	}

	pos = fileName.find_last_of(".");
	if(pos == std::string::npos)
	{	segments.push_back(fileName);
		segments.push_back("");
	}
	else
	{	segments.push_back(fileName.substr(0, pos));
		segments.push_back(fileName.substr(pos+1));
	}
}

/* Returns all space separated strings from a single file line */
void Parser::ParseSpaces(std::vector<char*>& parsedContent, char* pStr, const size_t szStr)
{
	parsedContent.clear();
	parsedContent.reserve(15);

	char* last      = pStr + szStr;
	bool  push      = true;
	for(char* pCh=pStr; pCh < last; pCh++)
	{	char	ch	= *pCh;   
		if((ch == ' ') || (ch == '\t') || (ch == '\n') || (ch == '\r'))
		{	push	= true;
			*pCh    = 0;
		}
		else if(push)
		{	push    = false;
			parsedContent.push_back(pCh);
	}	}
}

bool Parser::CreatePath(const std::string& pathName)
{
	std::string	workPath(pathName);
	
	std::vector<char*> segments;
	Parser::ParseChar(segments, &workPath[0], workPath.length(), '\\');

	std::string createPath(*segments.begin());
	for(std::vector<char*>::iterator it=segments.begin()+1; it != segments.end(); it++)
	{	createPath += '\\';
		createPath += *it;
		if(!GazeEeg::File::Mkdir(createPath))
			return false;
	}

	return true;
}

std::string Parser::ParseFileName(char* pStr)
{
	char* last = pStr + ::strlen(pStr) - 1;
	while(last >= pStr)
	{	if((*last == '/') || (*last == '\\'))
			break;
		last--;
	}

	return std::string(last + 1);
}

void Parser::ParsePath(std::string& path, std::string& name, std::string& ext, const std::string& src)
{
	std::string	workFile(src);
	std::replace(workFile.begin(), workFile.end(), '/', '\\');

	std::string::size_type	posExt	= workFile.find_last_of(".");
	std::string::size_type	posName = workFile.find_last_of("\\");

	path	= workFile.substr(0, posName);
	name	= workFile.substr(posName + 1, posExt - posName - 1);
	ext		= workFile.substr(posExt + 1);
}

bool Parser::IsValidInt(char* pStr)
{
	while(*pStr)
	{	if(!isdigit(*pStr++))
			return false;
	}

	return true;
}

bool Parser::IsValidFloat(char* pStr)
{
	while(*pStr)
	{	if(!(((*pStr >= '0') && (*pStr <= '9')) || (*pStr == '.') || (*pStr == '-')))
			return false;

		pStr++;
	}

	return true;
}

void Parser::Transpose(std::vector<char>& data, const size_t lineSize)
{
	size_t colSize = data.size()/lineSize;

	std::vector<char> tmp(data);

	for(size_t jj=0; jj < colSize; jj++)
	{	for(size_t ii=0; ii < lineSize; ii++)
		{	size_t	posSrc	= jj*lineSize + ii;
			size_t	posDst	= ii*colSize + jj;
			
			data[posDst]	= tmp[posSrc];
	}	}
}

int Parser::GetInt(const std::string& str, const size_t pos, const size_t len)
{
	int					val;
	std::istringstream	iss(str.substr(pos, len));

	iss >> val;

	return val;
}

