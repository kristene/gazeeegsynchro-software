/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

#include "TriggerSyncStat.h"
#include <iomanip>
#include <fstream>
#include <iostream>
#include <numeric>
#include <algorithm>

TriggerSyncStat::TriggerSyncStat(const std::vector<double> &data)
{
	for (int i = 0; i < binsSize; i++) values[i] = 0;
	processedTriggersCount = 0;

	for (int i = 0; i < data.size(); i++)
	{
		for (int j = 0; j < binsSize; j++)
		{
			if (j == 0)
			{
				if (data[i] < bins[0] + 0.5)
				{
					values[j]++;
					processedTriggersCount++;
				}
			}
			else
			if (j == (binsSize - 1))
			{
				if (data[i] > bins[(binsSize - 1)] - 0.5)
				{
					values[j]++;
					processedTriggersCount++;
				}
			}
			else
			{
				double p1 = bins[j] - 0.5;
				double p2 = bins[j] + 0.5;

				if (data[i] >= p1 && data[i] < p2)
				{
					values[j]++;
					processedTriggersCount++;
				}
			}
		}
	}

	allTriggersCount = data.size();

	//calcualte mean
	double sum = std::accumulate(data.begin(), data.end(), 0.0);
	/*int sum = 0;
	for (int i = 0; i < binsSize; i++)
	{
		sum = sum + bins[i] * values[i];
	}*/
	double m = sum / (double)data.size();
	mean = m;

	//calculate std
	std::vector<double> diff(data.size());
	std::transform(data.begin(), data.end(), diff.begin(), [m](double x) { return x - m; });
	double sq_sum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
	stdev = std::sqrt(sq_sum / (double)data.size());
}

TriggerSyncStat::TriggerSyncStat(std::vector<int> &triggers,const std::vector<double> &data, int marker)
{
	if (triggers.size() != data.size())
		std::cout << "Warning: Inconsistency detected while creating histogram!";

	// clear histogram
    for (int i = 0; i < binsSize; i++) values[i] = 0;
	processedTriggersCount = 0;

	std::vector<double> filteredData; //only with the trigger specified

	//build histogram with filtering (only the trigger/marker specified)
	for (int i = 0; i < data.size(); i++)
	{
		if (triggers[i] == marker)
		{
			for (int j = 0; j < binsSize; j++)
			{
				if (j == 0)
				{
					if (data[i] < bins[0] + 0.5)
					{
						values[j]++;
						processedTriggersCount++;
					}
				}
				else
				if (j == (binsSize - 1))
				{
					if (data[i] > bins[(binsSize - 1)] - 0.5)
					{
						values[j]++;
						processedTriggersCount++;
					}
				}
				else
				{
					double p1 = bins[j] - 0.5;
					double p2 = bins[j] + 0.5;

					if (data[i] >= p1 && data[i] < p2)
					{
						values[j]++;
						processedTriggersCount++;
					}
				}
			}

			filteredData.push_back(data[i]);
		}
	}

	allTriggersCount = filteredData.size(); 

	//Calculate mean
	double sum = std::accumulate(filteredData.begin(), filteredData.end(), 0.0);
	/*int sum = 0;
	for (int i = 0; i < binsSize; i++)
	{
		sum = sum + bins[i] * values[i];
	}*/
	double m = sum / (double)filteredData.size();
	mean = m;

	//Calculate std
	std::vector<double> diff(filteredData.size());
	std::transform(filteredData.begin(), filteredData.end(), diff.begin(), [m](double x) { return x - m; });
	double sq_sum = std::inner_product(diff.begin(), diff.end(), diff.begin(), 0.0);
	stdev = std::sqrt(sq_sum / (double)filteredData.size());
}

void TriggerSyncStat::PrintHistogram(std::ofstream &ofs)
{
	for (int i = 0; i < binsSize; i++)
	{
		ofs << std::setw(2) << bins[i] << ' '
			<< std::setw(5) << values[i] << std::endl;
	}
}

double TriggerSyncStat::GetMean()
{
	return mean;
}
double TriggerSyncStat::GetStdev()
{
	return stdev;
}

int TriggerSyncStat::GetTriggersCount()
{
	return allTriggersCount;
}

int TriggerSyncStat::GetProcessedTriggersCount()
{
	return processedTriggersCount;
}

TriggerSyncStat::~TriggerSyncStat()
{
}
