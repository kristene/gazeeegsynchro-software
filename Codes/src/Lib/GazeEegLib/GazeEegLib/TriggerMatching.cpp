/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

#include "StdAfx.h"

#include "TriggerMatching.h"
#include "GazeEegEnv.h"
#include "TriggerSyncStat.h"

#define ARTICLE_HISTO_SEG

TriggerMatching::TriggerMatching(MarkerList_t& stim1, const double acqF1, MarkerList_t& stim2, const double acqF2, const std::vector<MarkerList_t::iterator>* segments /*= 0*/) 
	: mySegments(segments)
	, myStimuli1(stim1)
	, myStimuli2(stim2)
	, myIdealSamplingRatio(acqF2/acqF1)
	, myRealSamplingRatio(acqF2/acqF1)
	, myFirstSegment(0)
	, myLastSegment(segments ? segments->size() : 0)
	, myEEGBufferOveflowStartOffset(-1)
	, myEEGBufferOveflowEndOffset(-1)
{
}

TriggerMatching::~TriggerMatching()
{
}

bool TriggerMatching::LongestCommonSubstring(GazeEegEnv& gazeEegEnv, const std::string& prompt)
{
	ApplyBufferOverflowCorrection();

	if (!MatchTriggers(gazeEegEnv)) { std::cout << "ERROR: Problem with TriggerMatching!" << std::endl; return false; }

	const std::vector<Lcs::Indexes>& indexes = GetIndexes();

	if (indexes.size() == 0) { std::cout << "ERROR: Problem with TriggerMatching and indexes!" << std::endl; return false; }

	std::cout << std::endl;
	std::cout << prompt << std::endl;
	std::cout << "LCS   Triggers = " << std::setw(5) << indexes.size() << std::endl;
	std::cout << "First Pair     = " << std::setw(5) << myStimuli1[indexes.begin()->index1].value	<< "," << std::setw(3) << myStimuli2[indexes.begin()->index2].value		<< std::endl;
	std::cout << "Last  Pair     = " << std::setw(5) << myStimuli1[indexes.rbegin()->index1].value	<< "," << std::setw(3) << myStimuli2[indexes.rbegin()->index2].value	<< std::endl;
	std::cout << "Nb    Segments = " << std::setw(5) << myLastSegment	<< std::endl;
	std::cout << std::endl;


/*	 FILE *f = fopen("indexes.csv", "w");
	for (int i = 0; i < indexes.size(); i++)
		fprintf(f, "%d;%d\n", indexes[i].index1, indexes[i].index2);
	fclose(f);
	f = fopen("mystimuli1.csv", "w");
	for (int i = 0; i < myStimuli1.size(); i++)
		fprintf(f, "%d\n", myStimuli1[i].value);
	fclose(f);
	f = fopen("mystimuli2.csv", "w");
	for (int i = 0; i < myStimuli2.size(); i++)
		fprintf(f, "%d\n", myStimuli2[i].value);
	fclose(f);
	f = fopen("mystimuli2_startsampleD.csv", "w");
	for (int i = 0; i < myStimuli2.size(); i++)
		fprintf(f, "%d\n", myStimuli2[i].startSampleD);
	fclose(f);
	f = fopen("mystimuli1time.csv", "w");
	for (int i = 0; i < myStimuli1.size(); i++)
		fprintf(f, "%d\n", myStimuli1[i].offset);
	fclose(f);

	f = fopen("mystimuli2time.csv", "w");
	for (int i = 0; i < myStimuli2.size(); i++)
		fprintf(f, "%d\n", myStimuli2[i].offset);
	fclose(f);

	f = fopen("mystimuli2duration.csv", "w");
	for (int i = 0; i < myStimuli2.size(); i++)
		fprintf(f, "%d\n", myStimuli2[i].duration);
	fclose(f);

	f = fopen("pairs.csv", "w");
	for (int i = 0; i < indexes.size(); i++)
		fprintf(f, "%d;%d\n", myStimuli1[indexes[i].index1].value, myStimuli2[indexes[i].index2].value);
	fclose(f); 
	*/



#if defined(ARTICLE_HISTO_SEG)
	ProcessArticleHisto(gazeEegEnv, indexes);
#endif
		
	DoCorrection(gazeEegEnv);

	return true;
}

void TriggerMatching::ProcessArticleHisto(GazeEegEnv& gazeEegEnv, const std::vector<Lcs::Indexes>& indexes)
{
	size_t	indStim = 0;
	uint32	markerOff1, markerOff2;
	MarkerList_t::iterator	markerItMemo2;
	for(std::vector<Lcs::Indexes>::const_iterator it=indexes.begin(); it != indexes.end(); it++, indStim++)
	{	MarkerList_t::iterator	markerIt1	= myStimuli1.begin() + it->index1;
		MarkerList_t::iterator	markerIt2	= myStimuli2.begin() + it->index2;
		uint16	trigger1 = markerIt1->value;
		uint16	trigger2 = markerIt2->value;
		if(indStim == 0)
		{	markerOff1	= markerIt1->offset;
			markerOff2	= markerIt2->offset;
		}
	
		uint32	off1	= markerIt1->offset - markerOff1;
		uint32	off2	= markerIt2->offset - markerOff2;				
		int		delta	= int(off1) - int(round(off2/myIdealSamplingRatio));
		if(delta > GazeEegEnv::Article::HALF_WIDTH)
			delta	= GazeEegEnv::Article::HALF_WIDTH;
		else if(delta < -GazeEegEnv::Article::HALF_WIDTH)
			delta	= -GazeEegEnv::Article::HALF_WIDTH;

		int index	= gazeEegEnv.article.indHist + GazeEegEnv::Article::HALF_WIDTH + delta;

		(gazeEegEnv.article.histTtlEegEt[index])++;
	}

	gazeEegEnv.article.indHist += GazeEegEnv::Article::FULL_WIDTH;
}

// 3 seg 11, 14
// 2 seg 2, 4, 5, 7, 13, 16, 17, 19

bool TriggerMatching::MatchTriggers(GazeEegEnv& gazeEegEnv)
{
	std::vector<short>	list1;
	list1.reserve(myStimuli1.size());

	for (MarkerList_t::const_iterator it = myStimuli1.begin(); it != myStimuli1.end(); it++)
		list1.push_back(short(it->value));

	std::vector<short>	list2;
	list2.reserve(myStimuli2.size());

	for (MarkerList_t::const_iterator it = myStimuli2.begin(); it != myStimuli2.end(); it++)
		list2.push_back(short(it->value));

	if (gazeEegEnv.doClassicLCS)
		return myLcs.Process2(list1, list2);
	else
		return myLcs.Process1(list1, list2);
}

void TriggerMatching::ProcessSegmentIndexes()
{
	const std::vector<Lcs::Indexes>& indexes = GetIndexes();

	size_t	indStim = 0;
	for(std::vector<Lcs::Indexes>::const_iterator it=indexes.begin(); it != indexes.end(); it++, indStim++)
	{	MarkerList_t::iterator	markerIt	= myStimuli2.begin() + it->index2;
		
		std::vector<MarkerList_t::iterator>::const_iterator it1;
		MarkerList_t::iterator								segmentIt;
		for(it1=mySegments->begin(); it1 != mySegments->end(); it1++)
		{	segmentIt = *it1;
			if(segmentIt > markerIt)
				break;
		}

		segmentIt = *--it1;
		if(segmentIt->IsExtra())
		{	segmentIt->isExtra	= false;
			segmentIt->start	= indStim;
		}

		segmentIt->stop			= indStim;
	}

	// elliminate all segments that don't contain triggers at the begining
	for(std::vector<MarkerList_t::iterator>::const_iterator it=mySegments->begin(); it != mySegments->end(); it++)
	{	MarkerList_t::iterator segmentIt = *it;
		if(!segmentIt->IsExtra() && (segmentIt->start != segmentIt->stop))
			break;

		myFirstSegment++;
	}
		
	// elliminate all segments that don't contain triggers at the end
	for(std::vector<MarkerList_t::iterator>::const_reverse_iterator it=mySegments->rbegin(); it != mySegments->rend(); it++)
	{	MarkerList_t::iterator segmentIt = *it;
		if(!segmentIt->IsExtra() && (segmentIt->start != segmentIt->stop))
			break;

		myLastSegment--;
	}
}

void TriggerMatching::DoCorrection(GazeEegEnv& gazeEegEnv)
{
	if(!mySegments)
		return;

	ProcessSegmentIndexes();

	// 1-1 2-2 11-3 14-3
	const std::vector<Lcs::Indexes>& indexes = GetIndexes();
		
	uint32				first1, last1, first2, last2, nbSamples1 = 0, nbSamples2 = 0, nbTrueSamples2 = 0, nbInsertBefore = 0, nbExtraSamples2 = 0, nbTrueExtraSamples2 = 0, nbSegments = 0;
	
	std::vector<MarkerList_t::iterator>::const_iterator	firstIt = mySegments->begin() + myFirstSegment;
	std::vector<MarkerList_t::iterator>::const_iterator	lastIt	= mySegments->begin() + myLastSegment;

	myRealSamplingRatio	= 0;
	double realSamplingRatio	= 0;
	
	for(std::vector<MarkerList_t::iterator>::const_iterator it=firstIt; it != lastIt; it++)
	{	
		MarkerList_t::iterator segmentIt = *it;
		if(segmentIt->IsExtra())
			continue;

		nbSegments++;
		nbInsertBefore	+= segmentIt->samplesInsertedBefore;

		std::vector<Lcs::Indexes>::const_iterator itStart	= indexes.begin()+segmentIt->start;
		std::vector<Lcs::Indexes>::const_iterator itStop	= indexes.begin()+segmentIt->stop;

		first1				= myStimuli1[itStart->index1].offset;
		first2				= myStimuli2[itStart->index2].offset;
		nbSamples1			= 0;
		nbSamples2			= 0;

		while(++itStart <= itStop)
		{	
			last1				= myStimuli1[itStart->index1].offset;
			last2				= myStimuli2[itStart->index2].offset;
			nbSamples1		   += last1 - first1;
			nbSamples2		   += last2 - first2;
		}

		segmentIt->segmentCorrection.slope	= double(nbSamples2)/double(nbSamples1);
		myRealSamplingRatio				   += segmentIt->segmentCorrection.slope;
	}
	myRealSamplingRatio	   /= nbSegments;

#if 0
	first1					= myStimuli1[indexes.begin()->index1].offset;
	first2					= myStimuli2[indexes.begin()->index2].offset;
	last1					= myStimuli1[indexes.rbegin()->index1].offset;
	last2					= myStimuli2[indexes.rbegin()->index2].offset;
	nbSamples1				= last1 - first1;

	(*firstIt)->duration   -= first2;
	(*(lastIt-1))->duration	= last2 - ((*(lastIt-1))->offset + nbInsertBefore);
	
	nbTrueSamples2			= uint32(round(nbSamples1*myRealSamplingRatio));
	nbSamples2				= 0;
	for(std::vector<MarkerList_t::iterator>::const_iterator it=firstIt; it != lastIt; it++)
	{	nbSamples2		   += (*it)->duration;
		nbExtraSamples2	   += (*it)->samplesInsertedBefore;
	}
	nbTrueExtraSamples2		= nbTrueSamples2 - nbSamples2;

	double	correctionK		= double(nbTrueExtraSamples2)/double(nbExtraSamples2);
	for(std::vector<MarkerList_t::iterator>::const_iterator it=firstIt; it != lastIt; it++)
	{	uint32 trueCorrection	= uint32((*it)->samplesInsertedBefore*correctionK);
		int32  shift			= trueCorrection - (*it)->samplesInsertedBefore;
		
		for(MarkerList_t::iterator it1=(*it); it1 != myStimuli2.end(); it1++)
		{	if(it1->IsSegment())
			{	it1->samplesInsertedBefore		+= shift;
				it1->correctionInsertedBefore	 = shift;
			}
			else
				it1->offset						+= shift;
	}	}

#else


	for(std::vector<MarkerList_t::iterator>::const_iterator it=firstIt; it != lastIt; it++)
	{	MarkerList_t::iterator segmentIt = *it;
		if(segmentIt->IsExtra())
			continue;

		std::vector<Lcs::Indexes>::const_iterator itStart	= indexes.begin()+segmentIt->start;
		std::vector<Lcs::Indexes>::const_iterator itStop	= indexes.begin()+segmentIt->stop;

		if(it == firstIt)
		{	first1				= myStimuli1[itStart->index1].offset;
			last1				= myStimuli1[itStop->index1].offset;
			first2				= myStimuli2[itStart->index2].offset;
			last2				= myStimuli2[itStop->index2].offset;
			
			nbSamples1			= last1 - first1;
			nbSamples2			= last2 - first2;
			myRealSamplingRatio	= double(nbSamples2)/double(nbSamples1);
			realSamplingRatio	= GetSlope(firstIt, it);
			continue;
		}

		int		incrShift	= (myRealSamplingRatio < 1) ? -1 : 1;
		int		shift		= incrShift;
#if 1
		GetEnergy(shift, segmentIt, first1, first2, myRealSamplingRatio, incrShift);
#else
		double	energy		= GetEnergy(segmentIt, first1, first2, 0);
		while(true)
		{	double en		= GetEnergy(segmentIt, first1, first2, shift);
			if(en > energy)
				break;

			energy			= en;
			shift		   += incrShift;
		}
#endif

		for(MarkerList_t::iterator it1=segmentIt; it1 != myStimuli2.end(); it1++)
		{	if(it1->IsSegment())
			{	it1->samplesInsertedBefore		+= shift;
				it1->correctionInsertedBefore	 = shift;
			}
			else
				it1->offset	+= shift;
		}

		last1				= myStimuli1[(indexes.begin()+segmentIt->stop)->index1].offset;
		last2				= myStimuli2[(indexes.begin()+segmentIt->stop)->index2].offset;
		nbSamples1			= last1 - first1;
		nbSamples2			= last2 - first2;
		myRealSamplingRatio	= double(nbSamples2)/double(nbSamples1);
		realSamplingRatio	= GetSlope(firstIt, it);
	}
#endif

#if defined(ARTICLE_HISTO_SEG)
	//std::ofstream	ofs("C:\\tmp\\HistoSeg.txt", std::ios::out|std::ios::trunc);
	std::ofstream	ofs(GazeEegEnv::LogFolder + std::string("GlobalError.txt"), std::ios::out|std::ios::trunc);
	
	nbSegments = myLastSegment - myFirstSegment;

	ofs << "matchingSlope = "	<< std::setprecision(15) << myRealSamplingRatio	<< std::endl;
	ofs << "slopePerSegment = "	<< nbSegments								<< std::endl;
	for(std::vector<MarkerList_t::iterator>::const_iterator it=firstIt; it != lastIt; it++)
		ofs << std::setprecision(15) << (*it)->segmentCorrection.slope << ' ';
	ofs << std::endl;
		
	ofs << "nbTriggers = "		<< indexes.size()								<< std::endl;
	ofs << "triggersPerSegment = "	<< nbSegments								<< std::endl;	
	for(std::vector<MarkerList_t::iterator>::const_iterator it=firstIt; it != lastIt; it++)
		ofs << ((*it)->stop - (*it)->start + 1) << ' ';
	ofs << std::endl;
	
	ofs << "samplesInsertedBefore = "	<< nbSegments									<< std::endl;
	for(std::vector<MarkerList_t::iterator>::const_iterator it=firstIt; it != lastIt; it++)
		ofs << (*it)->samplesInsertedBefore << ' ';
	ofs << std::endl;
	
	ofs << "correctionInsertedBefore = "	<< nbSegments									<< std::endl;
	for(std::vector<MarkerList_t::iterator>::const_iterator it=firstIt; it != lastIt; it++)
		ofs << (*it)->correctionInsertedBefore << ' ';
	ofs << std::endl;
	
	uint32	histoFirst1;
	uint32	histoFirst2;
	uint32	extraAdd		= 0;
	uint32	insertBefore	= 0;

	// prepare histogram
	double	sumPred = 0.0, sumPred2 = 0.0, sumCalc = 0.0, sumCalc2 = 0.0, sumCorrel = 0.0;
	int		nbTrig = 0;

	std::vector<double> errorDriftOnShiftOn;
	std::vector<int> triggers;

	ofs << std::setw(15) << std::setprecision(5) << std::fixed << "[final drift on, shift on]" << ' '
		<< std::setw(15) << std::setprecision(5) << std::fixed << "[drift on, shift off]" << ' '
		<< std::setw(15) << std::setprecision(5) << std::fixed << "[drift off, shift off]" << std::endl;

	for(std::vector<MarkerList_t::iterator>::const_iterator it=firstIt; it != lastIt; it++)
	{	
		MarkerList_t::iterator segmentIt = *it;
		if(segmentIt->IsExtra())
			continue;

		double realSamplingRatio = segmentIt->segmentCorrection.slope;

		extraAdd		+= segmentIt->correctionInsertedBefore;
		insertBefore	+= segmentIt->samplesInsertedBefore;

		std::vector<Lcs::Indexes>::const_iterator itStart	= indexes.begin()+segmentIt->start;
		std::vector<Lcs::Indexes>::const_iterator itStop	= indexes.begin()+segmentIt->stop;

		if(it == firstIt)
		{	
			histoFirst1			= myStimuli1[itStart->index1].offset;
			histoFirst2			= myStimuli2[itStart->index2].offset;
		}

		while(itStart <= itStop)
		{	
			uint32	offset1 = myStimuli1[itStart->index1].offset;
			uint32	offset2 = myStimuli2[itStart->index2].offset;
			uint32	diff1	= offset1 - histoFirst1;
			uint32	diff2	= offset2 - histoFirst2;

			double	vPred	= double(diff1);
			double	vCalc	= double(diff2)/myRealSamplingRatio;
			double	d1		= vPred - double(diff2)/myRealSamplingRatio; //Anton: shift on, drift on

			//Add trigger for histogram
			errorDriftOnShiftOn.push_back(d1);
			triggers.push_back(myStimuli2[itStart->index2].value);

			double	d2		= vPred - double(diff2 + extraAdd)/myRealSamplingRatio;
			double	d3		= vPred - double(diff2 + insertBefore + extraAdd)/myRealSamplingRatio;

			//			int		indHist	= int(::round(d1));
//			gazeEegEnv.quality.histogram[indHist+5]++;
//			ofs << diff1 << ' ' << (diff2 + extraAdd) << std::endl;

			ofs << std::setw(15) << std::setprecision(5) << std::fixed << d1 << ' '
				<< std::setw(24) << std::setprecision(5) << std::fixed << d2 << ' '
				<< std::setw(25) << std::setprecision(5) << std::fixed << d3 << std::endl;

			sumPred		+= vPred;
			sumPred2	+= vPred*vPred;
			sumCalc		+= vCalc;
			sumCalc2	+= vCalc*vCalc;
			sumCorrel	+= vPred*vCalc;
			nbTrig++;

			itStart++;
	    }	
	}

	WriteErrorHistograms(triggers, errorDriftOnShiftOn);
#endif

	double	meanPred	= sumPred/nbTrig;
	double	meanCalc	= sumCalc/nbTrig;
	double	meanCorrel	= sumCorrel/nbTrig;
	double	meanPred2	= sumPred2/nbTrig;
	double	meanCalc2	= sumCalc2/nbTrig;
	double	varPred		= sumPred2/nbTrig;
	double	varCalc		= sumCalc2/nbTrig;
	double	r			= (nbTrig*sumCorrel - sumCalc*sumPred)/std::sqrt((nbTrig*sumPred2 - sumPred*sumPred)*(nbTrig*sumCalc2 - sumCalc*sumCalc));
	gazeEegEnv.quality.r2 = r*r;
}

double TriggerMatching::GetSlope(std::vector<MarkerList_t::iterator>::const_iterator& first, std::vector<MarkerList_t::iterator>::const_iterator& last)
{
    double	S       = 0; //double(length(x))/sigma2;
    double	Sx      = 0; //sum(x)/sigma2;
    double	Sy      = 0; //sum(y)/sigma2;
    double	Sxx     = 0; //sum(x .^ 2)/sigma2;
    double	Sxy     = 0; //sum(x .* y)/sigma2;
	
	const std::vector<Lcs::Indexes>& indexes = GetIndexes();

	for(std::vector<MarkerList_t::iterator>::const_iterator it=first; it <= last; it++)
	{	if((*it)->IsExtra())
			continue;

		std::vector<Lcs::Indexes>::const_iterator itStart	= indexes.begin()+(*it)->start;
		std::vector<Lcs::Indexes>::const_iterator itStop	= indexes.begin()+(*it)->stop;

		while(itStart <= itStop)
		{	double x	 = myStimuli1[itStart->index1].offset;
			double y	 = myStimuli2[itStart->index2].offset + (*it)->samplesInsertedBefore + (*it)->correctionInsertedBefore;
			S			+= 1;
			Sx			+= x;
			Sy			+= y;
			Sxx			+= x*x;
			Sxy			+= x*y;

			itStart++;
		}
	}

	double	delta   = S*Sxx - Sx*Sx;

    double	N       = (Sxx*Sy - Sx*Sxy)/delta;
    double	M       = (S*Sxy - Sx*Sy)/delta;

	return M;
}


double TriggerMatching::GetEnergy(MarkerList_t::iterator& segmentIt, const uint32 first1, const uint32 first2, const int shiftLoop)
{
	const std::vector<Lcs::Indexes>& indexes = GetIndexes();
	
	double energy = 0;
	for(uint32 ii=segmentIt->start; ii <= segmentIt->stop; ii++)
	{	uint32	last1		= myStimuli1[(indexes.begin()+ii)->index1].offset;
		uint32	last2		= myStimuli2[(indexes.begin()+ii)->index2].offset + shiftLoop;
		uint32	nbSamples1	= last1 - first1;
		uint32	nbSamples2	= last2 - first2;
		double	last2in1	= first1 + nbSamples2/myRealSamplingRatio;
		double	en			= double(last1) - last2in1;

		energy			   += en;
	}

	return abs(energy/(segmentIt->stop - segmentIt->start + 1));
}

void TriggerMatching::GetEnergy(int& shift, MarkerList_t::iterator& segmentIt, const uint32 first1, const uint32 first2, const double slope, const int incrShift)
{
	double	n = first2 - slope*first1;
	double	k = sqrt(1 + slope*slope);
	double	a = -slope/k;
	double	b = 1/k;
	double	c = -n/k;

	double	enMax = 0;

	const std::vector<Lcs::Indexes>& indexes = GetIndexes();
	
	shift = 0;
	while(true)
	{	double	enCrt = 0;
		for(uint32 ii=segmentIt->start; ii <= segmentIt->stop; ii++)
		{	uint32	x	= myStimuli1[(indexes.begin()+ii)->index1].offset;
			uint32	y	= myStimuli2[(indexes.begin()+ii)->index2].offset + shift;

			double	en = a*x + b*y + c;
			enCrt	+= en;
		}

		enCrt		= abs(enCrt);
		if(shift && (enCrt > enMax))
			break;

		enMax	 = enCrt;
		shift	+= incrShift;
	}
}


void TriggerMatching::GetTriggerSynchro(TriggerSynchro& triggerSynchro, const bool doDebug) const
{
	const std::vector<Lcs::Indexes>& indexes = myLcs.GetIndexes();

	triggerSynchro.value.reserve(indexes.size());
	triggerSynchro.gazeOffset.reserve(indexes.size());
	triggerSynchro.eegOffset.reserve(indexes.size());
	triggerSynchro.diffs.reserve(indexes.size());

	CMarker& firstGaze	= *(myStimuli1.begin() + indexes.begin()->index1);
	CMarker& firstEeg	= *(myStimuli2.begin() + indexes.begin()->index2);
	for(std::vector<Lcs::Indexes>::const_iterator it=indexes.begin(); it != indexes.end(); it++)
	{	CMarker& gaze	= *(myStimuli1.begin() + it->index1);
		CMarker& eeg	= *(myStimuli2.begin() + it->index2);
		triggerSynchro.value.push_back(gaze.value);
		triggerSynchro.gazeOffset.push_back(gaze.offset	- firstGaze.offset);
		triggerSynchro.eegOffset.push_back(eeg.offset	- firstEeg.offset);
	}

	double	lastGazeOff	= *triggerSynchro.gazeOffset.rbegin();
	double	lastEegOff	= *triggerSynchro.eegOffset.rbegin();
	double	fact		= double(lastGazeOff)/lastEegOff;

	for(size_t ii=0; ii < triggerSynchro.gazeOffset.size(); ii++)
	{	double	gazeOff	= triggerSynchro.gazeOffset[ii];
		double	eegOff	= triggerSynchro.eegOffset[ii];
		eegOff		   *= fact;
		double	diff	= gazeOff - eegOff;
		triggerSynchro.diffs.push_back(int16(round(diff))); //DIG
		
		if(doDebug)
			std::cout << std::fixed << std::setprecision(2) << diff << " ";
	}

	if(doDebug)
		std::cout << std::endl;

	SetQualityFactor(triggerSynchro);
}

void TriggerMatching::SetQualityFactor(TriggerSynchro& triggerSynchro) const
{
	const	std::vector<Lcs::Indexes>& indexes = GetIndexes();
	size_t	stopIndex = indexes.size() - 1;

	uint32	first1				= myStimuli1[indexes.begin()->index1].offset;
	uint32	first2				= myStimuli2[indexes.begin()->index2].offset;
	uint32	nbSamples1			= myStimuli1[(indexes.begin()+stopIndex)->index1].offset - first1;
	uint32	nbSamples2			= myStimuli2[(indexes.begin()+stopIndex)->index2].offset - first2;
	double	realSamplingRatio	= double(nbSamples1)/double(nbSamples2);

	CPoint2d	pt1(0, 0);
	CPoint2d	pt2(nbSamples1, nbSamples2);
	CLine2d		line(pt1, pt2);
	line.Normalize();

	double	s2		= 0;
	double	s22		= 0;
	double	s2s2	= 0;
	double	s2s22	= 0;
	size_t										ii	= 0;
	std::vector<Lcs::Indexes>::const_iterator	it	= indexes.begin();
	for(size_t ii=0; ii <= stopIndex; it++, ii++)
	{	long	pos1	= myStimuli1[it->index1].offset	- first1;
		long	pos2	= myStimuli2[it->index2].offset	- first2;
		long	val1	= myStimuli1[it->index1].value;
		long	val2	= myStimuli2[it->index2].value;
		double	pos2D	= pos2*realSamplingRatio;
		long	pos2I	= int(::round(pos2D));
		int		drift	= pos2I - pos1; 

		CPoint2d	pt = line.GetIntersection(CLine2d::GetOyParallel(pos1));

		s2		+= double(pos2);
		s22		+= double(pos2)*double(pos2);
		s2s2	 = double(pos2) - pt.y;
		s2s22	+= s2s2*s2s2;
	}

	triggerSynchro.gazeFirst	= first1;
	triggerSynchro.eegFirst		= first2;
	triggerSynchro.R2			= 1.0 - s2s22/(s22 - s2*s2/(stopIndex + 1));
}


void TriggerMatching::ApplyBufferOverflowCorrection()
{
	//find if there is a buffer oveflow (257) in EEG stream
	int i = 0;
	int pos = -1;
	for (i = 0; i < myStimuli2.size(); i++)
		if (myStimuli2[i].value == 257) { pos = i; break; }

	if (pos == -1)
	{
		return;
	}
	else
	if (pos > -1) //apply correction
	{
		//start creation of list 1 and 2
		std::vector<short>	list1; //usually eyetraker
		list1.reserve(myStimuli1.size());

		for (MarkerList_t::const_iterator it = myStimuli1.begin(); it != myStimuli1.end(); it++)
			list1.push_back(short(it->value));

		std::vector<short>	list2; //usually EEG
		list2.reserve(myStimuli2.size());

		for (MarkerList_t::const_iterator it = myStimuli2.begin(); it != myStimuli2.end(); it++)
			list2.push_back(short(it->value));
		//end list creation

		//find max value in eyetracker stream 
		int maxValueSourceStream = *std::max_element(list1.begin(), list1.end());

		//Construct patterns and find the starting and ending position of the section we want to remove
		int StartPosRemove = BufferOveflowSectionConstruct(list1, list2, pos, false, maxValueSourceStream) + 1;
		int EndPosRemove = BufferOveflowSectionConstruct(list1, list2, pos, true, maxValueSourceStream);

		//Remove triggers from selected section
		if (StartPosRemove != -1 && EndPosRemove != -1)
		{

			myEEGBufferOveflowStartOffset = (signed long)myStimuli1[StartPosRemove].offset;
			myEEGBufferOveflowEndOffset = (signed long)myStimuli1[EndPosRemove].offset;
			std::cout << "EEG bufferoverflow start offset eyetracker: " << myEEGBufferOveflowStartOffset << std::endl;
			std::cout << "EEG bufferoverflow end   offset eyetracker: " << myEEGBufferOveflowEndOffset << std::endl;

			int oldSize = myStimuli1.size();
			myStimuli1.erase(myStimuli1.begin() + StartPosRemove, myStimuli1.begin() + EndPosRemove);
			int newSize = myStimuli1.size();
			std::cout << "Eye stream corrected. Triggers removed: " << oldSize - newSize << std::endl;
		}
		else std::cout << "ERROR: Could not apply correction to buffer overflow!" << std::endl;
	}
}

//The EEG stream is the one that lost data, so we are removing the corresponding section of triggers in the Eye stream. But first this section must be calculated.
//Used to construct pattern1 and pattern2 from the EEG signal around the 'startposEEG' trigger.
//After that pattern 1 is searched in the Eye stream starting at position (startposEEG + p) in decreasing order at max 'region' triggers
//After that pattern 2 is searched in the Eye stream starting at position (startposEEG - p) in increasing order at max 'region' triggers
//The p parameter is used to overlap the starting point (startposEEG+-p) 
int TriggerMatching::BufferOveflowSectionConstruct(std::vector<short> source, std::vector<short> target, int startposEEG, bool DirectionIncreasing, int maxValueSourceStream)
{
	const int region = 60; 
	int n = 10; //length of pattern
	int p = 5; //move start point in eye stream

	int startPosEYE = startposEEG;

	if (DirectionIncreasing)
	{
		startposEEG = startposEEG + 1;
		startPosEYE = startPosEYE - p;

		std::vector<short> pattern;
		for (int i = startposEEG; i < (startposEEG + n); i++)
		{
			if (target[i] <= maxValueSourceStream)
				pattern.push_back(target[i]);
		}
		
		n = pattern.size(); //length is corrected because we do not take all numbers as pattern
		//debug
		//std::cout << "Pattern 2: ";
		//for (int i = 0; i < n; i++) std::cout << pattern[i] << " ";
		//std::cout << std::endl;
	    //end debug
		for (int i = startPosEYE; i < (startPosEYE + region); i++)
		{
			int correct = 0;

			for (int j = i, p = 0; j< (i + n); j++, p++)
			{

				if (pattern[p] == source[j])
					correct++;
				else
					break;
			}

			if (correct == n)
				return i;
		}
	}
	else
	{
		startposEEG = startposEEG - 1;
		startPosEYE = startPosEYE + p;

		std::vector<short> pattern;
		for (int i = startposEEG; i > (startposEEG - n); i--)
		{
			if (target[i] <= maxValueSourceStream)
				pattern.push_back(target[i]);
		}
		n = pattern.size(); //length is corrected because we do not take all numbers as pattern

		//debug
		//std::cout << "Pattern 1: ";
		//for (int i = 0; i < n; i++) std::cout << pattern[i] << " ";
		//std::cout << std::endl;
		//end debug

		for (int i = startPosEYE; i > (startPosEYE - region); i--)
		{
			int correct = 0;

			for (int j = i, p = 0; j > (i - n); j--, p++)
			{

				if (pattern[p] == source[j])
					correct++;
				else
					break;
			}

			if (correct == n)
				return i;
		}
	}
	return -1;
}

void TriggerMatching::WriteErrorHistograms(std::vector<int> &triggers, const std::vector<double> &d1)
{
	if (triggers.size() != d1.size())
		std::cout << "Warning: Inconsistency detected!";

	const int bins_size = 11;

	int bins[bins_size] = { -5, -4, -3, -2, -1, 0, +1, +2, +3, +4, +5 };

	int values[bins_size];

	for (int i = 0; i < bins_size; i++) values[i] = 0;

	//1. HistoGlobalError.txt
	std::ofstream	ofs(GazeEegEnv::LogFolder + std::string("HistoGlobalError.txt"), std::ios::out | std::ios::trunc);

	TriggerSyncStat histo(d1);
	histo.PrintHistogram(ofs);
	ofs << "Trigger count: " << histo.GetTriggersCount() << std::endl;
	ofs << "Mean: " << std::setprecision(4) << histo.GetMean() << std::endl;
	ofs << "Std: " << std::setprecision(4) << histo.GetStdev() << std::endl;
	
	ofs.close();

	//2. HistoPerTriggerError.txt
	ofs = std::ofstream(GazeEegEnv::LogFolder + std::string("HistoPerTriggerError.txt"), std::ios::out | std::ios::trunc);
	
	//get a list of unique triggers
	std::vector<int> triggersUnique(triggers);
	std::sort(triggersUnique.begin(), triggersUnique.end());
	std::vector<int>::iterator it = std::unique(triggersUnique.begin(), triggersUnique.end());
	triggersUnique.resize(distance(triggersUnique.begin(), it));

    //for each trigger make a histogram 

	for (int t = 0; t < triggersUnique.size(); t++)
	{
		ofs << "Histrogram for trigger '" << triggersUnique[t] << "'" << std::endl;
		
		histo = TriggerSyncStat(triggers,d1, triggersUnique[t]);
		histo.PrintHistogram(ofs);
		ofs << "Trigger count: " << histo.GetTriggersCount() << std::endl;
		if (histo.GetTriggersCount() > 1)
		{
			ofs << "Mean: " << std::setprecision(4) << histo.GetMean() << std::endl;
			ofs << "Std: " << std::setprecision(4) << histo.GetStdev() << std::endl;
		}
		ofs << std::endl;
	}

	ofs.close();	
}
