/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

#include "stdafx.h"

#include "GazeEegEnv.h"

#define SYNCHRO_QUALITY_FILE	"c:\\tmp\\GazeEegSynchroQualityGelu.txt"
#define SYNCHRO_ARTICLE_FILE	"c:\\tmp\\GazeEegSynchroArticleGelu.txt"

std::string GazeEegEnv::LogFolder;

bool GazeEegEnv::Quality::Process()
{
	if(!doIt)
		return true;

	bool exists = false;

	{
		std::ifstream infile(SYNCHRO_QUALITY_FILE);
		exists = infile.good();
	}

	std::ofstream ofs;
	if(exists)
		ofs.open(SYNCHRO_QUALITY_FILE, std::ios::out|std::ios::app);
	else
		ofs.open(SYNCHRO_QUALITY_FILE, std::ios::out|std::ios::trunc);

	ofs << srcFile << " "
		<< std::setprecision(3) << tmpET << " "
		<< std::setprecision(3) << tmpEEG << " "
		<< std::setprecision(3) << tmpSync << " "
		<< std::setprecision(std::numeric_limits<long double>::digits10 + 1) << r2 << " ";

	for(std::vector<int>::iterator it=histogram.begin(); it != histogram.end(); it++)
		ofs << std::setw(5) << *it << " ";
		
	ofs << std::endl;
	
	return true;
}

bool GazeEegEnv::Article::Process()
{
	if(!doIt)
		return true;

	bool exists = false;

	{
		std::ifstream infile(SYNCHRO_ARTICLE_FILE);
		exists = infile.good();
	}

	std::ofstream ofs;
	if(exists)
		ofs.open(SYNCHRO_ARTICLE_FILE, std::ios::out|std::ios::app);
	else
		ofs.open(SYNCHRO_ARTICLE_FILE, std::ios::out|std::ios::trunc);

	ofs << srcFile << " ";

	for(std::vector<int>::iterator it=histTtlEegEt.begin(); it != histTtlEegEt.end(); it++)
		ofs << std::setw(5) << *it << " ";
				
	ofs << std::endl;
	
	return true;
}

void GazeEegEnv::ProcessOutputMatFiles()
{
	std::string		path, name, ext;
	Parser::ParsePath(path, name, ext, gazeMatFileName);

	gazeMatFileName = outputPath + '\\' + name + '.' + ext;
	
	Parser::ParsePath(path, name, ext, eegMatFileName);

	eegMatFileName = outputPath + '\\' + name + '.' + ext;
}
