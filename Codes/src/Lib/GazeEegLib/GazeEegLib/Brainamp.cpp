/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

#include "StdAfx.h"

#include "Brainamp.h"
#include "AscRawData.h"
#include "File.h"
using GazeEeg::File;

Brainamp::Brainamp()
	: myEEG(0)
{
}

Brainamp::~Brainamp()
{
	delete myEEG;
}

bool Brainamp::IsValid(GazeEegEnv& gazeEegEnv, const std::string& srcFileName)
{
#if 0
	std::string	fileName(srcFileName);
	std::transform(fileName.begin(), fileName.end(), fileName.begin(), ::tolower);

	if(!gazeEegEnv.DoAes())
	{	bool b1 = fileName.find(".eeg") != std::string::npos;
		bool b2 = fileName.find(".vhdr") != std::string::npos;
		bool b3 = fileName.find(".vmrk") != std::string::npos;
		
		if(!(b1 || b2 || b3))
			return false;

		std::vector<std::string>	segments;
		Parser::ParsePath(segments, fileName);

		if(segments[2] == "ext")
		{	std::vector<std::string>	segments1;
			Parser::ParsePath(segments1, segments[1]);

			Parser::ParsePath(segments, srcFileName);
			
			std::string::size_type	pos = segments1[1].find('-');

			Parser::ParsePath(segments1, segments[1]);
			if(pos == std::string::npos)
				gazeEegEnv.eegFileName		= segments[0] + '\\' + segments1[1] + '.' + segments1[2];
			else
				gazeEegEnv.eegFileName		= segments[0] + '\\' + segments1[1].substr(0, pos) + '.' + segments1[2];
		}
		else
			gazeEegEnv.eegFileName			= srcFileName;
#endif
	gazeEegEnv.eegFileName	= srcFileName;
	if(!gazeEegEnv.DoAes())
		gazeEegEnv.eegMatFileName	= gazeEegEnv.eegFileName + ".mat";


	if(!File::Exists(gazeEegEnv.eegFileName))
	{	gazeEegEnv.error = gazeEegEnv.eegFileName;
		return false;
	}

	if(!File::Exists(Parser::ReplaceExtention(gazeEegEnv.eegFileName, "vhdr")))
	{	gazeEegEnv.error = Parser::ReplaceExtention(gazeEegEnv.eegFileName, "vhdr");
		return false;
	}
		
	if(!File::Exists(Parser::ReplaceExtention(gazeEegEnv.eegFileName, "vmrk")))
	{	gazeEegEnv.error = Parser::ReplaceExtention(gazeEegEnv.eegFileName, "vmrk");
		return false;
	}

	return true;
}

Parser::binaryFormat_t Brainamp::GetBinaryFormat(const std::string& fileName)
{
	return myVhdr.GetBinaryFormat(Parser::ReplaceExtention(fileName, "vhdr"));
}

bool Brainamp::ProcessDataSegments(const bool doDataCorrection)
{
	if(!myVmrk.ProcessDataSegments(myVhdr, myEEG->GetDataSize()))
		return false;

	if(doDataCorrection && !myEEG->ProcessDataSegments(myVmrk, myVhdr))
		return false;

	return true;
}

bool Brainamp::SaveMatFile(const GazeEegEnv& gazeEegEnv)
{
	MSendDebug1("Saving     : ", gazeEegEnv.eegMatFileName);

	if(!myEEG->SaveRawData())
		return MSendError("Brainamp::SaveMatFile::myEEG.SaveRawData");

	MatComponents	allComponents;

	matvar_t*	dataComponents = GetDataComponents();
	if(!dataComponents)
		return MSendError("Brainamp::SaveMatFile dataComponents = 0");

	matvar_t*	eventComponents = GetEventComponents(gazeEegEnv.eventFileName);
	if(!eventComponents)
		return MSendError("Brainamp::SaveMatFile eventComponents = 0");
	
	allComponents.push_back(dataComponents);
	allComponents.push_back(eventComponents);
	matvar_t* mainStruct = allComponents.GetStructure("EegAcq");
	
	mat_t* matFile	= Mat_Create(gazeEegEnv.eegMatFileName.c_str(), 0);
	if(!matFile)
		return MSendError1("Brainamp::SaveMatFile can not create MAT file", gazeEegEnv.eegMatFileName);

//DIG	int ret = Mat_VarWrite(matFile, mainStruct, gazeEegEnv.doCompression ? COMPRESSION_ZLIB : 0);
	int ret = Mat_VarWrite(matFile, mainStruct, 0);
	Mat_VarFree(mainStruct);
	Mat_Close(matFile);
	
	return ret == 0;
}

bool Brainamp::SaveMatFile(AscRawData& eyelink, const std::string& ascFileName)
{
	std::string matFileName = Parser::ReplaceExtention(ascFileName, "asc.eeg.mat");

	MSendDebug1("Saving : ", matFileName);

	MatComponents	allComponents;

	// ASC header section
	{	MatComponents etComponents;
		
		etComponents.push_back(eyelink.Header().GetComponents());
		etComponents.push_back(eyelink.GetDataParamsComponentsForSynchro());
		etComponents.push_back(eyelink.GetDataComponentsForSynchro());
		
		allComponents.push_back(etComponents.GetStructure("Eyetracker"));
	}

	matvar_t*	dataComponents = GetDataComponents(&eyelink);
	if(!dataComponents)
		return MSendError("Brainamp::SaveMatFile dataComponents = 0");
	allComponents.push_back(dataComponents);

	matvar_t*	eventComponents = GetEventComponents(eyelink.mySynchronization.eventFile, &eyelink);
	if(!eventComponents)
		return MSendError("Brainamp::SaveMatFile eventComponents = 0");	
	allComponents.push_back(eventComponents);
	
	matvar_t* mainStruct = allComponents.GetStructure("EegAcq");
	
	mat_t* matFile	= Mat_Create(matFileName.c_str(), 0);
	if(!matFile)
		return MSendError1("Brainamp::SaveMatFile can not create MAT file", matFileName);

//DIG	int ret = Mat_VarWrite(matFile, mainStruct, gazeEegEnv.doCompression ? COMPRESSION_ZLIB : 0);
	int ret = Mat_VarWrite(matFile, mainStruct, 0);
	Mat_VarFree(mainStruct);
	Mat_Close(matFile);
	
	return ret == 0;
}

matvar_t* Brainamp::GetDataComponents(AscRawData* eyelink /*= 0*/)
{
	MatComponents matComponents;

	if(eyelink)
	{	MatComponents synchroComponents;

		// reference info
		synchroComponents.AddVariable("gazeReferenceAsc",	MAT_C_UINT32,	eyelink->mySynchronization.matParams.gazeReferenceAsc);			
		synchroComponents.AddVariable("gazeReference",		MAT_C_UINT32,	eyelink->mySynchronization.matParams.gazeReference);

		// sampling rate info			
		synchroComponents.AddVariable("trueSamplingRate",	MAT_C_UINT32,	eyelink->mySynchronization.matParams.trueSamplingRate);			
		synchroComponents.AddVariable("eegSamplingRate",	MAT_C_UINT32,	eyelink->mySynchronization.matParams.eegSamplingRate);			
		synchroComponents.AddVariable("gazeSamplingRate",	MAT_C_UINT32,	eyelink->mySynchronization.matParams.gazeSamplingRate);
			
		synchroComponents.AddVariable("eegOnGazeSamplingRatio",	MAT_C_DOUBLE,	eyelink->mySynchronization.matParams.eegOnGazeSamplingRatio);

		// channels info			
		synchroComponents.AddVariable("nbOfChannels",		MAT_C_UINT32,	eyelink->mySynchronization.matParams.nbOfChannels);			
		synchroComponents.AddVariable("eegNbChannels",		MAT_C_UINT32,	eyelink->mySynchronization.matParams.eegNbChannels);			
		synchroComponents.AddVariable("gazeNbChannels",		MAT_C_UINT32,	eyelink->mySynchronization.matParams.gazeNbChannels);
		
		// samples info			
		synchroComponents.AddVariable("nbOfSamples",		MAT_C_UINT32,	eyelink->mySynchronization.matParams.nbOfSamples);			
		synchroComponents.AddVariable("eegNbSamples",		MAT_C_UINT32,	eyelink->mySynchronization.matParams.eegNbSamples);			
		synchroComponents.AddVariable("gazeNbSamples",		MAT_C_UINT32,	eyelink->mySynchronization.matParams.gazeNbSamples);

		// segments info			
		synchroComponents.AddVariable("nbOfSegments",		MAT_C_UINT32,	eyelink->mySynchronization.matParams.nbOfSegments);

		// triggers info			
		synchroComponents.AddVariable("nbOfTriggers",		MAT_C_UINT32,	eyelink->mySynchronization.matParams.nbOfTriggers);			
		synchroComponents.AddVariable("nbOfExtraTriggers",	MAT_C_UINT32,	eyelink->mySynchronization.matParams.nbOfExtraTriggers);			
		synchroComponents.AddVariable("firstTrigger",		MAT_C_UINT32,	eyelink->mySynchronization.matParams.firstTrigger);			
		synchroComponents.AddVariable("lastTrigger",		MAT_C_UINT32,	eyelink->mySynchronization.matParams.lastTrigger);

		// triggers table
		synchroComponents.AddVariable("triggerValues",		MAT_C_UINT16,	eyelink->mySynchronization.matParams.triggerSynchro.value);			
		synchroComponents.AddVariable("triggerGazeOffset",	MAT_C_UINT32,	eyelink->mySynchronization.matParams.triggerSynchro.gazeOffset);			
		synchroComponents.AddVariable("triggerEegOffset",	MAT_C_UINT32,	eyelink->mySynchronization.matParams.triggerSynchro.eegOffset);			
		synchroComponents.AddVariable("diffs",				MAT_C_INT16,	eyelink->mySynchronization.matParams.triggerSynchro.diffs);			
		synchroComponents.AddVariable("gazeFirst",			MAT_C_UINT32,	eyelink->mySynchronization.matParams.triggerSynchro.gazeFirst);
		synchroComponents.AddVariable("eegFirst",			MAT_C_UINT32,	eyelink->mySynchronization.matParams.triggerSynchro.eegFirst);
		synchroComponents.AddVariable("R2",					MAT_C_DOUBLE,	eyelink->mySynchronization.matParams.triggerSynchro.R2);

		matvar_t* pStruct = synchroComponents.GetStructure("Synchro");
		matComponents.push_back(pStruct);
	}

	{
		MatComponents flagComponents;

		flagComponents.AddVariable("tInt16",		MAT_C_UINT16,	uint16(Parser::int16_type));
		flagComponents.AddVariable("tFloat32",		MAT_C_UINT16,	uint16(Parser::float_type));
		flagComponents.AddVariable("tFloat64",		MAT_C_UINT16,	uint16(Parser::double_type));

		matvar_t* pStruct = flagComponents.GetStructure("BinaryFormat");
		matComponents.push_back(pStruct);
	}

	{
		MatComponents flagComponents;

		flagComponents.AddVariable("MULTIPLEXED",	MAT_C_UINT16,	uint16(BrainampVhdr::DO_MULTIPLEXED));
		flagComponents.AddVariable("VECTORIZED",	MAT_C_UINT16,	uint16(BrainampVhdr::DO_VECTORIZED));

		matvar_t* pStruct = flagComponents.GetStructure("DataOrientation");
		matComponents.push_back(pStruct);
	}

	{
		MatComponents flagComponents;

		flagComponents.AddVariable("FREQUENCYDOMAIN",	MAT_C_UINT16,	uint16(0));
		flagComponents.AddVariable("TIMEDOMAIN",		MAT_C_UINT16,	uint16(1));

		matvar_t* pStruct = flagComponents.GetStructure("DataType");
		matComponents.push_back(pStruct);
	}

	{
		MatComponents flagComponents;

		flagComponents.AddVariable("NOSEGMENTED",	MAT_C_UINT16,	uint16(0));
		flagComponents.AddVariable("MARKERBASED",	MAT_C_UINT16,	uint16(1));

		matvar_t* pStruct = flagComponents.GetStructure("SegmentationType");
		matComponents.push_back(pStruct);
	}

	matComponents.push_back(myVhdr.GetParamsComponents(myEEG->GetNan(), eyelink));

	return matComponents.GetStructure("Data");
}

matvar_t* Brainamp::GetEventComponents(const std::string& logFileName, AscRawData* eyelink /*= 0*/)
{
	MatComponents eventComponents;
		
	StimulationMap stimulationMap;
	
	bool okLogFileName = stimulationMap.Read(logFileName);

	if(okLogFileName)
	{	MatComponents typeComponents;

		stimulationMap.AddComponents(typeComponents);

		matvar_t* pStruct = typeComponents.GetStructure("EventTypes");
		eventComponents.push_back(pStruct);
	}

	if(eyelink)
		eyelink->GetEventComponentsForSynchro(eventComponents);
	else
	{	stimulationMap.clear();

		// event section
		AcqPhases			acqPhases;
		Records				records;
		Sequences			sequences;
		Displayables		displayables;
		Frames				frames;
		Transitions			transitions;
		FixationSaccades	fixSaccLeft;
		FixationSaccades	fixSaccRight;
		Blinks				blinksLeft;
		Blinks				blinksRight;
		Triggers			eventStims;
		Triggers 			segments;
		Triggers 			triggers;
		Triggers 			allEvents;

		MarkerList_t&		vmrkMarkets = myVmrk.Markers();
		for(MarkerList_t::const_iterator it=vmrkMarkets.begin(); it != vmrkMarkets.end(); it++)
		{	allEvents.Add(it->value, uint32(it->offset), it->duration, it->fseek);
			
			if(it->value < CMarker::MARKER_SEGMENT_VAL)
			{	triggers.Add(it->value, uint32(it->offset), it->duration, it->fseek);
				
				std::ostringstream oss;				
				oss << "trigger";
				oss.width(3);
				oss.fill('0');
				oss << it->value;
				stimulationMap.Add(it->value, oss.str());
				
				continue;
			}
			else if(it->value == CMarker::MARKER_SEGMENT_VAL)
			{	segments.Add(CMarker::MARKER_SEGMENT_VAL, uint32(it->offset), it->duration, it->fseek);
				stimulationMap.Add(CMarker::MARKER_SEGMENT_VAL, "segment256");
				continue;
			}
			else if(it->value >= eventStim)
			{	eventStims.Add(it->value, uint32(it->offset), it->duration, it->fseek);
				
				std::ostringstream oss;				
				oss << "EventStim";
				oss.width(4);
				oss.fill('0');
				oss << it->value;
				stimulationMap.Add(it->value, oss.str());
				
				continue;
			}

			stimulationMap.Add(it->value, AscRawData::theEventTypeMap[it->value]);
			
			switch(it->value)
			{	case phaseLearning:
				case phaseTesting:
					acqPhases.Add(it->value, uint32(it->offset), uint32(it->duration), it->fseek);
					break;
				case recordStep:
					records.Add(it->value, uint32(it->offset), uint32(it->duration), it->fseek);
					break;
				case flowSequence:
					sequences.Add(it->value, uint32(it->offset), uint32(it->duration), it->fseek, 0, "");
					break;
				case displayableSpecial:
				case displayableImage:
				case displayableText:
				case displayableVideoSound:
				case displayableVideoNoSound:
					displayables.Add(it->value, uint32(it->offset), uint32(it->duration), it->fseek,
									0, 0, 0, 0, 0, 0, 0, 0, "", "");
					break;
				case displayableFrame:
						frames.Add(it->value, uint32(it->offset), uint32(it->duration), it->fseek);
					break;
					case transitionTime:
					case transitionMouse:
					case transitionKeyboard:
					case transitionGaze:
					case transitionSSacc:
					case transitionESacc:
					case transitionSFix:
					case transitionEFix:
					case transitionSpecial:
						transitions.Add(it->value, uint32(it->offset), uint32(it->duration), it->fseek,
										"", 0, 0, 0, 0, 0, 0);
						break;
					case flowFixationLeft:
					case flowSaccadeLeft:
						fixSaccLeft.Add(it->value, uint32(it->offset), uint32(it->duration), it->fseek,
										0, 0, 0, 0, 0, 0, 0);
						break;
					case flowFixationRight:
					case flowSaccadeRight:
						fixSaccRight.Add(it->value, uint32(it->offset), uint32(it->duration), it->fseek,
										0, 0, 0, 0,0 ,0, 0);
						break;
					case flowBlinkLeft:
						blinksLeft.Add(	it->value, uint32(it->offset), uint32(it->duration), it->fseek);
						break;
					case flowBlinkRight:
						blinksRight.Add(	it->value, uint32(it->offset), uint32(it->duration), it->fseek);
						break;
		}	}

		if(!okLogFileName)
		{	MatComponents typeComponents;

			stimulationMap.AddComponents(typeComponents);

			matvar_t* pStruct = typeComponents.GetStructure("EventTypes");
			eventComponents.push_back(pStruct);
			
			stimulationMap.Write(logFileName);
		}

		if(acqPhases.IsValid())
		{	matvar_t* pStruct = acqPhases.GetComponents("AcqPhases", false);
			eventComponents.push_back(pStruct);
		}
		if(records.IsValid())
		{	matvar_t* pStruct = records.GetComponents("RecordSteps", false);
			eventComponents.push_back(pStruct);
		}
		if(sequences.IsValid())
		{	Parser::Transpose(sequences.name, NameSize);

			matvar_t* pStruct = sequences.GetComponents("Sequences", false);
			eventComponents.push_back(pStruct);
		}
		if(displayables.IsValid())
		{	Parser::Transpose(displayables.name,			NameSize);
			Parser::Transpose(displayables.relativePath,	PathSize);

			matvar_t* pStruct = displayables.GetComponents("Displayables", false);
			eventComponents.push_back(pStruct);
		}
		if(frames.IsValid())
		{	matvar_t* pStruct = frames.GetComponents("Frames", false);
			eventComponents.push_back(pStruct);
		}
		if(transitions.IsValid())
		{	Parser::Transpose(transitions.name, NameSize);
			
			matvar_t* pStruct = transitions.GetComponents("Transitions", false);
			eventComponents.push_back(pStruct);
		}
		if(fixSaccLeft.IsValid())
		{	matvar_t* pStruct = fixSaccLeft.GetComponents("FixationSaccadesLeft", false);
			eventComponents.push_back(pStruct);
		}
		if(fixSaccRight.IsValid())
		{	matvar_t* pStruct = fixSaccRight.GetComponents("FixationSaccadesRight", false);
			eventComponents.push_back(pStruct);
		}
		if(blinksLeft.IsValid())
		{	matvar_t* pStruct = blinksLeft.GetComponents("BlinksLeft", false);
			eventComponents.push_back(pStruct);
		}
		if(blinksRight.IsValid())
		{	matvar_t* pStruct = blinksRight.GetComponents("BlinksRight", false);
			eventComponents.push_back(pStruct);
		}
		if(eventStims.IsValid())
		{	matvar_t* pStruct = eventStims.GetComponents("EventStims", false);
			eventComponents.push_back(pStruct);
		}
		if(segments.IsValid())
		{	matvar_t* pStruct = segments.GetComponents("Segments", false);
			eventComponents.push_back(pStruct);
		}
		if(triggers.IsValid())
		{	matvar_t* pStruct = triggers.GetComponents("Triggers", false);
			eventComponents.push_back(pStruct);
		}
		if(allEvents.IsValid())
		{	matvar_t* pStruct = allEvents.GetComponents("AllEvents", false);
			eventComponents.push_back(pStruct);
	}	}

	return eventComponents.GetStructure("Events");
}

//Used by the application AddEventStim
bool Brainamp::SaveEegLab(const GazeEegEnv& gazeEegEnv, const std::string& resultFile)
{
	//calls the same SaveEegLab virtual method for all 3 classes:

	if(!myVhdr.SaveEegLab(gazeEegEnv.eegFileName, resultFile))
		return MSendError("Brainamp::myVhdr.SaveEegLab");
	
	if(!myVmrk.SaveEegLab(gazeEegEnv.eegFileName, resultFile))
		return MSendError("Brainamp::myVmrk.SaveEegLab");

	if(!myEEG->SaveEegLab(resultFile))
		return MSendError("Brainamp::myEEG.SaveEegLab");

	StimulationMap		eventDictAll;
	MarkerList_t&		markers = myVmrk.Markers();
	for(MarkerList_t::iterator it=markers.begin(); it != markers.end(); it++)
	{	uint16		value	= it->value;
		if(value == CMarker::MARKER_OTHER_VAL)
			continue;

		std::string name	= gazeEegEnv.aes.it->eventDictAll[value].first;

		eventDictAll.Add(value, name);
	}

	return eventDictAll.Write(resultFile + MEventFile);
}
