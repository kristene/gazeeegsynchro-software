/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

#include "StdAfx.h"

#include "FixationSaccades.h"
#include <iostream>

FixationSaccades::FixationSaccades(const size_t size /*= 0*/)
	: Events(size)
	, startPos(2*size)
	, endPos(2*size)
	, averagePupilSizeFixation(size)
	, amplitudeSaccade(size)
	, peakVelocitySaccade(size)
{
}

FixationSaccades::~FixationSaccades()
{
}

matvar_t* FixationSaccades::GetComponents(char* structName, const bool conserve)
{
	MatComponents matComponents;
	Events::GetComponents(matComponents, conserve);
	
	matComponents.AddVariable("startPos",	value.size(), MAT_C_DOUBLE, startPos,	conserve);
	matComponents.AddVariable("endPos",		value.size(), MAT_C_DOUBLE, endPos,		conserve);

	//Added later by Anton
	if (averagePupilSizeFixation.size() != 0)
		matComponents.AddVariable("averagePupilSizeFixation", averagePupilSizeFixation.size(), MAT_C_DOUBLE, averagePupilSizeFixation, conserve);

	if (amplitudeSaccade.size() != 0)
		matComponents.AddVariable("amplitudeSaccade",		  amplitudeSaccade.size(),         MAT_C_DOUBLE, amplitudeSaccade,         conserve);

	if (peakVelocitySaccade.size() != 0)
		matComponents.AddVariable("peakVelocitySaccade",      peakVelocitySaccade.size(),      MAT_C_DOUBLE, peakVelocitySaccade,      conserve);

	return matComponents.GetStructure(structName);
}

void FixationSaccades::Add(	const uint16 value, const uint32 time, const uint32 duration, const uint32 fseek, 
							const double startPosX, const double endPosX, 
							const double startPosY, const double endPosY,
							const double averagePupilSizeFixation, const double amplitudeSaccade, const double peakVelocitySaccade
                          )
{
	Events::Add(value, time, duration, fseek);
	
	this->startPos.push_back(startPosX);
	this->startPos.push_back(startPosY);
	
	this->endPos.push_back(endPosX);
	this->endPos.push_back(endPosY);

	//added later by Anton
	this->averagePupilSizeFixation.push_back(averagePupilSizeFixation);
	this->amplitudeSaccade.push_back(amplitudeSaccade);
	this->peakVelocitySaccade.push_back(peakVelocitySaccade);
}

