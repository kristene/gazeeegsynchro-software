/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

#include "StdAfx.h"

#include "BrainampVhdr.h"
#include "AscRawData.h"
#include "Parser.h"
#include "Event.h"
#include "MemoryFile.h"

BrainampVhdr::BrainampVhdr()
	: nbSamples(0)
	, codePage(CP_UNCKNOWN)				//[Common Infos]
	, dataFormat(DF_UNCKNOWN)			
	, dataOrientation(DO_UNCKNOWN)
	, dataType(DT_UNCKNOWN)
	, nbChannels(0)
	, dataPoints(0)
	, usecPerSample(0)
	, segmentationType(ST_NOSEGMENTED)
	, binaryFormat(Parser::int16_type)	//[Binary Infos]
{
}

BrainampVhdr::~BrainampVhdr()
{
}

Parser::binaryFormat_t BrainampVhdr::GetBinaryFormat(const std::string& fileName)
{
	// [Binary Infos]
	binaryFormat			= Parser::unknown_type;
	std::string binFormat	= ::MyGetPrivateProfileString("Binary Infos", "BinaryFormat", "INT_16", fileName);
	if(binFormat.find("INT_16") == 0)
		binaryFormat		= Parser::int16_type;
	else if(binFormat.find("IEEE_FLOAT_32") == 0)
		binaryFormat		= Parser::float_type;
	else if(binFormat.find("IEEE_FLOAT_64") == 0)
		binaryFormat		= Parser::double_type;

	return binaryFormat;
}

bool BrainampVhdr::Process(const std::string& fileName)
{
	MSendDebug1("Processing : ", fileName);
	
	// [Common Infos]

	std::string str = ::MyGetPrivateProfileString("Common Infos", "Codepage", "CP_UNCKNOWN", fileName);
	if(str == "UTF-8")
		codePage	= CP_UTF_8;

	str = ::MyGetPrivateProfileString("Common Infos", "DataFormat", "BINARY", fileName);
	if(str == "BINARY")
		dataFormat	= DF_BINARY;

	str = ::MyGetPrivateProfileString("Common Infos", "DataOrientation", "MULTIPLEXED", fileName);
	if (str == "MULTIPLEXED")
	{
		dataOrientation = DO_MULTIPLEXED;
		//std::cout << "EEG file is multiplexed\r\n";
	}
	else if (str == "VECTORIZED")
	{
		dataOrientation = DO_VECTORIZED;
		//std::cout << "EEG file is vectorized\r\n";
		return MSendError("Only Brainamp eeg in format MULTIPLEXED is supported. The provided EEG file is in format VECTORIZED.");
	}
	str = ::MyGetPrivateProfileString("Common Infos", "DataType", "FREQUENCYDOMAIN", fileName);
	if(str == "FREQUENCYDOMAIN")
		dataType	= DT_FREQUENCYDOMAIN;
	else if(str == "TIMEDOMAIN")
		dataType	= DT_TIMEDOMAIN;
	
	nbChannels		= uint16(::MyGetPrivateProfileInt("Common Infos", "NumberOfChannels", 0, fileName));
	if(nbChannels < 1)
		return MSendError("BrainampEEG::Process nbChannels < 1");
	
	dataPoints		= uint32(::MyGetPrivateProfileInt("Common Infos", "DataPoints", 0, fileName));
	
	usecPerSample	= ::MyGetPrivateProfileDouble("Common Infos", "SamplingInterval", 0, fileName);

	str = ::MyGetPrivateProfileString("Common Infos", "SegmentationType", "ST_NOSEGMENTED", fileName);
	if(str == "MARKERBASED")
		segmentationType	= ST_MARKERBASED;
	
	segmentDataPoints	= uint32(::MyGetPrivateProfileInt("Common Infos", "SegmentDataPoints", 0, fileName));
	
	// [Binary Infos]
	binaryFormat	= GetBinaryFormat(fileName);

	// [Channel Infos]
	nameChannels.reserve(nbChannels);
	gainChannels.reserve(nbChannels);

	for(int ii=1; ii <= nbChannels; ii++) 
	{	std::ostringstream oss;
		oss << "Ch" << ii;

		std::string retrieve = ::MyGetPrivateProfileString("Channel Infos", (char*) oss.str().c_str(), "", fileName);
	
		std::vector<char*>	strings;
		Parser::ParseChar(strings, (char*) retrieve.c_str(), retrieve.length(), ',');

		nameChannels.push_back(*strings.begin());
		if(strings.size() < 2)
			gainChannels.push_back(1.0);
		else
			gainChannels.push_back(double(atof(*(strings.rbegin()+1))));
	}

	return true;
}

matvar_t* BrainampVhdr::GetParamsComponents(const Parser::double_t valNaN, AscRawData* eyelink /*= 0*/)
{
	MatComponents matComponents;

	matComponents.AddVariable("binaryFormat",		MAT_C_UINT16,	binaryFormat);
	matComponents.AddVariable("dataOrientation",	MAT_C_UINT16,	dataOrientation);
	matComponents.AddVariable("dataType",			MAT_C_UINT16,	dataType);
	matComponents.AddVariable("segmentationType",	MAT_C_UINT16,	segmentationType);
	matComponents.AddVariable("segmentDataPoints",	MAT_C_UINT32,	segmentDataPoints);
	
	if(eyelink)
	{	matComponents.AddVariable("samplingRate",		MAT_C_DOUBLE,	eyelink->mySynchronization.samplingRate);
		matComponents.AddVariable("nbOfChannels",		MAT_C_UINT32,	eyelink->mySynchronization.nbOfChannels);
		matComponents.AddVariable("nbOfSamples",		MAT_C_UINT32,	eyelink->mySynchronization.nbOfSamples);
	}
	else
	{	matComponents.AddVariable("samplingRate",		MAT_C_DOUBLE,	::round(1000000.0/double(usecPerSample)));
		matComponents.AddVariable("nbOfChannels",		MAT_C_UINT32,	uint32(nbChannels));
		matComponents.AddVariable("nbOfSamples",		MAT_C_UINT32,	nbSamples);
	}

	switch(binaryFormat)
	{	case Parser::int16_type:
			matComponents.AddVariable("valNaN",		MAT_C_INT16,	Parser::int16_t(valNaN));
			break;
		case Parser::float_type:
			matComponents.AddVariable("valNaN",		MAT_C_SINGLE,	Parser::float_t(valNaN));
			break;
		case Parser::double_type:
			matComponents.AddVariable("valNaN",		MAT_C_DOUBLE,	Parser::double_t(valNaN));
			break;
	}

	GetChannelNamesComponents(matComponents, eyelink);
	GetChannelGainsComponents(matComponents, eyelink);

	return matComponents.GetStructure("Params");
}

void BrainampVhdr::GetChannelNamesComponents(MatComponents& matComponents, AscRawData* eyelink /*= 0*/) const
{
	size_t nbChannels = eyelink ? eyelink->mySynchronization.nbOfChannels : nameChannels.size();
	std::vector<char>	channelNames(nbChannels*ChannelNameSize, 0);

	size_t ii;
	for(ii=0; ii < nameChannels.size(); ii++)
		memcpy(&channelNames[ii*ChannelNameSize], nameChannels[ii].c_str(), nameChannels[ii].length());

	if(eyelink)
	{	for(size_t jj=0; ii < nbChannels; ii++, jj++)
		{	std::string channelName = eyelink->ChannelNames()[jj];
			memcpy(&channelNames[ii*ChannelNameSize], channelName.c_str(), channelName.length());
	}	}

	Parser::Transpose(channelNames, ChannelNameSize);

	matComponents.AddVariable("channelNames",	ChannelNameSize,	MAT_C_CHAR,  channelNames,	false);
}

void BrainampVhdr::GetChannelGainsComponents(MatComponents& matComponents, AscRawData* eyelink /*= 0*/)
{
	if(eyelink)
	{	while(gainChannels.size() < eyelink->mySynchronization.nbOfChannels)
			gainChannels.push_back(0.5);
	}

	matComponents.AddVariable("channelGains", MAT_C_DOUBLE, gainChannels);
}

// https://github.com/mateusjoffily/EDA/blob/master/vhdr2mat.m
// saves the new .VHDR file
bool BrainampVhdr::SaveGazeEeg(const std::string& src, const std::string& dest, const std::vector<std::string>& extraChannelNames, const double samplingRate, const int nbOfSamples)
{
	std::string srcFile		= Parser::ReplaceExtention(src,  "vhdr");
	std::string destFile	= dest + ".vhdr";
	
	MSendDebug1("Saving : ", destFile);

	MemoryFile	memFile;
	if(!memFile.Read(srcFile))
		return MSendError("BrainampVhdr::SaveGazeEeg::MemoryFile::Read");

	memFile.ReplaceBetween(Parser::GetFileName(destFile) + ".eeg", "DataFile=");
	memFile.ReplaceBetween(Parser::GetFileName(destFile) + ".vmrk", "MarkerFile=");

	int	nbOfChannels = int(nbChannels + extraChannelNames.size());

	//With "ReplaceBetween" only exisitng parameters are updated!
	//Warning: The order of execution below must match the order of parameters in the .vhdr file!

	std::string samplingIntervalStr;
	{
		std::ostringstream oss;
		oss << nbOfChannels;
		memFile.ReplaceBetween(oss.str(), "NumberOfChannels=");
	}

	{
		std::ostringstream oss;
		oss << nbOfSamples; //after interpolation
		bool isReplaced = memFile.ReplaceBetween(oss.str(), "DataPoints="); //use DataPoints= only if it already existed in the input file
		if (!isReplaced)
			memFile.AddNewAfterPrevious(oss.str(), "DataPoints=", "NumberOfChannels="); 
	}

	{
		std::ostringstream oss;
		//samplingInterval	= int(1000000.0/samplingRate); //truncating
		//oss << samplingInterval;
		samplingIntervalStr = std::to_string(1000000.0 / samplingRate); //default precision of 6 digits 
		oss << samplingIntervalStr;
		memFile.ReplaceBetween(oss.str(), "SamplingInterval=");
	}
	
	{
		std::ostringstream oss1;
		oss1 << "Ch" << nbChannels << '=';

		std::ostringstream oss;
		size_t ii = nbChannels + 1;
		for(std::vector<std::string>::const_iterator it=extraChannelNames.begin(); it != extraChannelNames.end(); it++, ii++)
			oss << "Ch" << ii << '=' << *it << ",,1.0," << char(0xC2) << char(0xB5) << "V\r\n";
		
		memFile.AddAfter(oss.str(), oss1.str());
	}

	{   //extra "Number of channels:" with ":" instead of "=", proably not used
		std::ostringstream oss;
		oss << ' ' << nbOfChannels;
		memFile.ReplaceBetween(oss.str(), "Number of channels:");
	}

	{
		std::ostringstream oss;
		oss << ' ' << int(samplingRate);
		memFile.ReplaceBetween(oss.str(), "Sampling Rate [Hz]:");
	}

	{
		std::ostringstream oss;
		//oss << ' ' << int(samplingInterval);
		oss << ' ' << samplingIntervalStr;
		memFile.ReplaceBetween(oss.str(), "S]:");
	}
	
	{
		std::ostringstream oss1;
		oss1 << nbChannels;

		std::ostringstream oss;
		size_t ii = nbChannels + 1;
		for (std::vector<std::string>::const_iterator it = extraChannelNames.begin(); it != extraChannelNames.end(); it++, ii++)
			//oss << ii << "    " << *it << "       " << ii << "               1.0," << char(0xC2) << char(0xB5) << "V             10             1000              Off                0\r\n";
			oss << "Ch" << ii << '=' << "0,0,0\r\n"; //corrected by Anton for EEGLab import

		memFile.AddAfter(oss.str(), oss1.str());
	}

	{
		std::ostringstream oss;
		for (std::vector<std::string>::const_iterator it = extraChannelNames.begin(); it != extraChannelNames.end(); it++)
			oss << *it << ":      Out of Range!\r\n";

		memFile.AddAfter(oss.str(), "Impedance [kOhm]", ":\r\n");
	}

	return memFile.Write(destFile);
}

//saves a .VHDR file
bool BrainampVhdr::SaveEegLab(const std::string& eegSrcFileName, const std::string& eegDestFileName)
{
	std::string srcFile		= Parser::ReplaceExtention(eegSrcFileName,  "vhdr");
	std::string destFile	= Parser::ReplaceExtention(eegDestFileName, "vhdr");
	
	MSendDebug1("Saving : ", destFile);

	MemoryFile	memFile;
	if(!memFile.Read(srcFile))
		return MSendError("BrainampVhdr::SaveGazeEeg::MemoryFile::Read");

	std::string		pathS, nameS, extS;
	Parser::ParsePath(pathS, nameS, extS, eegSrcFileName);

	std::string		path, name, ext;
	Parser::ParsePath(path, name, ext, destFile);
	
	memFile.ReplaceBetween(name + '.' + extS,	"DataFile=");
	memFile.ReplaceBetween(name + ".vmrk",		"MarkerFile=");

	return memFile.Write(destFile);
}
