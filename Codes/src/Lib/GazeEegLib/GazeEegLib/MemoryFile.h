/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

#pragma once

#include "File.h"

class MemoryFile : public std::string
{
public:
	MemoryFile();
	~MemoryFile();

	bool	Read(const std::string& srcFileName);
	bool	Write(const std::string& destFileName);
	bool	ReplaceBetween(const std::string& targget, const std::string& header, const std::string& footer = "\r\n");
	bool	GetString(std::string& targget, const std::string& header, const std::string& footer = "\r\n");
	bool	AddAfter(const std::string& targget, const std::string& header, const std::string& footer = "\r\n");
	bool    AddNewAfterPrevious(const std::string& value, const std::string& newHeader, const std::string& previousHeader);
	bool	EraseAfter(const std::string& header, const std::string& footer = "\r\n");
	bool	EraseBefore(const std::string& header);
	template <class type> bool	GetValue(type& value, const std::string& fieldName, const std::string separator = "=")
	{
		std::string valueStr;
		if(!GetString(valueStr, fieldName))
			return false;

		std::string::size_type pos = valueStr.find(separator);
		if(pos != std::string::npos)
			valueStr = valueStr.substr(pos + separator.length());

		std::istringstream iss(valueStr);
		iss >> value;

		return true;
	}
	inline std::string& Ltrim()
	{
        erase(begin(), std::find_if(begin(), end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
        return *this;
	}
	inline std::string& Rtrim()
	{
        erase(std::find_if(rbegin(), rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), end());
        return *this;
	}
	inline std::string& Trim()
	{
        Ltrim();
		return Rtrim();
	}
private:
	std::string::size_type SkipSequence(const std::string::size_type pos, const std::string& sequence);

private:
	std::string::size_type	myStartPos;
};
