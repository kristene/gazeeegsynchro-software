/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

/*!	\file	Displayable.h
 	\author	Gelu Ionescu.
 
 	\brief	Generic \a Displayable support
*/
#pragma once

#include "Event.h"

/*!	\class	Displayable
 
 	\brief	Support for Displayable events
 */
 
class DisplayableNum : public Event
{
public:
	//! public constructors
	/*! \param type event type
	 *  \param startTime start timestamp
	 *  \param name displayable name
	 *  \param relativePath relative path of the displayable
	 *  \param nbFrames number of frame if the displayable is a video
	 *  \param frameDuration frame duration if the displayable is a video
	 *  \param leftRect, topRect, rightRect, bottomRect the video position on the screen
	 *
	 *	\see eventType_t for event types
	 */
	DisplayableNum(const uint16 value, const uint32 startTime);
	DisplayableNum(const uint16 value, const uint32 startTime, const uint16 nbFrames, const uint16 frameDuration, const uint16 leftRect, const uint16 topRect, const uint16 rightRect, const uint16 bottomRect);
	DisplayableNum(const uint32 startTime);
	virtual ~DisplayableNum();
		

	//! displayable section
	uint16			nbFrames;		//!< number of frames (1 if not video)
	uint16			playedFrames;	//!< number of playeble frames for videos (sometime some frames are not displayed)
	uint16			firstFrame;		//!< first frame for videos (sometime some frames are not displayed)
	uint16			frameDuration;	//!< frame duration for videos 
	uint16			leftRect;		//!< displayable rectangle 
	uint16			topRect;
	uint16			rightRect;
	uint16			bottomRect;

	//! transition section
	//! \see CTransition for definitions
	uint16			tType;
	uint16			tTypeEnd;
	uint32			tTimeout;
	uint16			tNbEvents;
	uint16			tStopAttribute;
	uint16			tPosX;
	uint16			tPosY;
};

class Displayable : public DisplayableNum
{
public:
	//! public constructors
	/*! \param type event type
	 *  \param startTime start timestamp
	 *  \param name displayable name
	 *  \param relativePath relative path of the displayable
	 *  \param nbFrames number of frame if the displayable is a video
	 *  \param frameDuration frame duration if the displayable is a video
	 *  \param leftRect, topRect, rightRect, bottomRect the video position on the screen
	 *
	 *	\see eventType_t for event types
	 */
	Displayable(const uint16 value, const uint32 startTime, const std::string& name, const std::string& relativePath);
	Displayable(const uint16 value, const uint32 startTime, const std::string& name, const std::string& relativePath, const uint16 nbFrames, const uint16 frameDuration, const uint16 leftRect, const uint16 topRect, const uint16 rightRect, const uint16 bottomRect);
	Displayable(const uint32 startTime, std::vector<char*>::const_iterator& it);
	
	// Nathalie 20170529
	Displayable(const uint32 startTime, const std::string& imageName);
	void End(const uint32 endTime, const uint16 stopAttribute);

	~Displayable();
		
	//! set the associated transition with a displayable
	/*! \param type transition type
	 *  \param timeout associated timeout
	 *  \param nbEvents number of events of the same type necessary to generate a transitionhe displayable is a video
	 *
	 *	\see CTransition 
	 */
	void SetTransition(const uint16 value, const std::string& name, const uint32 timeout, const uint16 nbEvents);
		
	//! set the end moment of a displayable
	/*! \param endTime stop timestamp
	 *  \param it take the information from a vector
	 *
	 *	\see CTransition 
	 */
	void End(const uint32 endTime, std::vector<char*>::const_iterator& it);

	//! displayable section
	std::string		name;			//!< displayable name
	std::string		relativePath;	//!< displayable relative path

	//! transition section
	//! \see CTransition for definitions
	std::string		tName;
};
