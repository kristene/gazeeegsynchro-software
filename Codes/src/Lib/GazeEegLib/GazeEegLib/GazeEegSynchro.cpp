/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

// GaseEegSynchro.cpp�: d�finit le point d'entr�e pour l'application console.
//
#include "stdafx.h"

#include "AscRawData.h"
#include "Brainamp.h"
#include "Parser.h"
#include "File.h"
#include "ttl.h"

using GazeEeg::File;

typedef enum
{	_ERROR_ARGUMENTS,
	_ERROR_FILE,
} error_t;

typedef enum
{	ARG_FILE_EEG,
	ARG_FILE_GAZE,
	ARG_FILE_TOWARD_LOW,
	ARG_FILE_ONLY_SEGMENTS,
	ARG_ALL,
	ARG_OPTIONAL	= (ARG_ALL - ARG_FILE_TOWARD_LOW),
	ARG_MANDATORY	= (ARG_ALL - ARG_OPTIONAL),
} arg_t;

static void Help(int argc, char* argv[], const std::string& text)
{
	std::cout << "Software version : " << SOFTWARE_VERSION << std::endl;
	std::cout << "Author: G. Ionescu - UMR 5216 Gipsa-lab / CNRS Grenoble-INP, Grenoble Alpes University" << std::endl;
	std::cout << "Contributors: G. Ionescu, A. Andreev, E. Kristensen - UMR 5216 Gipsa-lab / CNRS Grenoble-INP, Grenoble Alpes University" << std::endl;
	std::cout << "Copyright (c) 2018 Gelu Ionescu, Gipsa-lab / CNRS Grenoble-INP, Grenoble Alpes University" << std::endl;
	std::cout << "All rights reserved." << std::endl;

	std::cout << std::endl;
	
	std::cout		<< text;
	for(int ii=0; ii < argc; ii++)
		std::cout	<< ' ' << argv[ii];
	std::cout	<< std::endl << std::endl;
}

static void Help(int argc, char* argv[], const error_t reason, const std::string& text = "", const std::string& text1 = "")
{
	Help(argc, argv, "GazeEegSynchro");

	switch(reason)
	{	case _ERROR_ARGUMENTS:
			std::cout << "Invalid argument list";
			break;
		case _ERROR_FILE:
			//std::cout << "Source files does not exist : " << text	<< std::endl;
			//std::cout << "Source files does not exist : " << text1	<< std::endl;
			std::cout << "Problem with input files. Please verify that the .eeg, .vhdr, .vmrk and .asc files exist and are named correctly." << std::endl;
			break;
	}

	std::cout  << std::endl << std::endl << std::endl;

	std::cout << "Generate Brainamp & Eyelink synchronized data"													<< std::endl << std::endl;
	//std::cout << "Calling convention : GaseEegSynchro EEGfileName ASCfileName [-f valMsec] [-t trigFileName] [-p] [-v] [-h outputPath] [-b] [-1]"	<< std::endl << std::endl;
	std::cout << "Calling convention : GazeEegSynchro.exe EEGfileName ASCfileName [options]" << std::endl << std::endl;
	
	std::cout << "EEGfileName         - a valid .EEG file Brainamp format (multiplexed)" << std::endl;
	std::cout << "ASCfileName         - a valid .ASC file EyeLink format" << std::endl;
	std::cout << std::endl;
	std::cout << "If the EEGfileName and the ASCfileName have the same base name then only one can be provided." << std::endl << std::endl;

	std::cout << "Options" << std::endl;
	//std::cout << "[-f valMsec]        - False blink duration (0 = no detection)"																<< std::endl;
	//std::cout << "[-t trigFileName]   - File name for the trigger names"																		<< std::endl;
	std::cout << "[-p]                - pupil size processing (default : false)"																<< std::endl;
	//std::cout << "[-v]                - process video frames as events (default : false)"														<< std::endl;
	std::cout << "[-h outputPath]     - Specifies where the synthonized output files are stored"								                << std::endl;
	
	std::cout << "[-e]                - interpolation toward EEG               (default :  toward GAZE)"										<< std::endl;
	//std::cout << "-m                  - output .mat files in Gipsa-lab format  (default :  false)" << std::endl;
	//std::cout << "[-1]                - force event duration                   (default : true = force to 1)    "								<< std::endl;
	
	std::cout << std::endl << std::endl;

	system("pause");

	exit(-1);
}

template <class T> void ProcessingDetails(int argc, char* argv[], const GazeEegEnv& gazeEegEnv, const T eegNaN)
{
	Help(argc, argv, "GazeEegSynchro");

	std::cout	<< "Processing " << std::endl
				<< "  EEG file name              : " << gazeEegEnv.eegFileName							<< std::endl
				<< "  EYELINK file name          : " << gazeEegEnv.gazeFileName							<< std::endl
				<< (!gazeEegEnv.triggerFileName.empty()	?  ("  Triggers File   : " + gazeEegEnv.triggerFileName) : "")		<< std::endl;

	std::cout	<< "  Do drift correction        : " << "result files begin with \"synchro\" string" << std::endl;
	if(gazeEegEnv.doNoDrift)
		std::cout	<< "  No drift correction        : " << "result files begin with \"synchrono\" string" << std::endl;

	if(gazeEegEnv.eegReference)
		std::cout	<< "  Interpolation              : " << "Uses EEG device as reference"								<< std::endl;
	else
		std::cout	<< "  Interpolation              : " << "Uses Eytracker device as reference"						<< std::endl;

	if(gazeEegEnv.falseBlinkDuration)
		std::cout	<< "  False blinks detection     : " << "YES under " << gazeEegEnv.falseBlinkDuration << " msec"				<< std::endl;

	if(gazeEegEnv.onPupil)
		std::cout	<< "  Pupil size                 : " << "YES"		  << std::endl;

	if (gazeEegEnv.doMatbuilder)
		std::cout   << "  Mat files output enabled   : " << "YES" << std::endl;
		
	std::cout << std::endl;
}

extern int	MatBuilder(int argc, char* argv[]);

int GaseEegSynchro(int argc, char* argv[])
{	
	if(argc == 0)
		Help(argc, argv, _ERROR_ARGUMENTS);
	
	GazeEegEnv		gazeEegEnv;
	bool			first = true;

	std::string		srcFileName1;
	std::string		srcFileName2;

	AscRawData		myEyelink;
	Brainamp		myBrainamp;


	// check the processing conditions	
	for(int ii=0; ii < argc; ii++)
	{	// common with MatBuilder

		if(strcmp(argv[ii], "-h") == 0)
			gazeEegEnv.outputPath			= argv[++ii];		
		// specific to EEG
		else if(strcmp(argv[ii], "-e") == 0)
			gazeEegEnv.eegReference			= true;

		// hiden options
		else if(strcmp(argv[ii], "-1") == 0)
			gazeEegEnv.forceDuration1		= false;
		else if (strcmp(argv[ii], "-f") == 0)
			gazeEegEnv.falseBlinkDuration = atoi(argv[++ii]);
		else if (strcmp(argv[ii], "-m") == 0) //use matbuilder to generate .mat and .event.txt files
			gazeEegEnv.doMatbuilder = true;
		else if (strcmp(argv[ii], "-t") == 0)
			gazeEegEnv.triggerFileName = argv[++ii];
		else if (strcmp(argv[ii], "-p") == 0)
			gazeEegEnv.onPupil = true;
		else if (strcmp(argv[ii], "-v") == 0)
			gazeEegEnv.onVideoframe = true;
		else if(strcmp(argv[ii], "-Q") == 0)
			gazeEegEnv.quality.doIt			= true;
		else if(strcmp(argv[ii], "-A") == 0)
			gazeEegEnv.article.doIt			= true;
		else if(strcmp(argv[ii], "-N") == 0)
			gazeEegEnv.doNoDrift			= true;
		else if(strcmp(argv[ii], "-G") == 0)
			gazeEegEnv.doDebug				= true;
		else if(strcmp(argv[ii], "-P") == 0)
			gazeEegEnv.doPause				= true;
		else if (strcmp(argv[ii], "-T") == 0)
			gazeEegEnv.doTtl = true;
		//else if (strcmp(argv[ii], "-y") == 0) //add velocity data if available
		//	gazeEegEnv.doVelocityData = true;
		else if (strcmp(argv[ii], "-L") == 0) //switch between the two versions of LCS synchro algorithm
		{
			gazeEegEnv.doClassicLCS = true;
		}
		else if(argv[ii][0] != '-')
		{	if(first)
			{	srcFileName1		= argv[ii];
				srcFileName2		= srcFileName1;
				first				= false;
			}
			else
				srcFileName2		= argv[ii];
	}	}

	gazeEegEnv.doVelocityData = true;

	//start: make sure that only the EEG or ASC file are supplied
	bool ascFileDetected = false;
	bool eegFileDetected = false;
	if (srcFileName1.size() > 4)
	{
		std::string	fileName(srcFileName1);
		std::transform(fileName.begin(), fileName.end(), fileName.begin(), ::tolower);

		int p = fileName.find(".asc");

		if (fileName.find(".asc", fileName.size() - 4) != std::string::npos)
			ascFileDetected = true;

		if (fileName.find(".eeg", fileName.size() - 4) != std::string::npos)
			eegFileDetected = true;
	}
	if (srcFileName2.size() > 4)
	{
		std::string	fileName(srcFileName2);
		std::transform(fileName.begin(), fileName.end(), fileName.begin(), ::tolower);

		if (fileName.find(".asc", fileName.size() - 4) != std::string::npos)
			ascFileDetected = true;

		if (fileName.find(".eeg", fileName.size() - 4) != std::string::npos)
			eegFileDetected = true;
	}
	if (!ascFileDetected && !eegFileDetected)
	{
		std::cerr << "You must provide at least one file with extension .asc or .eeg" << std::endl;
		exit(-4);
	}
	//end: make sure that only the EEG or ASC file are supplied

	if(srcFileName1 == srcFileName2)
	{	std::string	fileName(srcFileName1);
		std::transform(fileName.begin(), fileName.end(), fileName.begin(), ::tolower);

		if(fileName.find(".asc") != std::string::npos)
			srcFileName1.replace(fileName.find(".asc"), 4, ".eeg");
		else if(fileName.find(".eeg") != std::string::npos)
			srcFileName1.replace(fileName.find(".eeg"), 4, ".asc");
		else if(fileName.find(".vhdr") != std::string::npos)
			srcFileName1.replace(fileName.find(".vhdr"), 5, ".asc");
		else if(fileName.find(".vmrk") != std::string::npos)
			srcFileName1.replace(fileName.find(".vmrk"), 5, ".asc");
		else
			Help(argc, argv, _ERROR_FILE, srcFileName1, srcFileName2);
	}

	// correction of the input files. File::Exists requires ".\" before a file that is in the same folder and its full path is not specified
	if (srcFileName1.find("/" ) == std::string::npos && srcFileName1.find("\\") == std::string::npos)
		srcFileName1 =  ".\\"  + srcFileName1;
	if (srcFileName2.find("/") == std::string::npos && srcFileName2.find("\\") == std::string::npos)
		srcFileName2 = ".\\" + srcFileName2;

	// check the Eyelink & Brainamp soure file
	if(!(myEyelink.IsValid(gazeEegEnv, srcFileName1) && 
		 myBrainamp.IsValid(gazeEegEnv, srcFileName2)))
	{	if(!(myEyelink.IsValid(gazeEegEnv, srcFileName2) && 
			myBrainamp.IsValid(gazeEegEnv, srcFileName1)))
			Help(argc, argv, _ERROR_FILE, srcFileName1, srcFileName2);
	}

	// build result file name
	std::string		resultFile;

	std::string		path, name, ext;
	Parser::ParsePath(path, name, ext, gazeEegEnv.gazeFileName);
	gazeEegEnv.quality.srcFile = name;
	gazeEegEnv.article.srcFile = name;

	if (gazeEegEnv.outputPath.empty())
	{
		gazeEegEnv.outputPath = path;
	}
	else
	{	if(!Parser::CreatePath(gazeEegEnv.outputPath))
		{
			std::cerr << "!!! Can not create output path -> " << gazeEegEnv.outputPath << std::endl;
			
			system("pause");

			exit(-3);
		}

		gazeEegEnv.ProcessOutputMatFiles();
	}

    //start handle log folder location
	if (!gazeEegEnv.outputPath.empty())
	{
		GazeEegEnv::LogFolder = "";

		if ((gazeEegEnv.outputPath[gazeEegEnv.outputPath.length()-1] == '\\'))
			GazeEegEnv::LogFolder = gazeEegEnv.outputPath + "log";
		else GazeEegEnv::LogFolder = gazeEegEnv.outputPath + "\\log";

		bool logFolderExists = GazeEeg::File::Exists(GazeEegEnv::LogFolder) && GazeEeg::File::IsDirectory(GazeEegEnv::LogFolder);

		if (!logFolderExists)
		{
			if (!Parser::CreatePath(GazeEegEnv::LogFolder))
			{
				std::cerr << "!!! Can not create output path -> " << GazeEegEnv::LogFolder << std::endl;

				system("pause");

				exit(-3);
			}
		}
		GazeEegEnv::LogFolder = GazeEegEnv::LogFolder + "\\";
		myEyelink.SetLogFile(GazeEegEnv::LogFolder + std::string("Log-AscRawData.txt"));
	}
	//end handle log folder location

	Ttl		ttlRef;
	bool	ttlExists = false;

	resultFile = gazeEegEnv.outputPath + "\\synchro_";

	Parser::ParsePath(path, name, ext, gazeEegEnv.gazeMatFileName);
	resultFile	+= name;

	// process EEG
	Parser::binaryFormat_t	binaryFormat = myBrainamp.GetBinaryFormat(gazeEegEnv.eegFileName);

	clock_t begin = clock();

	bool	bProcessEyelink		= false;
	bool	bProcessBrainamp	= false;
	bool	bSynchro			= false;
	switch(binaryFormat) // EK : Process depending on binaryformat
	{	
	    case Parser::int16_type:
			{	
			    ProcessingDetails(argc, argv, gazeEegEnv, Parser::int16NaN); // EK : display the processing details

				bProcessBrainamp	= myBrainamp.Process(gazeEegEnv, Parser::int16NaN, false);
				if(!bProcessBrainamp)
					break;
				
				begin				= clock();
				gazeEegEnv.doRounding = true; //allows data with decimal point to be rounded instead of truncated when saving it to int16
				bProcessEyelink		= myEyelink.Process(gazeEegEnv);
				gazeEegEnv.quality.tmpET = (double)(clock() - begin) / CLOCKS_PER_SEC;
				if(!bProcessEyelink)
					break;

				ttlExists			= ttlRef.Read(gazeEegEnv.gazeFileName);

				GazeEeg::File::Remove(gazeEegEnv.gazeMatFileName);		
				if(gazeEegEnv.doMatbuilder && !myEyelink.SaveMatFile(gazeEegEnv))
				{
					std::cerr << "!!! Can not create a valid MAT file -> " << gazeEegEnv.gazeMatFileName << std::endl;
					
					system("pause");

					exit(-3);
				}

				// EK : saving .asc.mat and matching ET EEG triggers, saving asc.eeg.event.txt
				bSynchro = myBrainamp.SynchronizeGazeEeg(gazeEegEnv, resultFile, myEyelink, ttlRef, Parser::int16NaN);
			}
			break;
		case Parser::float_type:
			{	
			    ProcessingDetails(argc, argv, gazeEegEnv, Parser::floatNaN);

				bProcessBrainamp	= myBrainamp.Process(gazeEegEnv, Parser::floatNaN, false);
				if(!bProcessBrainamp)
					break;
				
				begin				= clock();
				bProcessEyelink		= myEyelink.Process(gazeEegEnv);
				gazeEegEnv.quality.tmpET = (double)(clock() - begin) / CLOCKS_PER_SEC;
				if(!bProcessEyelink)
					break;

				ttlExists			= ttlRef.Read(gazeEegEnv.gazeFileName);

				GazeEeg::File::Remove(gazeEegEnv.gazeMatFileName);		
				if(gazeEegEnv.doMatbuilder && !myEyelink.SaveMatFile(gazeEegEnv))
				{
					std::cerr << "!!! Can not create a valid MAT file -> " << gazeEegEnv.gazeMatFileName << std::endl;
					
					system("pause");

					exit(-3);
				}

				bSynchro	 = myBrainamp.SynchronizeGazeEeg(gazeEegEnv, resultFile, myEyelink, ttlRef, Parser::floatNaN);
			}
			break;
		case Parser::double_type:
			{	
			    ProcessingDetails(argc, argv, gazeEegEnv, Parser::doubleNaN);

				bProcessBrainamp	= myBrainamp.Process(gazeEegEnv, Parser::doubleNaN, false);
				if(!bProcessBrainamp)
					break;
				
				begin				= clock();
				bProcessEyelink		= myEyelink.Process(gazeEegEnv);
				gazeEegEnv.quality.tmpET = (double)(clock() - begin) / CLOCKS_PER_SEC;
				if(!bProcessEyelink)
					break;

				ttlExists			= ttlRef.Read(gazeEegEnv.gazeFileName);

				GazeEeg::File::Remove(gazeEegEnv.gazeMatFileName);		
				if(gazeEegEnv.doMatbuilder && !myEyelink.SaveMatFile(gazeEegEnv))
				{
					std::cerr << "!!! Can not create a valid MAT file -> " << gazeEegEnv.gazeMatFileName << std::endl;
					
					system("pause");

					exit(-3);
				}

				bSynchro	 = myBrainamp.SynchronizeGazeEeg(gazeEegEnv, resultFile, myEyelink, ttlRef, Parser::doubleNaN);
			}
			break;
		default:
			std::cerr << "!!! Invalid EEG data format" << std::endl;
			system("pause");
			exit(-3);
			break;
	}

//	gazeEegEnv.quality.duration = (double)(clock() - begin) / CLOCKS_PER_SEC;
	std::cout << "Alignment errors [samples] for all shared triggers are in: " << GazeEegEnv::LogFolder + std::string("GlobalError.txt") << std::endl;
	std::cout << "Histogram of the alignment errors [samples] for all triggers: " << GazeEegEnv::LogFolder + std::string("HistoGlobalError.txt") << std::endl;
	std::cout << "Histogram of the alignment errors [samples] for each trigger: " << GazeEegEnv::LogFolder + std::string("HistoPerTriggerError.txt") << std::endl;

	gazeEegEnv.quality.Process();
	gazeEegEnv.article.Process();

	if(!bProcessEyelink)
	{	std::cerr << "!!! Can not process the ASC file -> " << gazeEegEnv.gazeFileName << std::endl;
			
		system("pause");
		exit(-2);
	}

	if(!bProcessBrainamp)
	{	std::cerr << "!!! Can not process the EEG files -> " << gazeEegEnv.eegFileName << std::endl;
		
		system("pause");
		exit(-3);
	}

	// synchronize data
	if(!bSynchro)
	{	std::cerr << "!!! Can not synchronize data !!!" << std::endl;
			
		system("pause");
		exit(-3);
	}

	//std::string	matBuilderFile	= resultFile + ".eeg";
	//char*		pMatBuilderFile	= (char*) matBuilderFile.c_str();
	//::MatBuilder(1, &pMatBuilderFile);

	//if(!gazeEegEnv.doDrift)
	//{	std::string matBuilderFileNo(matBuilderFile);
	//	std::string::size_type pos = matBuilderFileNo.find("synchro_");
	//	matBuilderFileNo.replace(pos, 8, "synchrono_");

	//	char*		pMatBuilderFileNo	= (char*) matBuilderFileNo.c_str();
	//	::MatBuilder(1, &pMatBuilderFileNo);
	//}

	//char* matBuilderArgs[]	=	{	(char*) gazeEegEnv.gazeFileName.c_str(),
	//								"-h",
	//								(char*) gazeEegEnv.outputPath.c_str()
	//							};

	//::MatBuilder(3, matBuilderArgs);

	std::cout << "!!! GazeEegSynchro END !!! " << std::endl;

	if(gazeEegEnv.doPause)
		system("pause");

	return 0;
}
