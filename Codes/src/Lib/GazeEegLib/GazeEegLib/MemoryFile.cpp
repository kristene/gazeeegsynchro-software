/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

#include "StdAfx.h"

#include "File.h"
#include "MemoryFile.h"

MemoryFile::MemoryFile()
	: myStartPos(0)
{
}

MemoryFile::~MemoryFile()
{
}

bool MemoryFile::Read(const std::string& srcFileName)
{
	if(!GazeEeg::File::ReadInString(*this, srcFileName))
		return MSendError("MemoryFile::Read::File::ReadInString");
	
	return true;
}

bool MemoryFile::Write(const std::string& destFileName)
{
	if(!GazeEeg::File::WriteFromString(*this, destFileName))
		return MSendError("MemoryFile::Write::File::WriteFromString");
	
	return true;
}

bool MemoryFile::ReplaceBetween(const std::string& targget, const std::string& header, const std::string& footer /*= "\r\n"*/)
{
	std::string::size_type	pos = myStartPos;
	
	myStartPos	= find(header, myStartPos);
	
	if(myStartPos == std::string::npos)
	{	
		myStartPos = pos;
		return false;
	}

	myStartPos += header.length();
	pos			= find_first_of(footer, myStartPos);

	replace(begin()+myStartPos, begin()+pos, targget);

	return true;
}

bool MemoryFile::GetString(std::string& targget, const std::string& header, const std::string& footer /*= "\nrn"*/)
{
	std::string::size_type	pos = myStartPos;
	
	myStartPos	= find(header, myStartPos);
	if(myStartPos == std::string::npos)
	{	myStartPos = pos;
		return false;
	}

	myStartPos += header.length();
	pos			= find_first_of(footer, myStartPos);
	targget		= substr(myStartPos, pos - myStartPos);
	myStartPos	= pos;

	return true;
}

bool MemoryFile::AddAfter(const std::string& targget, const std::string& header, const std::string& footer /*= "\r\n"*/)
{
	std::string::size_type	pos = myStartPos;

	myStartPos	= find(header, myStartPos);
	if(myStartPos == std::string::npos)
	{	myStartPos = pos;
		return false;
	}

	myStartPos	= SkipSequence(myStartPos + header.length(), footer);

	insert(myStartPos, targget);

	return true;
}

bool MemoryFile::AddNewAfterPrevious(const std::string& value, const std::string& newHeader, const std::string& previousHeader)
{
	std::string::size_type	pos = myStartPos;

	pos = find(previousHeader, 0);

	if (pos == std::string::npos)
	{
		return false;
	}

	std::string::size_type	end_line = find("\n", pos);
	insert(end_line + 1, newHeader + value + "\r\n");

	return true;
}

bool MemoryFile::EraseAfter(const std::string& header, const std::string& footer /*= "\r\n"*/)
{
	std::string::size_type	pos = myStartPos;

	myStartPos	= find(header, myStartPos);
	if(myStartPos == std::string::npos)
	{	myStartPos = pos;
		return false;
	}

	myStartPos	= SkipSequence(myStartPos + header.length(), footer);

	erase(myStartPos);

	return true;
}

std::string::size_type MemoryFile::SkipSequence(const std::string::size_type startPos, const std::string& sequence)
{
	std::string::size_type pos	= find_first_of(sequence, startPos);
	std::string::size_type pos1	= find_first_of(sequence, pos + 1);

	return ((pos1 - pos) == 1) ? pos1 + 1 : pos + 1;
}


bool MemoryFile::EraseBefore(const std::string& header)
{
	std::string::size_type	pos = myStartPos;

	myStartPos	= find(header, myStartPos);
	if(myStartPos == std::string::npos)
	{	myStartPos = pos;
		return false;
	}

	erase(myStartPos);

	return true;
}

