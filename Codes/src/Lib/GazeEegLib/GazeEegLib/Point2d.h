/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

#pragma once

#include <math.h>

#ifndef _min_
#define	_min_(a, b)	(((a) < (b)) ? (a) : (b))
#endif

#ifndef _max_
#define	_max_(a, b)	(((a) > (b)) ? (a) : (b))
#endif

#ifndef	M_PI
#define	M_PI				3.1415926535897932385
#endif

#ifndef	M_SQRT2
#define	M_SQRT2				1.4142135623730950488
#endif

#ifndef	M_LOG2
#define	M_LOG2				0.3010299956639811952
#endif

#undef	MAX_VERIF_ERROR
#define	MAX_VERIF_ERROR		double(0.000001)

#ifndef DEGREES
#define	DEGREES(radians)	((180.0*radians)/M_PI)
#endif

#ifndef RADIANS
#define	RADIANS(degrees)	((M_PI*degrees)/180.0)
#endif

class CPoint2d;
class CCovector2d;
class CLine2d;

class  CVector2d
{
public:
    double	x;	//!< position on Ox axis
    double	y;	//!< position on Oy axis
    double	w;	//!< perspective component

    CVector2d();															//!<	null object (x,y,w = 0)
    CVector2d(const double iX, const double iY, const double iW=0) { x = iX; y = iY;  w = iW; }		//!<	build with specified coordinates
    CVector2d(const double* tab);										//!<	build from a buffer
    CVector2d(const CVector2d& v) {	x = v.x; y = v.y; w = v.w; }		//!<	copy constructor
    CVector2d(const CVector2d* v);										//!<	copy constructor
    CVector2d(const double angleD);										//!<	copy constructor
    ~CVector2d(){}														//!<	destructor

	// Operations
    //const CVector2d&	operator=(const CVector2d& v);							//!<	equal operator
    //const CVector2d& 	operator=(const CVector2d* v);						//!<	equal operator
    friend  CVector2d 	operator+(const CVector2d& v1, const CVector2d& v2);	//!<	operation : v  = v1 + v2 
    const CVector2d& 	operator+=(const CVector2d& v);						//!<	operation : v += v1
    friend  CVector2d 	operator-(const CVector2d& v1, const CVector2d& v2);	//!<	operation : v  = v1 - v2
    friend  CVector2d 	operator-(const CVector2d& v1);  					//!<	operation : v = -v1
    const CVector2d& 	operator-=(const CVector2d& v);  					//!<	operation : v -= v1
    friend  CVector2d  operator*(const CVector2d& v1, double scale);  		//!<	operation : v = v1*k
    friend  CVector2d  operator*(double scale, const CVector2d& v1);  		//!<	operation : v = k*v1
    const CVector2d& 	operator*=(double scale); 	     					//!<	operation : v *= k
    friend  double     operator*(const CVector2d& v, const CCovector2d& cv);	//!<	dot product
    friend  CVector2d  operator/(const CVector2d& v1, double scale);			//!<	operation : v = v1/k
    const CVector2d& 	operator/=(double scale)
	{
		if (fabs(scale) < MAX_VERIF_ERROR)
		{	*this = CVector2d::GetEmpty();
		}
		else
		{	x	/= scale;
			y	/= scale;
		}

		return *this;
	}
    friend  bool	operator==(const CVector2d& v1, const CVector2d& v2);		//!<	equality test
    friend  bool  	operator!=(const CVector2d& v1, const CVector2d& v2);		//!<	equality test

	// Functions 
    double				Dot(const CVector2d& v) const;						//!<	dot product
    friend  double		Dot(const CVector2d& v1, const CVector2d& v2);		//!<	dot product
    double				GetArea(const CVector2d& v) const;						//!<	get parallelogram area
    friend  double		GetArea(const CVector2d& v1, const CVector2d& v2);		//!<	get parallelogram area
    double				GetAngle(const CVector2d& v) const;						//!<	get angle (radians) between 2 vectors
    friend  double		GetAngle(const CVector2d& v1, const CVector2d& v2);		//!<	get angle (radians) between 2 vectors
    double				GetAngleD(const CVector2d& v) const;						//!<	get angle (degrees) between 2 vectors
    friend  double		GetAngleD(const CVector2d& v1, const CVector2d& v2);		//!<	get angle (degrees) between 2 vectors
    CVector2d&			Normalize();											//!<	vector normalization
    CVector2d 			GetNormalize() const;									//!<	returns a normalized vector 
    friend  CVector2d 	GetNormalize(const CVector2d& v);						//!<	returns a normalized vector
    bool				Compare(const CVector2d& v) const;						//!<	vector comparision
    double     			GetLength() const;										//!<	get vector norm
    bool				IsEmpty() const;										//!<	verify if the vector is empty (x,y,w = 0)
    CVector2d&			Empty();												//!<	make an empty vector (x,y,w = 0)
	static CVector2d 	GetEmpty()
	{
		return CVector2d(0,0,0); 
	}
    friend  CVector2d 	GetEmpty(const CVector2d& v);							//!<	return an empty vector (x,y,w = 0)
    static CVector2d		GetI();													//!<	return Ox unitary vector (x=1,y=0,w = 0)
    static CVector2d 	GetJ();													//!<	return Oy unitary vector (x=0,y=1,w = 0)
};

/*!     \brief  A 2D geometric point.
 
        To represent points in the 2D space we need two coordinates :  \b x and  \b y. But, because our library
		should be compatible with the perspective geometry, we ad a further coordinate : \b w.

		\remarks In the nonperspectrive geometry, for points, always <b>w = 1</b> 
 */
class  CPoint2d : public CVector2d
{
public:

	// Constructors - Destructor
	CPoint2d() : CVector2d(0, 0, 1) {}															//!<	null point (x=0,y=0,w=1)
    CPoint2d(const double iX, const double iY, const double iW=1) : CVector2d(iX, iY, iW) {}		//!<	build with specified coordinates
    //CPoint2d(const double* tab);										//!<	build from a buffer
	CPoint2d(const CVector2d& v) : CVector2d(v.x, v.y, v.w) {}
    //CPoint2d(const CVector2d* v);										//!<	copy constructor
    //CPoint2d(const CPoint2d& p);										//!<	copy constructor
    //CPoint2d(const CPoint2d* p);										//!<	copy constructor
    ~CPoint2d(){}

	// Operations
    friend  CPoint2d 	operator+(const CPoint2d& v1, const CVector2d& v2);	//!<	point translation
    const	CPoint2d& 	operator+=(const CVector2d& v);						//!<	point translation
    friend  CPoint2d 	operator-(const CPoint2d& v1, const CVector2d& v2);	//!<	point translation
    friend  CVector2d 	operator-(const CPoint2d& v1, const CPoint2d& v2);	//!<	vector between 2 points
    const	CPoint2d& 	operator-=(const CVector2d& v);						//!<	point translation

// Fonctions 
    bool				IsPointOnLine(const CLine2d& l) const;				//!<	is a point on a 2D line
    double				GetDistance(const CPoint2d& p) const
	{
		return sqrt((p.x - x)*(p.x - x) + (p.y - y)*(p.y - y)); 
	}
    friend  double		GetDistance(const CPoint2d& p1,const CPoint2d& p2);	//!<	distance between two points
    CLine2d				GetParallel(CLine2d& l) const;						//!<	get a parallel 2D line passing through the current point
};

/*!     \brief  A 2D geometric covector (it can be seen as a horizontal vector).
 
        Similarly to the CVector2d object, this class allows the creation of  2D dual objects (e.g. lines).
 */
class  CCovector2d
{
public:
	// Class elements
   double	a;	//!< direction parameter
   double	b;	//!< direction parameter
   double	c;	//!< distance parameter

	// Constructors - Destructor
    CCovector2d();														//!<	null object (a,b,c = 0)
    CCovector2d(const double iA, const double iB, const double iC) { a = iA; b = iB; c = iC; }//!<	build with specified coordinates
    CCovector2d(const double* tab);										//!<	build from a buffer
    CCovector2d(const CCovector2d& cv);									//!<	copy constructor
    CCovector2d(const CCovector2d* cv);									//!<	copy constructor
    ~CCovector2d(){}														//!<	destructor

	// Operations
    const CCovector2d&	operator=(const CCovector2d& cv)							//!<	equal operator
	{
		a = cv.a; 
		b = cv.b; 
		c = cv.c;
		return *this;
	}
    const CCovector2d&	operator=(const CCovector2d* cv);							//!<	equal operator
    friend  CCovector2d	operator*(const CCovector2d& cv, double scale);				//!<	operation : v = v1*k
    friend  CCovector2d	operator*(double scale, const CCovector2d& cv);				//!<	operation : v = k*v1
    const CCovector2d&	operator*=(double scale);									//!<	operation : v *= k
    friend  CVector2d	operator*(const CCovector2d& cv1, const CCovector2d& cv2)
	{
		CVector2d v(cv1.b*cv2.c - cv1.c*cv2.b, cv1.c*cv2.a - cv1.a*cv2.c, 
			cv1.a*cv2.b - cv1.b*cv2.a);

		if(fabs(v.w) < MAX_VERIF_ERROR)
			v = CVector2d::GetEmpty();
		else
		{	v	/= v.w;
			v.w	 = 1;
		}

		return v;
	}
    friend  CCovector2d	operator/(const CCovector2d& cv, double scale);				//!<	operation : v = v1/k
    const CCovector2d&	operator/=(double scale);									//!<	operation : v /= k
    friend  bool		operator==(const CCovector2d& cv1, const CCovector2d& cv2);	//!<	equality test
    friend  bool  		operator!=(const CCovector2d& cv1, const CCovector2d& cv2);	//!<	equality test

	// Fonctions
    bool				IsEmpty() const;										//!<	verify if the covector is empty (a,b,c = 0)
    CCovector2d&			Empty();												//!<	make an empty covector (a,b,c = 0)
    static CCovector2d	GetEmpty();												//!<	return an empty covector (a,b,c = 0)
    friend  CCovector2d	GetEmpty(const CCovector2d& cv);							//!<	return an empty covector (a,b,c = 0)
    bool 	 			Compare(const CCovector2d& cv) const;					//!<	vector comparision
};

/*!     \brief  A 2D line.
 */
class  CLine2d : public CCovector2d
{
public:

	// Constructors - Destructor
    CLine2d() : CCovector2d(0, 0, 0){}															//!<	null object (a,b,c = 0)
    CLine2d(const double iA, const double iB, const double iC) : CCovector2d(iA, iB, iC){}//!<	build with specified coordinates
    CLine2d(const double* tab);											//!<	build from a buffer
    CLine2d(const CCovector2d& cv);										//!<	copy constructor
    CLine2d(const CCovector2d* cv);										//!<	copy constructor
    CLine2d(const CLine2d& l);											//!<	copy constructor
    CLine2d(const CLine2d* l);											//!<	copy constructor
    CLine2d(const CPoint2d& p1, const CPoint2d& p2)	
		: CCovector2d(p1.y*p2.w - p1.w*p2.y, 
				p1.w*p2.x - p1.x*p2.w, 
				p1.x*p2.y - p1.y*p2.x)
	{}
    CLine2d(const float dist, const float angleD);						//!<	build a line at a distance from origin with a given slope     
    ~CLine2d(){}															//!<	destructor
	
// Fonctions
    static		CLine2d	GetOx();									//!<	get the Ox axis
    static 		CLine2d	GetOy();									//!<	get the Oy axis
    static 		CLine2d	GetOxParallel(const double oY);				//!<	get a parallel to Ox axis
    static 		CLine2d	GetOyParallel(const double oX)				//!<	get a parallel to Oy axis
	{
		return CLine2d(CPoint2d(oX, 0), CPoint2d(oX, 1));	
	}
    bool		IsPointOnLine(const CPoint2d& pt) const;				//!<	iss an CPoint2d point on the line
    friend		bool IsPointOnLine(const CPoint2d& pt, const CLine2d& l);
    CPoint2d	GetIntersection(const CLine2d& l) const
	{
		CVector2d pt = (*this) * l;

		if(fabs(pt.w) < MAX_VERIF_ERROR)
			return CPoint2d::GetEmpty();

		return CPoint2d(pt);
	}
    friend		CPoint2d	GetIntersection(const CLine2d& l1, const CLine2d& l2);				//!<	get intersection point betweel two 2D lines
    CLine2d		GetPerpendicular(const CPoint2d& pt) const
	{
		return CLine2d(-b, a, b*pt.x - a*pt.y);
	}
    CLine2d		GetParallel(CPoint2d& pt) const;						//!<	get a parallel passing through a given point
    double      GetSignedDistance(const CPoint2d& pt) const
	{
		return GetIntersection(GetPerpendicular(pt)).GetDistance(pt);
	}
    friend		double	GetSignedDistance(const CPoint2d& pt, const CLine2d& l);			//!<	get the signed distance to a given point
    double		GetDistance(const CPoint2d& pt) const
	{
		return fabs(GetSignedDistance(pt));	
	}
    friend		double	GetDistance(const CPoint2d& pt, const CLine2d& l);	//!<	get the absolute distance to a given point
    CLine2d&		Normalize()
					{
						double val = sqrt(a*a + b*b);	
						if (val < MAX_VERIF_ERROR)
							a = b = c = 0;
						else
						{	a /= val;
							b /= val;
							c /= val;
						}

						return *this;
					}
    CLine2d 		GetNormalize() const;
    friend CLine2d GetNormalize(const CLine2d& p);
    double		GetAngle(const CLine2d& line) const;					//!<	get  angle (radians) between 2 lines
    double		GetAngleD(const CLine2d& line) const	{	return DEGREES(GetAngle(line));	}//!<	get  angle (degrees) between 2 lines
    friend double GetAngle(const CLine2d& l1, const CLine2d& l2)	{	return l1.GetAngle(l2);	}	//!<	get  angle (radians) between 2 lines
    friend double GetAngleD(const CLine2d& l1, const CLine2d& l2)	{	return l1.GetAngleD(l2);	}	//!<	get  angle (degrees) between 2 lines
};
