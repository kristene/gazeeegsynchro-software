/*
Copyright 2021 Gelu Ionescu, Emmanuelle Kristensen, Anton Andreev,
Gipsa-lab, UMR 5216, UGA, CNRS, Grenoble-INP.
This software is a computer program whose purpose is to synchronize
electroencephalographic and gaze-tracking measures.

This software is governed by the CeCILL-B license under French law and
abiding by the rules of distribution of free software.  You can  use,
modify and/ or redistribute the software under the terms of the CeCILL-B
license as circulated by CEA, CNRS and INRIA at the following URL
"http://www.cecill.info".

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability.

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or
data to be ensured and,  more generally, to use and operate it in the
same conditions as regards security.

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

#include "StdAfx.h"

#include "Transitions.h"
#include "Event.h"

Transitions::Transitions(const size_t size /*= 0*/)
	: Events(size)
	, name(NameSize*size, 0)
	, typeAsked(size)
	, timeout(size)
	, nbEvents(size)
	, stopAttribute(size)
	, posX(size)
	, posY(size)
{
}

Transitions::~Transitions()
{
}

matvar_t* Transitions::GetComponents(char* structName, const bool conserve)
{
	MatComponents matComponents;
	Events::GetComponents(matComponents, conserve);
	
	matComponents.AddVariable("name",			NameSize,	MAT_C_CHAR,		name,			conserve);
	matComponents.AddVariable("typeAsked",					MAT_C_UINT16,	typeAsked,		conserve);
	matComponents.AddVariable("timeout",					MAT_C_UINT32,	timeout,		conserve);
	matComponents.AddVariable("nbEvents",					MAT_C_UINT16,	nbEvents,		conserve);
	matComponents.AddVariable("stopAttribute",				MAT_C_UINT16,	stopAttribute,	conserve);
	matComponents.AddVariable("posX",						MAT_C_UINT16,	posX,			conserve);
	matComponents.AddVariable("posY",						MAT_C_UINT16,	posY,			conserve);

	return matComponents.GetStructure(structName);
}

void Transitions::Add(	const uint16 value, const uint32 time, const uint32 duration, const uint32 fseek, 
			const std::string& name,
			const uint16 typeAsked, const uint32 timeout, const uint16 nbEvents, const uint16 stopAttribute,
			const uint16 posX,const uint16 posY)
{
	Events::Add(value, time, duration, fseek);
	
	std::vector<char> vName(NameSize);
	memcpy(&vName[0], name.c_str(), name.length());
	this->name.insert(this->name.end(), vName.begin(), vName.end());

	this->typeAsked.push_back(typeAsked);
	this->timeout.push_back(timeout);
	this->nbEvents.push_back(nbEvents);
	this->stopAttribute.push_back(stopAttribute);
	this->posX.push_back(posX);
	this->posY.push_back(posY);
}
