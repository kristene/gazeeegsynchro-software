# -*- coding: utf-8 -*-
"""
Created on Fri May 20 2021
This function generates a figure with three histograms of the alignment errors
after before and after synchronization.
The input data file is GlobalError.txt generated after each execution of
GazeEegSynchro software.
Input  : GlobalErrorFileName , for example '..\..\Data\Scenario1_1\log\GlobalError.txt'
@author: AGD
"""

import matplotlib.pyplot as plt
import numpy as np
import os.path


def fViewHistogram(GlobalErrorNameFile):
    if os.path.isfile(GlobalErrorNameFile):  
        
        file = open(GlobalErrorNameFile, "r")
        Lines = file.readlines()
        file.close()
    
    #Header with a fixed size    
        HeaderSize = 11    
        Header = Lines[0:(HeaderSize-1)]
        Data = Lines[HeaderSize:np.size(Lines)]
              
        cpt = -1
        
        for hdr in Header:
            cpt = cpt+1
            if hdr.find('nbTriggers') != -1:
                HeaderVector = hdr.split()
                NbTrigger = int(HeaderVector[2])
                
            if hdr.find('correctionInsertedBefore') != -1:
                HeaderVector = hdr.split()
                NbSegment = int(HeaderVector[2])
                
        DeltaC = hdr.split()
        
        strInfo = '$N_T$ = ' + str(NbTrigger) + '\n' + '$N_P$ = ' + str(NbSegment-1) +'\n'
        strInfo = strInfo + '$|\Delta_C|$   = '
        
        if NbSegment != 1:
            DeltaC = DeltaC[1: np.size(DeltaC)]
            for delta in DeltaC:
                strInfo = strInfo  + str(abs(int(delta))) + '   '
        else:
            strInfo = strInfo + str(abs(int(DeltaC[0])))
    
    #Preparing data    
        ErrorData = np.zeros(shape=(NbTrigger,3))  
        cpt = -1
        for line in Data:
            cpt = cpt+1
            lineVector = line.split()
            ErrorData[cpt, 0] = float(lineVector[0]) 
            ErrorData[cpt, 1] = float(lineVector[1])
            ErrorData[cpt, 2] = float(lineVector[2])
    
        ErrorMean = np.mean(ErrorData[:, 0])
        ErrorStd  = np.std(ErrorData[:, 0])
        strInfo = strInfo + '\n' + 'Mean = ' + str(round(ErrorMean, 3)) + '\n' + 'Std    = ' + str(round(ErrorStd, 3))
    
    #Graph               
        plt.figure()
        plt.subplot(1,3,1)
        plt.hist(ErrorData[:,2], bins = 20 )
        plt.ylabel('Number of triggers')
        plt.xlabel('Alignment errors [samples]')
        plt.title('Drift (off), shift (off) correction')
        
        plt.subplot(1,3,2)
        plt.hist(ErrorData[:,1], bins = 100 )
        plt.ylabel('Number of triggers')
        plt.xlabel('Alignment errors [samples]')
        plt.title('Drift (on), shift (off) correction')
        
        plt.subplot(1,3,3)
        plt.hist(ErrorData[:,0], bins = np.arange(-5.5, 6.5, 1), rwidth = 0.8)   
        plt.ylabel('Number of triggers')
        plt.xlabel('Alignment errors [samples]')
        plt.title('Drift (on), shift (on) correction')
        yaxis = plt.ylim()
        plt.text(-5,yaxis[1]-120.,strInfo)
     
        plt.show()
    
    else:
         print('The file ' + GlobalErrorNameFile + ' doesn''t exist.\n')    
    return

