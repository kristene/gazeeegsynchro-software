function [fid, fighdl] = fViewHistograms(GlobalErrorFileName)
% This function generates a figure with three histograms of the alignment errors
% after before and after synchronization.
% The input data file is GlobalError.txt generated after each execution of
% GazeEegSynchro software.
%Input  : GlobalErrorFileName , for example '..\..\Data\Scenario1_1\log\GlobalError.txt'
%Output : fighdl : handle of the created figure
%         fid : file identifier of the 'GlobalError.txt' file
%AGD, Fri May 20 2021
%

fid = fopen(GlobalErrorFileName);
if fid ==-1
    fprintf('The file %s doesn''t exist'.\n', GlobalErrorFileName)
    fighdl = [];
else
    A = textscan(fid, '%s');
    fclose(fid);
    
    pos1 = find(contains(A{1}, 'nbTriggers'));
    
    nbTriggers = str2double( A{1}{pos1+2});
    nbItem = nbTriggers * 3;
    
    indexData = length(A{1}) - nbItem +1;
    
    errorTab = zeros(nbTriggers, 3);
    
    cpt = -1 ;
    for idx = 1 : nbTriggers
        cpt = cpt +1;
        errorTab(idx, 1) = str2double(A{1}{indexData+cpt});
        cpt = cpt +1;
        errorTab(idx, 2) = str2double(A{1}{indexData+cpt});
        cpt = cpt +1;
        errorTab(idx, 3) = str2double(A{1}{indexData+cpt});
    end
    
    pos2 = find(contains(A{1}, 'correctionInsertedBefore'));
    nbSegment = str2num(A{1}{pos2+2});
    
    switch nbSegment
        case 1
            DeltaC = 0;
        case 2
            DeltaC = abs(str2num(A{1}{pos2+4}));
            
        otherwise
            DeltaC = zeros(1, nbSegment-1);
            
            for idx = 2 : nbSegment
                DeltaC(idx-1) = abs(str2num(A{1}{pos2+2+idx}));
            end
    end
    
    strText    = cell(1, 5);
    strText{1} = ['N_T = ' num2str(nbTriggers)];
    strText{2} = ['N_P = ' num2str(nbSegment-1)];
    strText{3} = ['|\DeltaC|  = ' num2str(DeltaC)];
    strText{4} = ['Mean = ' num2str(round(mean(errorTab(:, 1)),3))];
    strText{5} = ['Std    = ' num2str(round(std(errorTab(:, 1)),3))];
    
    fighdl = figure;
    subplot(1,3,1)
    hist(errorTab(:, 3), 20);
    title('Drift (off), shift (off) correction', 'FontSize', 14)
    xlabel('Alignment error [samples]', 'FontSize', 14)
    ylabel('Number of Triggers', 'FontSize', 14)
    
    subplot(1,3,2)
    hist(errorTab(:, 2), 100);
    title('Drift (on), shift (off) correction', 'FontSize', 14)
    xlabel('Alignment error [samples]', 'FontSize', 14)
    ylabel('Number of Triggers', 'FontSize', 10)
    
    subplot(1,3,3)
    [N, X] = hist(errorTab(:, 1), (-5:1:5));
    bar(X, N)
    vaxis = axis();
    ht = text(-5.5 ,vaxis(4)-30,strText);
    set(ht, 'VerticalAlignment', 'top','HorizontalAlignment', 'left', 'FontSize', 12 )
    xlabel('Alignment error [samples]', 'FontSize', 14)
    ylabel('Number of Triggers', 'FontSize', 14)
    title('Drift (on), shift (on) correction', 'FontSize', 14)
end

