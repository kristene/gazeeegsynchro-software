# -*- coding: utf-8 -*-
"""
This program generates a figure with three histograms of the alignment errors
after before and after synchronization.
The input data file is GlobalError.txt generated after each execution of
GazeEegSynchro software.

 Copyright 2021, Anne Guérin-Dugué, Gipsa-lab, UMR 5216, UGA, CNRS, 
 Grenoble-INP.

"""

import matplotlib.pyplot as plt
import histoerror as he



plt.close('all')

#Load file
NameFile = './GlobalError.txt'

he.fViewHistogram(NameFile)
